################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/graeme/dev/grambots/Source/Common/AiWorker.cpp \
/home/graeme/dev/grambots/Source/Common/AnimatedMesh.cpp \
/home/graeme/dev/grambots/Source/Common/AnimatedMeshData.cpp \
/home/graeme/dev/grambots/Source/Common/Assets.cpp \
/home/graeme/dev/grambots/Source/Common/Bone.cpp \
/home/graeme/dev/grambots/Source/Common/BoneData.cpp \
/home/graeme/dev/grambots/Source/Common/Bot.cpp \
/home/graeme/dev/grambots/Source/Common/Box.cpp \
/home/graeme/dev/grambots/Source/Common/Font.cpp \
/home/graeme/dev/grambots/Source/Common/GMath.cpp \
/home/graeme/dev/grambots/Source/Common/Game.cpp \
/home/graeme/dev/grambots/Source/Common/Grambots.cpp \
/home/graeme/dev/grambots/Source/Common/Laser.cpp \
/home/graeme/dev/grambots/Source/Common/Logger.cpp \
/home/graeme/dev/grambots/Source/Common/Main.cpp \
/home/graeme/dev/grambots/Source/Common/Map.cpp \
/home/graeme/dev/grambots/Source/Common/Renderable.cpp \
/home/graeme/dev/grambots/Source/Common/ShaderManager.cpp \
/home/graeme/dev/grambots/Source/Common/SkyBox.cpp \
/home/graeme/dev/grambots/Source/Common/StaticMesh.cpp \
/home/graeme/dev/grambots/Source/Common/StaticMeshData.cpp \
/home/graeme/dev/grambots/Source/Common/Team.cpp \
/home/graeme/dev/grambots/Source/Common/TextBlock.cpp \
/home/graeme/dev/grambots/Source/Common/TextureManager.cpp 

OBJS += \
./Source/Common/AiWorker.o \
./Source/Common/AnimatedMesh.o \
./Source/Common/AnimatedMeshData.o \
./Source/Common/Assets.o \
./Source/Common/Bone.o \
./Source/Common/BoneData.o \
./Source/Common/Bot.o \
./Source/Common/Box.o \
./Source/Common/Font.o \
./Source/Common/GMath.o \
./Source/Common/Game.o \
./Source/Common/Grambots.o \
./Source/Common/Laser.o \
./Source/Common/Logger.o \
./Source/Common/Main.o \
./Source/Common/Map.o \
./Source/Common/Renderable.o \
./Source/Common/ShaderManager.o \
./Source/Common/SkyBox.o \
./Source/Common/StaticMesh.o \
./Source/Common/StaticMeshData.o \
./Source/Common/Team.o \
./Source/Common/TextBlock.o \
./Source/Common/TextureManager.o 

CPP_DEPS += \
./Source/Common/AiWorker.d \
./Source/Common/AnimatedMesh.d \
./Source/Common/AnimatedMeshData.d \
./Source/Common/Assets.d \
./Source/Common/Bone.d \
./Source/Common/BoneData.d \
./Source/Common/Bot.d \
./Source/Common/Box.d \
./Source/Common/Font.d \
./Source/Common/GMath.d \
./Source/Common/Game.d \
./Source/Common/Grambots.d \
./Source/Common/Laser.d \
./Source/Common/Logger.d \
./Source/Common/Main.d \
./Source/Common/Map.d \
./Source/Common/Renderable.d \
./Source/Common/ShaderManager.d \
./Source/Common/SkyBox.d \
./Source/Common/StaticMesh.d \
./Source/Common/StaticMeshData.d \
./Source/Common/Team.d \
./Source/Common/TextBlock.d \
./Source/Common/TextureManager.d 


# Each subdirectory must supply rules for building sources it contributes
Source/Common/AiWorker.o: /home/graeme/dev/grambots/Source/Common/AiWorker.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/AnimatedMesh.o: /home/graeme/dev/grambots/Source/Common/AnimatedMesh.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/AnimatedMeshData.o: /home/graeme/dev/grambots/Source/Common/AnimatedMeshData.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Assets.o: /home/graeme/dev/grambots/Source/Common/Assets.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Bone.o: /home/graeme/dev/grambots/Source/Common/Bone.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/BoneData.o: /home/graeme/dev/grambots/Source/Common/BoneData.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Bot.o: /home/graeme/dev/grambots/Source/Common/Bot.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Box.o: /home/graeme/dev/grambots/Source/Common/Box.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Font.o: /home/graeme/dev/grambots/Source/Common/Font.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/GMath.o: /home/graeme/dev/grambots/Source/Common/GMath.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Game.o: /home/graeme/dev/grambots/Source/Common/Game.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Grambots.o: /home/graeme/dev/grambots/Source/Common/Grambots.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Laser.o: /home/graeme/dev/grambots/Source/Common/Laser.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Logger.o: /home/graeme/dev/grambots/Source/Common/Logger.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Main.o: /home/graeme/dev/grambots/Source/Common/Main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Map.o: /home/graeme/dev/grambots/Source/Common/Map.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Renderable.o: /home/graeme/dev/grambots/Source/Common/Renderable.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/ShaderManager.o: /home/graeme/dev/grambots/Source/Common/ShaderManager.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/SkyBox.o: /home/graeme/dev/grambots/Source/Common/SkyBox.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/StaticMesh.o: /home/graeme/dev/grambots/Source/Common/StaticMesh.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/StaticMeshData.o: /home/graeme/dev/grambots/Source/Common/StaticMeshData.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/Team.o: /home/graeme/dev/grambots/Source/Common/Team.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/TextBlock.o: /home/graeme/dev/grambots/Source/Common/TextBlock.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/Common/TextureManager.o: /home/graeme/dev/grambots/Source/Common/TextureManager.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DOS_LINUX -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


