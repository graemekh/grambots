#pragma once

#define DEFAULT_BUFFER_SIZE 100

template <class T>
class AsyncMessageBuffer
{
public:		
	AsyncMessageBuffer();
	AsyncMessageBuffer(unsigned int capacity);
	~AsyncMessageBuffer();
	bool Read(T *out);
	void Write(T element);

private:
	inline unsigned int GetEffectivePosition(unsigned long position);

	T *_elements;
	unsigned int _capacity;
	volatile unsigned long _readPosition;
	volatile unsigned long _writePosition;
};

template <class T>
AsyncMessageBuffer<T>::AsyncMessageBuffer()
{
	_capacity = DEFAULT_BUFFER_SIZE;
	_elements = new T[_capacity];
	_readPosition = 0;
	_writePosition = 0;
}

template <class T>
AsyncMessageBuffer<T>::AsyncMessageBuffer(unsigned int capacity)
{
	_capacity = capacity;
	_elements = new T[_capacity];
	_readPosition = 0;
	_writePosition = 0;
}

template <class T>
unsigned int AsyncMessageBuffer<T>::GetEffectivePosition(unsigned long position)
{
	return (unsigned int)(position % _capacity);
}

template <class T>
AsyncMessageBuffer<T>::~AsyncMessageBuffer()
{
	delete _elements;
}

template <class T>
bool AsyncMessageBuffer<T>::Read(T *out)
{
	// can only read value if writer is ahead of reader
	if (_readPosition >= _writePosition)
		return false;
	*out = _elements[GetEffectivePosition(_readPosition++)];
	return true;
}

template <class T>
void AsyncMessageBuffer<T>::Write(T element)
{
	_elements[GetEffectivePosition(_writePosition++)] = element;
}