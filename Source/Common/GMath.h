#pragma once

#define PI 3.14159265f
#define TWO_PI 6.28318531f
#define PI_OVER_TWO 1.57079633f
#define PI_OVER_180 0.0174532925f
#define _180_OVER_PI 57.2957795f

#include "MyOpenGL.h"
#include "CommonStructs.h"
#include <math.h>

class GMath
{
public:

	inline static Vector3 CrossProduct(const Vector3 &a, const Vector3 &b)
	{
		return Vector3(
			a.Y*b.Z - a.Z*b.Y,
			a.Z*b.X - a.X*b.Z,
			a.X*b.Y - a.Y*b.X);
	}
	
	inline static Vector3 AddVectors(const Vector3 &a, const Vector3 &b)
	{
		return Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
	}

	inline static Vector3 VecScalarMult(const Vector3 &v, float mult)
	{
		return Vector3(v.X * mult, v.Y * mult, v.Z * mult);
	}

	inline static void NormalizedQuaternionToMatrix33(GLfloat *q, GLfloat *m)
	{
		m[0] = 1.0f - 2.0f*q[2]*q[2] - 2.0f*q[3]*q[3]; 
		m[1] = 2.0f*q[1]*q[2] + 2.0f*q[3]*q[0]; 
		m[2] = 2.0f*q[1]*q[3] - 2.0f*q[2]*q[0];

		m[3] = 2.0f*q[1]*q[2] - 2.0f*q[3]*q[0]; 
		m[4] = 1.0f - 2.0f*q[1]*q[1] - 2.0f*q[3]*q[3]; 
		m[5] = 2.0f*q[2]*q[3] + 2.0f*q[1]*q[0];

		m[6] = 2.0f*q[1]*q[3] + 2.0f*q[2]*q[0]; 
		m[7] = 2.0f*q[2]*q[3] - 2.0f*q[1]*q[0]; 
		m[8] = 1.0f - 2.0f*q[1]*q[1] - 2.0f*q[2]*q[2];
	}

	inline static Matrix33 NormalizedQuaternionToMatrix33(Quaternion q)
	{
		Matrix33 result;

		result.v11 = 1.0f - 2.0f*q.Y*q.Y - 2.0f*q.Z*q.Z; 
		result.v12 = 2.0f*q.X*q.Y + 2.0f*q.Z*q.W; 
		result.v13 = 2.0f*q.X*q.Z - 2.0f*q.Y*q.W;

		result.v21 = 2.0f*q.X*q.Y - 2.0f*q.Z*q.W; 
		result.v22 = 1.0f - 2.0f*q.X*q.X - 2.0f*q.Z*q.Z; 
		result.v23 = 2.0f*q.Y*q.Z + 2.0f*q.X*q.W;

		result.v31 = 2.0f*q.X*q.Z + 2.0f*q.Y*q.W; 
		result.v32 = 2.0f*q.Y*q.Z - 2.0f*q.X*q.W; 
		result.v33 = 1.0f - 2.0f*q.X*q.X - 2.0f*q.Y*q.Y;

		return result;
	}

	inline static void SetRotationFromMatrix(GLfloat *mIn, GLfloat *mOut)
	{
		mOut[0] = mIn[0];
		mOut[1] = mIn[1];
		mOut[2] = mIn[2];

		mOut[4] = mIn[3];
		mOut[5] = mIn[4];
		mOut[6] = mIn[5];

		mOut[8] = mIn[6];
		mOut[9] = mIn[7];
		mOut[10] = mIn[8];
	}

	inline static void SetRotationFromNormalizedQuaternion(GLfloat *q, GLfloat *m)
	{
		GLfloat rotationMatrix[9];
		NormalizedQuaternionToMatrix33(q, rotationMatrix);
		SetRotationFromMatrix(rotationMatrix, m);
	}

	inline static void NormalizeQuaternion(GLfloat *q)
	{
		float magnitude = sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
		q[0] = q[0] / magnitude;
		q[1] = q[1] / magnitude;
		q[2] = q[2] / magnitude;
		q[3] = q[3] / magnitude;
	}

	inline static Vector3 Normalize(const Vector3 &vec)
	{
		float magnitude = sqrt(vec.X*vec.X + vec.Y*vec.Y + vec.Z*vec.Z);
		return Vector3(
			vec.X / magnitude,
			vec.Y / magnitude,
			vec.Z / magnitude);
	}

	inline static float GetRandomNumber(float min, float max)
	{
		float random = (float)rand() / (float)RAND_MAX;
		return random * (max - min) + min;
	}

	inline static void MultiplyQuaternions(GLfloat *quatA, GLfloat *quatB, GLfloat *result)
	{
		GLfloat qa[] = {quatA[0], quatA[1], quatA[2], quatA[3]};
		GLfloat qb[] = {quatB[0], quatB[1], quatB[2], quatB[3]};

		result[0] = qa[0]*qb[0] - qa[1]*qb[1] - qa[2]*qb[2] - qa[3]*qb[3];
		result[1] = qa[0]*qb[1] + qa[1]*qb[0] + qa[2]*qb[3] - qa[3]*qb[2];
		result[2] = qa[0]*qb[2] + qa[2]*qb[0] + qa[3]*qb[1] - qa[1]*qb[3];
		result[3] = qa[0]*qb[3] + qa[3]*qb[0] + qa[1]*qb[2] - qa[2]*qb[1];
	}

	inline static Quaternion MultiplyQuaternions(Quaternion qb, Quaternion qa)
	{
		Quaternion product;

		product.W = qa.W*qb.W - qa.X*qb.X - qa.Y*qb.Y - qa.Z*qb.Z;
		product.X = qa.W*qb.X + qa.X*qb.W + qa.Y*qb.Z - qa.Z*qb.Y;
		product.Y = qa.W*qb.Y + qa.Y*qb.W + qa.Z*qb.X - qa.X*qb.Z;
		product.Z = qa.W*qb.Z + qa.Z*qb.W + qa.X*qb.Y - qa.Y*qb.X;

		return product;
	}

	// Note that the conjugate is also the inverse in the case of normalized quaternions
	inline static Quaternion GetQuaternionConjugate(Quaternion q)
	{
		return Quaternion(q.W, -q.X, -q.Y, -q.Z);
	}

	inline static Quaternion ExponentiateQuaternion(Quaternion q, float exponent)
	{
		float alpha = acos(q.W);
		float newAlpha = alpha * exponent;
		float mult = sin(newAlpha) / sin(alpha);
		return Quaternion(cos(newAlpha), q.X * mult, q.Y * mult, q.Z * mult);
	}

	inline static Quaternion GetInterpolatedQuaternion(Quaternion q0, Quaternion q1, float interpolationFactor)
	{
		Quaternion q0conj = GetQuaternionConjugate(q0);
		Quaternion diff = MultiplyQuaternions(q0conj, q1);
		Quaternion delta = ExponentiateQuaternion(diff, interpolationFactor);
		Quaternion final = MultiplyQuaternions(q0, delta);
		return final;
	}

	inline static Matrix33 InterpolateQuaternionsToMatrix(Quaternion q0, Quaternion q1, float interpolationFactor)
	{
		return NormalizedQuaternionToMatrix33(Slerp(q0, q1, interpolationFactor));
	}

	inline static Matrix33 MultiplyMatrices(Matrix33 mA, Matrix33 mB)
	{
		Matrix33 result;
		
		result.v11 = mA.v11*mB.v11 + mA.v12*mB.v21 + mA.v13*mB.v31;
		result.v12 = mA.v11*mB.v12 + mA.v12*mB.v22 + mA.v13*mB.v32;
		result.v13 = mA.v11*mB.v13 + mA.v12*mB.v23 + mA.v13*mB.v33;
		
		result.v21 = mA.v21*mB.v11 + mA.v22*mB.v21 + mA.v23*mB.v31;
		result.v22 = mA.v21*mB.v12 + mA.v22*mB.v22 + mA.v23*mB.v32;
		result.v23 = mA.v21*mB.v13 + mA.v22*mB.v23 + mA.v23*mB.v33;
		
		result.v31 = mA.v31*mB.v11 + mA.v32*mB.v21 + mA.v33*mB.v31;
		result.v32 = mA.v31*mB.v12 + mA.v32*mB.v22 + mA.v33*mB.v32;
		result.v33 = mA.v31*mB.v13 + mA.v32*mB.v23 + mA.v33*mB.v33;

		return result;
	}

	inline static void InitIdentityMatrix44(float *m)
	{
		m[0]  = 1.0f; m[1]  = 0.0f; m[2]  = 0.0f; m[3]  = 0.0f;
		m[4]  = 0.0f; m[5]  = 1.0f; m[6]  = 0.0f; m[7]  = 0.0f;
		m[8]  = 0.0f; m[9]  = 0.0f; m[10] = 1.0f; m[11] = 0.0f;
		m[12] = 0.0f; m[13] = 0.0f; m[14] = 0.0f; m[15] = 1.0f;
	}

	inline static void Matrix33StructToMatrixArray44(Matrix33 m, float *mOut)
	{
		mOut[0] = m.v11;
		mOut[1] = m.v12;
		mOut[2] = m.v13;
		mOut[3] = 0.0f;

		mOut[4] = m.v21;
		mOut[5] = m.v22;
		mOut[6] = m.v23;
		mOut[7] = 0.0f;

		mOut[8] = m.v31;
		mOut[9] = m.v32;
		mOut[10] = m.v33;
		mOut[11] = 0.0f;

		mOut[12] = 0.0f;
		mOut[13] = 0.0f;
		mOut[14] = 0.0f;
		mOut[15] = 1.0f;
	}

	inline static int Min(int a, int b)
	{
		return a < b ? a : b;
	}

	inline static float Min(float a, float b)
	{
		return a < b ? a : b;
	}

	inline static int Max(int a, int b)
	{
		return a > b ? a : b;
	}

	inline static float Max(float a, float b)
	{
		return a > b ? a : b;
	}

	inline static float DotProduct(const Vector3 &a, const Vector3 &b)
	{
		return a.X*b.X + a.Y*b.Y + a.Z*b.Z;
	}

	inline static Vector3 VecDiff(const Vector3 &a, const Vector3 &b)
	{
		return Vector3(a.X-b.X, a.Y-b.Y, a.Z-b.Z);
	}

	inline static float GetDistanceBetweenPoints(const Vector3 &p1, const Vector3 &p2)
	{
		float xd = p1.X - p2.X;
		float yd = p1.Y - p2.Y;
		float zd = p1.Z - p2.Z;
		return sqrt(xd*xd + yd*yd + zd*zd);
	}

	inline static float VecLength(const Vector3 &v)
	{
		return sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z);
	}

	inline static bool CalcRayPlaneCollision(const Ray &ray, const Plane &plane, Vector3 &collisionPoint)
	{
		throw "not implemented";
		//float d = VecDiff(
	}

	inline static Vector3 GetNormalizedVector(Vector3 &vec)
	{
		float length = sqrt(vec.X*vec.X + vec.Y*vec.Y + vec.Z*vec.Z);
		return Vector3(vec.X / length, vec.Y / length, vec.Z / length);
	}

	static float RayTriangleIntersection(const Vector3 &rayOrig, const Vector3 &rayDelta, const Vector3 &p0, const Vector3 &p1, const Vector3 &p2, float minT);

	static CollisionResult RayCylinderIntersection(const Ray &ray, const Vector3 &cylinderOrigin, const Vector3 &cylinderDirection, float cylinderRadius, CollisionType type);

	static CollisionResult RaySphereIntersection(const Ray &ray, const Vector3 &center, float radius, CollisionType type);

	inline static Quaternion Slerp(Quaternion q0, Quaternion q1, float t)
	{
		float cosOmega = q0.X*q1.X + q0.Y*q1.Y + q0.Z*q1.Z + q0.W*q1.W;
		if (cosOmega < 0.0f)
		{
			q1.X = -q1.X;
			q1.Y = -q1.Y;
			q1.Z = -q1.Z;
			q1.W = -q1.W;
			cosOmega = -cosOmega;
		}
		float k0, k1;
		if (cosOmega > 0.9999f)
		{
			k0 = 1.0f - t;
			k1 = t;
		}
		else
		{
			float sinOmega = sqrt(1.0f - cosOmega*cosOmega);
			float omega = atan2(sinOmega, cosOmega);
			float oneOverSinOmega = 1.0f / sinOmega;
			k0 = sin((1.0f - t) * omega) * oneOverSinOmega;
			k1 = sin(t * omega) * oneOverSinOmega;
		}
		return Quaternion(
			q0.W*k0 + q1.W*k1,
			q0.X*k0 + q1.X*k1,
			q0.Y*k0 + q1.Y*k1,
			q0.Z*k0 + q1.Z*k1);
	}

	inline static int FloorZeroInt(float f)
	{
		if (f < 0.0f)
			return (int)ceil(f);
		else
			return (int)floor(f);
	}

	inline static Vector3 MatVecMult(Matrix33 m, Vector3 v)
	{
		return Vector3(
			m.v11 * v.X + m.v21 * v.Y + m.v31 * v.Z,
			m.v12 * v.X + m.v22 * v.Y + m.v32 * v.Z,
			m.v13 * v.X + m.v23 * v.Y + m.v33 * v.Z);
	}

	inline static float HeadingFromDirection(Vector3 dir)
	{
		return -atan2(-dir.X, -dir.Z);
	}

	inline static float PitchFromDirection(Vector3 dir)
	{
		return -atan2(-dir.Y, sqrt(dir.X*dir.X + dir.Z*dir.Z));
	}
};

