#pragma once

#include "MyOpenGL.h"
#include <map>
#include <string>

class TextureManager
{
public:
	TextureManager();
	~TextureManager();
	GLuint GetTexture(std::string name) { return _textureMap->at(name); }
	GLuint CreateTexture(std::string name, GLubyte pixels[], unsigned int width, unsigned int height, GLint format, GLenum formatType);
	GLuint CreateFloatTexture2d(std::string name, GLfloat *data, unsigned int minWidth, unsigned int minHeight, GLint format, GLenum formatType, int floatsPerPixel);
	GLuint CreateTextureFromFile(std::string fileName, std::string name, GLint format, GLenum formatType);
	void DestroyTexture(std::string name);
	void DestroyAllTextures();

private:
	std::map<std::string, unsigned int> *_textureMap;
	unsigned int RoundToNextPowerOfTwo(unsigned int value);
};