#pragma once

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>
#include "BoneData.h"
#include "MyOpenGL.h"
#include "CommonStructs.h"

class StaticMeshData
{
public:
	StaticMeshData(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount);
	~StaticMeshData();
	static StaticMeshData *LoadFromFile(std::string fileName);

	float *VertexData;
	int VertexCount;
	GLushort *IndexData;
	int TriangleCount;

private:
	static int GetIntFromHeaderLine(std::string line);
	static Vector3 GetVertexFromLine(std::string line);
	static Face GetTriangleFromLine(std::string line);

	static std::vector<std::string> &SplitString(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> SplitString(const std::string &s, char delim);
};