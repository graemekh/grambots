#pragma once

#ifndef NULL
#define NULL 0
#endif

struct Vector3
{
	Vector3()
	{
		X = 0;
		Y = 0;
		Z = 0;
	}

    Vector3(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    float X;
    float Y;
    float Z;
};

struct Vector2
{
	Vector2()
	{
		X = 0;
		Y = 0;
	}

	Vector2(float x, float y)
	{
		X = x;
		Y = y;
	}

	float X;
	float Y;
};

struct TexCoord
{
	TexCoord()
	{
		U = 0;
		V = 0;
	}

	TexCoord(float uVal, float vVal)
	{
		U = uVal;
		V = vVal;
	}

	float U, V;
};

struct Quaternion
{
	Quaternion()
	{
		W = 1.0f;
		X = 0.0f;
		Y = 0.0f;
		Z = 0.0f;
	}

	Quaternion(float w, float x, float y, float z)
	{
		W = w;
		X = x;
		Y = y;
		Z = z;
	}

	float W, X, Y, Z;
};

struct Matrix33
{
	Matrix33()
	{
		v11 = 1.0f; v12 = 0.0f; v13 = 0.0f;
		v21 = 0.0f; v22 = 1.0f; v23 = 0.0f;
		v31 = 0.0f; v32 = 0.0f; v33 = 1.0f;
	}

	Matrix33(float _11, float _12, float _13, float _21, float _22, float _23, float _31, float _32, float _33)
	{
		v11 = _11; v12 = _12; v13 = _13;
		v21 = _21; v22 = _22; v23 = _23;
		v31 = _31; v32 = _32; v33 = _33;
	}

	float v11, v12, v13, v21, v22, v23, v31, v32, v33;
};

struct Face
{
	int v1, v2, v3;
};

struct Color
{
	Color()
	{
		R = 0.0f;
		G = 0.0f;
		B = 0.0f;
	}

	Color(float r, float g, float b)
	{
		R = r;
		G = g;
		B = b;
	}

	float R, G, B;
};

struct Ray
{
	Ray(Vector3 startPoint, Vector3 directionVector, float rayLength)
	{
		start = startPoint;
		direction = directionVector;
		length = rayLength;
	}

	Ray()
	{
		length = 0.0f;
	}

	Vector3 start;
	Vector3 direction;
	float length;
};

struct Plane
{
	Plane(Vector3 planePoint, Vector3 planeNormal)
	{
		point = planePoint;
		normal = planeNormal;
	}

	Vector3 point;
	Vector3 normal;
};

struct Triangle
{
	Triangle(Vector3 vert1, Vector3 vert2, Vector3 vert3)
	{
		v1 = vert1;
		v2 = vert2;
		v3 = vert3;
	}

	Triangle()
	{
	}

	Vector3 v1;
	Vector3 v2;
	Vector3 v3;
};

enum CollisionType
{
	NoCollision = 0x01,
	MapCollision = 0x02,
	BotCollision = 0x04
};

struct CollisionResult
{
	CollisionResult()
	{
		collisionType = NoCollision;
		object = NULL;
	}

	CollisionResult(CollisionType type)
	{
		collisionType = type;
		distance = 0.0f;
		object = NULL;
	}

	CollisionResult(CollisionType type, Vector3 point, float distanceAlongRay)
	{
		collisionType = type;
		collisionPoint = point;
		distance = distanceAlongRay;
		object = NULL;
	}

	CollisionType collisionType;
	Vector3 collisionPoint;
	float distance;
	void *object;
};