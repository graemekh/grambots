#pragma once
#pragma inline_recursion(on)

#include "MyOpenGL.h"
#include "AnimatedMeshData.h"
#include "Bone.h"

class AnimatedMesh
{
public:
    AnimatedMesh(AnimatedMeshData *data);
    ~AnimatedMesh();
    void Render();
	//void UpdateMesh();

	inline float *GetVertexData() { return _vertexData; }
	inline float *GetOriginalVertexData() { return _originalVertexData; }

	AnimatedMeshData *Data;
	Bone *RootBone;

private:
	//inline void UpdateMeshToBoneAtFrame(Bone *bone);
    GLuint _vertexBufferId;
    GLuint _indexBufferId;
	GLuint _normalBufferId;

    int _polygonCount;
	int _vertexCount;
	float *_originalVertexData;
	float *_vertexData;
};