#include "Font.h"
#include <vector>

Font::Font(std::vector<CharInfo> characters, GLuint textureId, int textureWidth, int textureHeight, int charWidth, int charHeight)
{
	_characters = characters;
	_textureId = textureId;
	_textureWidth = textureWidth;
	_textureHeight = textureHeight;
	_charWidth = charWidth;
	_charHeight = charHeight;
}

