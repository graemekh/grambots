#pragma once

#include <vector>
#include <string>
#include <memory>
#include "Game.h"
#include "Bone.h"
#include "CommonStructs.h"
#include "Renderable.h"
#include "StaticMesh.h"
#include "AnimatedMeshData.h"
#include "StaticMeshData.h"
#include "ShaderManager.h"
#include "Laser.h"
#include "Team.h"

#define BONE_FRAME_MATRICES_TEX_OFFSET 1
#define BONE_FRAME_QUATERNIONS_TEX_OFFSET 2

#define GUY_Y_OFFSET 3.8f
#define STOP_TIME 100
#define BOT_WALL_HIT_RADIUS 1.8f
#define BOT_BOUNDING_RADIUS 4.0f
#define FALL_THRESHOLD 0.5f
#define FALL_ACCELERATION 100.0f

#define MAX_PATH_SEGMENTS 1000

class Bot;
class Team;

struct Path
{
	Path()
	{
		currentSegment = 0;
		segmentCount = 0;
	}

	int currentSegment;
	int segmentCount;
	bool reachesFinalDestination;
	Vector2 points[MAX_PATH_SEGMENTS];
};

struct BotState
{
	Vector3 position;
};

struct BotCommand
{
	bool doNothing;
	bool isTravelling;
	Vector2 finalDestination;
	Bot *target;
	Path path;
};

struct AiFrame
{
	AiFrame()
	{
		isActive = false;
		isComplete = false;
	}

	volatile bool isActive;
	volatile bool isComplete;
	BotCommand command;
};

class BotStateBuffer
{
public:
	BotStateBuffer();
	BotState GetActiveFrame();
	void SetActiveFrame(BotState state);
private:
	BotState _frameA;
	BotState _frameB;
	volatile bool _usingFrameA;
};

class AiFrameBuffer
{
public:
	AiFrameBuffer();
	~AiFrameBuffer();
	AiFrame *UpdateAndReturnActiveFrame();
	AiFrame *GetActiveFrame();
	AiFrame *GetInactiveFrame();

private:
	AiFrame *_frameA;
	AiFrame *_frameB;
	volatile bool _usingFrameA;
};

class Bot
{
public:
	Bot(Game *game, AnimatedMeshData *bodyMeshData, Team *team);
	~Bot();

	void StartRunning();
	void StopRunning();
	void HandleEvent(Uint8 *keyState, float deltaTime);
	void Update(float deltaTime);
	void ExecuteAiFrame(float deltaTime);
	void Move(float deltaX, float deltaZ);
	void SetHeading(float newHeading) { _heading = newHeading; }
	float GetHeading() { return _heading; }

	CollisionResult CalculateRayCollision(const Ray &ray);

	inline Vector3 GetPosition() { return _position; }

	inline void SetPosition(Vector3 position)
	{
		position.Y += GUY_Y_OFFSET;
		_position = position;
		UpdateCell();
	}

	void SetPositionXZ(float x, float z);
	void Die() { _isRagdolling = true; };
	void SetIsSelected(bool value) { _isSelected = value; };
	bool GetIsSelected() { return _isSelected; };
	Team *GetTeam() { return _team; }
	AiFrameBuffer *GetAiFrameBuffer() { return &_aiFrameBuffer; }
	BotStateBuffer GetStateBuffer() { return _botStateBuffer; };

	volatile bool GetIsActive() { return _isActive; }
	void SetIsActive(bool isActive) { _isActive = isActive; }

	void Render();
	LaserInfo CreateNewLaserShotFromGun();

	static CollisionResult RayBotCollisionWithinCell(const Ray &ray, int cellIndex, void *objToIgnore);
	static CollisionResult RayBotCollision(const Ray &ray, void *objToIgnore);
	static void InitBotCells(float worldWidth, float worldDepth);

	static void DestroySharedResources();
private:
	volatile bool _isActive;
	static void DestroyBotCells();

	BotStateBuffer _botStateBuffer;
	void RefreshBotState();

	bool _isRagdolling;
	bool _hasStartedRagdoll;
	Bone **_bones;
	int _boneCount;
	float *_boneRotationOverrides;
	void UpdateNormalBoneRotationOverrides();
	void UpdateRagdollBoneRotationOverrides(float deltaTime);
	void InitBoneRotationOverrides();
	bool GetClockwiseMovesDown(Vector3 rotationVec, Vector3 direction);

	AiFrameBuffer _aiFrameBuffer;

	Team *_team;
	bool _isSelected;

	Bot *_nextBotInCell;
	Bot *_previousBotInCell;
	int _cellIndex;
	static Bot **_cells;
	static int _cellsAcross;
	static float _cellWidth;
	void UpdateCell();

	bool _isShooting;
	Vector3 RotateDirectionWithBoneUsingOverrides(Bone *bone, Vector3 vert);
	Vector3 RotateVertWithBone(Bone *bone, Vector3 vert, int frameFrom, int frameTo, float interpolationFactor);
	float GetOutlineThickness();

	Vector3 _position;
	float _heading;
	float _pitch;
	Game *_game;
	StaticMesh *_bodyMesh;
	bool _isRunning;
	bool _isFalling;
	float _fallSpeed;

	float _lowerBodyFrameTo;
	float _upperBodyFrameTo;
	float _lowerBodyFrameFrom;
	float _upperBodyFrameFrom;
	float _lowerBodyInterpolationFactor;
	float _upperBodyInterpolationFactor;

	Bone *_boneLegUpperLeft;
	Bone *_boneLegUpperRight;
	Bone *_boneLegLowerLeft;
	Bone *_boneLegLowerRight;
	Bone *_boneBackLower;
	Bone *_boneBackUpper;
	Bone *_boneHead;
	Bone *_boneArmUpperRight;
	Bone *_boneArmUpperLeft;
	Bone *_boneArmLowerRight;
	Bone *_boneArmLowerLeft;
	Bone *_boneHandRight;
	Bone *_boneHandLeft;
	Bone *_boneRoot;

	float _timeUntilFullStop;
	bool _wasRunningLastFrame;
	Vector3 _gunBarrelVert;

	static GLuint _shaderProgramId;
	static GLint _outlineColorUniLoc;
	static GLint _outlineThicknessUniLoc;
	static GLint _bonePositionsUniLoc;
	static GLint _boneFrameMatricesUniLoc;
    static GLint _boneFrameQuaternionsUniLoc;
	static GLint _upperBodyFrameToUniLoc;
	static GLint _lowerBodyFrameToUniLoc;
    static GLint _upperBodyFrameFromUniLoc;
    static GLint _lowerBodyFrameFromUniLoc;
    static GLint _upperBodyInterpolationFactorUniLoc;
    static GLint _lowerBodyInterpolationFactorUniLoc;
	static GLint _bonePathsUniLoc;
	static GLint _animationModeUniLoc;
	static GLint _boneOverridesUniLoc;
	static GLint _bonePathPositionAttrLoc;
	static GLint _bodySectionAttrLoc;
	static GLint _boneFrameMatricesTextureId;
    static GLint _boneFrameQuaternionsTextureId;
	static bool _staticInitializationComplete;
	static void InitShader(AnimatedMeshData *data, Game *game);
	static GLuint _vertBonePositionsBuffer;
	static GLuint _vertBodySectionBuffer;

	static StaticMesh *_markerMesh;
	static void InitMarkerMesh(Game *game);
};

