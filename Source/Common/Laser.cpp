#include "MyOpenGL.h"
#include "Laser.h"
#include "CommonStructs.h"
#include "GMath.h"

Laser::Laser(GLuint shaderProgramId)
{
	float width = 0.4f;
	float height = 0.4f;
	float depth = LASER_BEAM_LENGTH;
	_shaderProgramId = shaderProgramId;

    float vertices[] = {
        -width/2,  height/2, depth,  // 0
         width/2,  height/2, depth,  // 1
         width/2, -height/2, depth,  // 2
        -width/2, -height/2, depth,  // 3
        -width/2,  height/2, 0,  // 4
         width/2,  height/2, 0,  // 5
         width/2, -height/2, 0,  // 6
        -width/2, -height/2, 0   // 7
    };

    GLushort indices[] = {
        0, 3, 2, 1, // front
        5, 6, 7, 4, // back
        1, 2, 6, 5, // right
        4, 7, 3, 0, // left
        4, 0, 1, 5, // top
        3, 7, 6, 2  // bottom
    };

    _mesh = StaticMesh::CreateQuadMesh(vertices, 8, indices, 6);

}

Laser::~Laser()
{
    delete _mesh;
}

void Laser::Render()
{
	glEnable(GL_BLEND);
	glUseProgram(_shaderProgramId);
	glFrontFace(GL_CCW);
	glPolygonMode( GL_FRONT, GL_FILL);
    _mesh->Render();
	glUseProgram(NULL);
	glDisable(GL_BLEND);
}