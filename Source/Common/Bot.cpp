#include "Bot.h"

#define TOP_OF_WORLD 1000.0f
#define BOTTOM_OF_WORLD -50.0f
#define CELL_PADDING 50

// oh my god static initializers are ugly in c++
bool Bot::_staticInitializationComplete = false;
StaticMesh *Bot::_markerMesh = NULL;
GLuint Bot::_shaderProgramId;
GLint Bot::_outlineColorUniLoc;
GLint Bot::_outlineThicknessUniLoc;
GLint Bot::_bonePositionsUniLoc;
GLint Bot::_boneFrameMatricesUniLoc;
GLint Bot::_boneFrameQuaternionsUniLoc;
GLint Bot::_upperBodyFrameToUniLoc;
GLint Bot::_upperBodyFrameFromUniLoc;
GLint Bot::_upperBodyInterpolationFactorUniLoc;
GLint Bot::_lowerBodyFrameToUniLoc;
GLint Bot::_lowerBodyFrameFromUniLoc;
GLint Bot::_lowerBodyInterpolationFactorUniLoc;
GLint Bot:: _bonePathsUniLoc;
GLint Bot::_animationModeUniLoc;
GLint Bot::_boneOverridesUniLoc;
GLint Bot::_bonePathPositionAttrLoc;
GLint Bot::_bodySectionAttrLoc;
GLuint Bot::_vertBonePositionsBuffer;
GLuint Bot::_vertBodySectionBuffer;
GLint Bot::_boneFrameMatricesTextureId;
GLint Bot::_boneFrameQuaternionsTextureId;

Bot **Bot::_cells;
float Bot::_cellWidth;
int Bot::_cellsAcross;

Bot::Bot(Game *game, AnimatedMeshData *bodyMeshData, Team *team)
{
	_isActive = false;
	_team = team;
	_isRagdolling = false;
	_hasStartedRagdoll = false;
	_position = Vector3(0.0f, 0.0f, 0.0f);
	_game = game;
	_isShooting = false;
	_bodyMesh = new StaticMesh(bodyMeshData->VertexData, bodyMeshData->VertexCount, bodyMeshData->IndexData, bodyMeshData->TriangleCount, 3, GL_TRIANGLES, NULL, NULL, 0, bodyMeshData->NormalData);
	_isRunning = false;
	_heading = 0.0f;
	_pitch = 0.5f;
	_timeUntilFullStop = 0.0f;
	_wasRunningLastFrame = false;
	_isFalling = false;
	_fallSpeed = 0.0f;
	_nextBotInCell = NULL;
	_previousBotInCell = NULL;
	_cellIndex = -1;
	_boneRotationOverrides = new float[bodyMeshData->BoneCount * 4];
	_isSelected = false;
	_lowerBodyFrameFrom = 0.0f;
	_lowerBodyFrameTo = 0.0f;
	_lowerBodyInterpolationFactor = 1.0f;

	_upperBodyFrameFrom = 0.0f;
	_upperBodyFrameTo = 0.0f;
	_upperBodyInterpolationFactor = 1.0f;

	RefreshBotState();

	int gunBarrelVertIndex = bodyMeshData->SpecialVertices.at("gunBarrel");
	_gunBarrelVert = Vector3(
		bodyMeshData->VertexData[gunBarrelVertIndex * 3 + 0],
		bodyMeshData->VertexData[gunBarrelVertIndex * 3 + 1],
		bodyMeshData->VertexData[gunBarrelVertIndex * 3 + 2]);

	//_shaderProgramId = game->GetShaderManager()->GetShaderProgramId("test");

	// for convenience get named references to each bone
	_boneRoot = new Bone(bodyMeshData->RootBone, NULL);
	_boneBackLower = _boneRoot->GetChildren()[0];
	_boneLegUpperLeft = _boneRoot->GetChildren()[2];
	_boneLegUpperRight = _boneRoot->GetChildren()[1];
	_boneLegLowerLeft = _boneLegUpperLeft->GetChildren()[0];
	_boneLegLowerRight = _boneLegUpperRight->GetChildren()[0];
	_boneBackUpper = _boneBackLower->GetChildren()[0];
	_boneHead = _boneBackUpper->GetChildren()[2];
	_boneArmUpperRight = _boneBackUpper->GetChildren()[0];
	_boneArmUpperLeft = _boneBackUpper->GetChildren()[1];
	_boneArmLowerRight = _boneArmUpperRight->GetChildren()[0];
	_boneArmLowerLeft = _boneArmUpperLeft->GetChildren()[0];
	_boneHandRight = _boneArmLowerRight->GetChildren()[0];
	_boneHandLeft = _boneArmLowerLeft->GetChildren()[0];

	_boneCount = 14;
	_bones = new Bone *[_boneCount];
	_bones[0] = _boneRoot;
	_bones[1] = _boneBackLower;
	_bones[2] = _boneBackUpper;
	_bones[3] = _boneArmUpperRight;
	_bones[4] = _boneArmLowerRight;
	_bones[5] = _boneHandRight;
	_bones[6] = _boneArmUpperLeft;
	_bones[7] = _boneArmLowerLeft;
	_bones[8] = _boneHandLeft;
	_bones[9] = _boneHead;
	_bones[10] = _boneLegUpperRight;
	_bones[11] = _boneLegLowerRight;
	_bones[12] = _boneLegUpperLeft;
	_bones[13] = _boneLegLowerLeft;

	// initialize index of each bone
	for (int i = 0; i < _boneCount; i++)
		_bones[i]->SetIndex(i);

	this->InitBoneRotationOverrides();

	if (!_staticInitializationComplete)
	{
		InitShader(bodyMeshData, game);
		InitMarkerMesh(game);
	}
	_staticInitializationComplete = true;

	UpdateCell();
}

Bot::~Bot()
{
	delete[] _boneRotationOverrides;
	delete _boneRoot; // this triggers deletion of children
	delete _bodyMesh;
}

/* static */ void Bot::InitShader(AnimatedMeshData *data, Game *game)
{
	ShaderManager *shaderManager = game->GetShaderManager();
	std::string errorMsg;

	///////////////////////////////////////////////////////////////////////////
	// Pre-calculate data that will need to be sent to shader.  Some of this
	// needs to be known before even loading the shader because some text
	// substitutions need to be executed on the glsl code.
	///////////////////////////////////////////////////////////////////////////

	// initial bone overrides, just make them all something that will be easily noticable if I accidentally use them
	float *initialOverrides = new float[data->BoneCount * 4];
	for (int i = 0; i < data->BoneCount; i++)
	{
		initialOverrides[i * 4 + 0] = 0.0f;
		initialOverrides[i * 4 + 1] = 0.0f;
		initialOverrides[i * 4 + 2] = 0.0f;
		initialOverrides[i * 4 + 3] = 1.0f;
	}

	// Setup global bone positions (these should never change)
	float *bonePosData = new float[data->BoneCount * 3];
	for (int i = 0; i < data->BoneCount; i++)
	{
		Vector3 pos = data->Bones[i]->Head;
		bonePosData[i * 3 + 0] = pos.X;
		bonePosData[i * 3 + 1] = pos.Y;
		bonePosData[i * 3 + 2] = pos.Z;
	}

    // Setup global animation data in matrix and quaternion form (this should never change)
	GLfloat *boneFrameMatrices = new GLfloat[data->BoneCount * data->FrameCount * 9];
    GLfloat *boneFrameQuaternions = new GLfloat[data->BoneCount * data->FrameCount * 4];
    for (int b = 0; b < data->BoneCount; b++)
	{
		BoneData *bone = data->Bones[b];
		for (int f = 0; f < data->FrameCount; f++)
		{
			Matrix33 m = bone->MatrixFrames[f];
            Quaternion q = bone->Frames[f];

			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 0] = m.v11;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 1] = m.v12;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 2] = m.v13;

			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 3] = m.v21;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 4] = m.v22;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 5] = m.v23;

			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 6] = m.v31;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 7] = m.v32;
			boneFrameMatrices[(b * data->FrameCount * 9) + (9 * f) + 8] = m.v33;

            boneFrameQuaternions[(b * data->FrameCount * 4) + (4 * f) + 0] = q.X;
            boneFrameQuaternions[(b * data->FrameCount * 4) + (4 * f) + 1] = q.Y;
            boneFrameQuaternions[(b * data->FrameCount * 4) + (4 * f) + 2] = q.Z;
            boneFrameQuaternions[(b * data->FrameCount * 4) + (4 * f) + 3] = q.W;
		}
	}

	// init bone paths global uniform
	std::vector<int> bonePathsVec;
	for (int i = 0; i < data->BoneCount; i++)
	{
		BoneData *bone = data->Bones[i];
		if (bone->ChildCount == 0) // only care about leaf nodes
		{
			for (int j = bone->PathSize - 1; j >= 0; j--)
			{
				bonePathsVec.push_back(bone->Path[j]->Index);
			}
		}
	}
	int *bonePaths = new int[bonePathsVec.size()];
	for (unsigned int i = 0; i < bonePathsVec.size(); i++)
		bonePaths[i] = bonePathsVec[i];

	// init bone path position for vertex
	float *vertBonePositions = new float[data->VertexCount];
	for (int boneIndex = 0; boneIndex < data->BoneCount; boneIndex++)
	{
		BoneData *bone = data->Bones[boneIndex];
		for (int boneVertIndex = 0; boneVertIndex < bone->DirectVertexCount; boneVertIndex++)
		{
			int vertIndex = bone->DirectVertices[boneVertIndex];
			for (unsigned int bonePathPosition = 0; bonePathPosition < bonePathsVec.size(); bonePathPosition++)
			{
				if (bonePathsVec[bonePathPosition] == boneIndex)
				{
					vertBonePositions[vertIndex] = ((float)bonePathPosition) + 0.01f;
					break;
				}
			}
		}
	}

	// init vertex body sections (0 = upper body, 1 = lower body)
	float *vertBodySections = new float[data->VertexCount];
	for (int boneIndex = 0; boneIndex < data->BoneCount; boneIndex++)
	{
		BoneData *bone = data->Bones[boneIndex];
		for (int boneVertIndex = 0; boneVertIndex < bone->DirectVertexCount; boneVertIndex++)
		{
			int vertIndex = bone->DirectVertices[boneVertIndex];
			vertBodySections[vertIndex] = boneIndex < 10 ? 0.01f : 1.01f;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Create the actual shader and perform text substitutions on glsl code
	///////////////////////////////////////////////////////////////////////////

	std::vector<std::pair<std::string, std::string> > substitutions;

	char boneCountStr[50];
    char bonePathsLengthStr[50];
    snprintf(boneCountStr, 50, "%d", data->BoneCount);
    snprintf(bonePathsLengthStr, 50, "%d", (int)bonePathsVec.size());
    substitutions.push_back(std::pair<std::string, std::string>("__BONE_COUNT__", boneCountStr));
    substitutions.push_back(std::pair<std::string, std::string>("__BONE_PATHS_LENGTH__", bonePathsLengthStr));

	_shaderProgramId = shaderManager->LoadShaders("AnimGuy",
                                                  Assets::GetAssetPath("Shaders/AnimGuyVertex.glsl"),
                                                  Assets::GetAssetPath("Shaders/AnimGuyPixel.glsl"),
                                                  substitutions);

	///////////////////////////////////////////////////////////////////////////
	// Send data to shader
	///////////////////////////////////////////////////////////////////////////
	glUseProgram(_shaderProgramId);

	_animationModeUniLoc = glGetUniformLocation(_shaderProgramId, "animationMode");
	glUniform1i(_animationModeUniLoc, 1);

	_boneOverridesUniLoc = glGetUniformLocation(_shaderProgramId, "boneOverrides");
	glUniform4fv(_boneOverridesUniLoc, data->BoneCount, initialOverrides);
	delete[] initialOverrides;

	_bonePositionsUniLoc = glGetUniformLocation(_shaderProgramId, "bonePositions");
	glUniform3fv(_bonePositionsUniLoc, data->BoneCount, bonePosData);
	delete[] bonePosData;

	_boneFrameMatricesTextureId = game->GetTextureManager()->CreateFloatTexture2d(
			"boneFrameMatrices", boneFrameMatrices, data->FrameCount * 3, data->BoneCount, GL_RGB32F_ARB, GL_RGB, 3);
	_boneFrameMatricesUniLoc = glGetUniformLocation(_shaderProgramId, "boneFrameMatrices");
	glActiveTexture(GL_TEXTURE0 + BONE_FRAME_MATRICES_TEX_OFFSET);
	glBindTexture(GL_TEXTURE_2D, _boneFrameMatricesTextureId);
	glUniform1i(_boneFrameMatricesUniLoc, BONE_FRAME_MATRICES_TEX_OFFSET);

    _boneFrameQuaternionsTextureId = game->GetTextureManager()->CreateFloatTexture2d(
		"boneFrameQuaternions", boneFrameQuaternions, data->FrameCount, data->BoneCount, GL_RGBA32F_ARB, GL_RGBA, 4);
    _boneFrameQuaternionsUniLoc = glGetUniformLocation(_shaderProgramId, "boneFrameQuaternions");
	glActiveTexture(GL_TEXTURE0 + BONE_FRAME_QUATERNIONS_TEX_OFFSET);
	glBindTexture(GL_TEXTURE_2D, _boneFrameQuaternionsTextureId);
    glUniform1i(_boneFrameQuaternionsUniLoc, BONE_FRAME_QUATERNIONS_TEX_OFFSET);

	_bonePathsUniLoc = glGetUniformLocation(_shaderProgramId, "bonePaths");
	glUniform1iv(_bonePathsUniLoc, bonePathsVec.size(), bonePaths);
	delete[] bonePaths;

	_outlineColorUniLoc = glGetUniformLocation(_shaderProgramId, "outlineColor");
	glUniform4f(_outlineColorUniLoc, 1.0f, 0.0f, 0.0f, 1.0f);

	_outlineThicknessUniLoc = glGetUniformLocation(_shaderProgramId, "outlineThickness");
	glUniform1f(_outlineThicknessUniLoc, 0.0f);

	_upperBodyFrameToUniLoc = glGetUniformLocation(_shaderProgramId, "upperBodyFrameTo");
    _upperBodyFrameFromUniLoc = glGetUniformLocation(_shaderProgramId, "upperBodyFrameFrom");
    _upperBodyInterpolationFactorUniLoc = glGetUniformLocation(_shaderProgramId, "upperBodyInterpolationFactor");

	_lowerBodyFrameToUniLoc = glGetUniformLocation(_shaderProgramId, "lowerBodyFrameTo");
    _lowerBodyFrameFromUniLoc = glGetUniformLocation(_shaderProgramId, "lowerBodyFrameFrom");
    _lowerBodyInterpolationFactorUniLoc = glGetUniformLocation(_shaderProgramId, "lowerBodyInterpolationFactor");

	_bonePathPositionAttrLoc = glGetAttribLocation(_shaderProgramId, "bonePathPosition");
	glGenBuffers(1, &_vertBonePositionsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _vertBonePositionsBuffer);
	glBufferData(GL_ARRAY_BUFFER, data->VertexCount * sizeof(float), vertBonePositions, GL_STATIC_DRAW);
	delete[] vertBonePositions;

	_bodySectionAttrLoc = glGetAttribLocation(_shaderProgramId, "bodySection");
	glGenBuffers(1, &_vertBodySectionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _vertBodySectionBuffer);
	glBufferData(GL_ARRAY_BUFFER, data->VertexCount * sizeof(float), vertBodySections, GL_STATIC_DRAW);
	delete[] vertBodySections;
}

/* static */ void Bot::InitMarkerMesh(Game *game)
{
	float radius = 2.5f;
	float verts[] = {
		-radius, 0.0f,  radius,
		 radius, 0.0f,  radius,
		 radius, 0.0f, -radius,
		-radius, 0.0f, -radius
	};

	GLushort indices[] = { 0, 2, 1, 0, 3, 2 };

	float uvCoords[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	GLuint textureId = game->GetTextureManager()->GetTexture("marker");

	_markerMesh = new StaticMesh(verts, 4, indices, 2, 3, GL_TRIANGLES, NULL, uvCoords, textureId, NULL);
}

/* static */ void Bot::DestroySharedResources()
{
	DestroyBotCells();
	delete _markerMesh;
}

void Bot::InitBoneRotationOverrides()
{
	for (int i = 0; i < _boneCount; i++)
	{
		_boneRotationOverrides[i * 4 + 0] = 0.0f;
		_boneRotationOverrides[i * 4 + 1] = 0.0f;
		_boneRotationOverrides[i * 4 + 2] = 0.0f;
		_boneRotationOverrides[i * 4 + 3] = 1.0f;
	}
}

void Bot::UpdateNormalBoneRotationOverrides()
{
	int frameFrom, frameTo;
	float interpolationFactor;
	BoneData *boneData;

	for (int i = 0; i < _boneCount; i++)
	{
		boneData = _bones[i]->GetBoneData();

		if (i <= 9)
		{
			frameFrom = (int)_upperBodyFrameFrom;
			frameTo = (int)_upperBodyFrameTo;
			interpolationFactor = _upperBodyInterpolationFactor;
		}
		else
		{
			frameFrom = (int)_lowerBodyFrameFrom;
			frameTo = (int)_lowerBodyFrameTo;
			interpolationFactor = _lowerBodyInterpolationFactor;
		}

		if (interpolationFactor < 0.999f)
		{
			// there IS interpolation
			Quaternion q0 = boneData->Frames[frameFrom];
			Quaternion q1 = boneData->Frames[frameTo];
			Quaternion interpolatedQuaternion = GMath::Slerp(q0, q1, interpolationFactor);
			_boneRotationOverrides[i * 4 + 0] = interpolatedQuaternion.X;
			_boneRotationOverrides[i * 4 + 1] = interpolatedQuaternion.Y;
			_boneRotationOverrides[i * 4 + 2] = interpolatedQuaternion.Z;
			_boneRotationOverrides[i * 4 + 3] = interpolatedQuaternion.W;
		}
		else
		{
			// there is NO interpolation
			Quaternion q = boneData->Frames[frameTo];
			_boneRotationOverrides[i * 4 + 0] = q.X;
			_boneRotationOverrides[i * 4 + 1] = q.Y;
			_boneRotationOverrides[i * 4 + 2] = q.Z;
			_boneRotationOverrides[i * 4 + 3] = q.W;
		}
	}
}

void Bot::UpdateRagdollBoneRotationOverrides(float deltaTime)
{
	Vector3 force(0.0f, 1.0f, 0.0f);

	for (int i = 0; i < _boneCount; i++)
	{
		//if (i != 0 && i != 1 && i != 2 && i != 4) continue;

		// init current status
		Bone *bone = _bones[i];
		BoneData *boneData = bone->GetBoneData();
		Vector3 startTorque = bone->GetTorque();
		Quaternion q = Quaternion(_boneRotationOverrides[i * 4 + 3], _boneRotationOverrides[i * 4 + 0], _boneRotationOverrides[i * 4 + 1], _boneRotationOverrides[i * 4 + 2]);
		Vector3 rotatedDir = RotateDirectionWithBoneUsingOverrides(bone, boneData->Direction);

		// calculate force being applied to bone
		Vector3 deltaTorqueAxis = GMath::CrossProduct(force, rotatedDir);
		float angle = acos(GMath::DotProduct(force, rotatedDir));
		float perpendicularForce = 0.01f * sin(angle);

		// ignore tiny forces
		if (perpendicularForce < 0.001f)
			continue;

		Vector3 deltaTorque = GMath::VecScalarMult(deltaTorqueAxis, perpendicularForce);
		Vector3 newTorque = GMath::AddVectors(startTorque, deltaTorque);

		// apply friction/wind resistance
		newTorque = GMath::VecScalarMult(newTorque, 0.9f);

		bone->SetTorque(newTorque);

		// rotate bone based on calculated torque
		float rotationMagnitude = GMath::VecLength(newTorque);

		// clamp the rotation magnitude
		rotationMagnitude = GMath::Min(rotationMagnitude, 0.087f);

		// ignore tiny rotation
		if (abs(rotationMagnitude) < 0.0001f)
			continue;

		Vector3 rotationAxis = GMath::Normalize(newTorque);
		float thetaOver2 = rotationMagnitude / 2.0f;
		float sinThetaOver2 = sin(thetaOver2);
		Quaternion qAdjust(
			cos(thetaOver2),
			rotationAxis.X * sinThetaOver2,
			rotationAxis.Y * sinThetaOver2,
			rotationAxis.Z * sinThetaOver2);

		q = GMath::MultiplyQuaternions(q, qAdjust);

		_boneRotationOverrides[i * 4 + 3] = q.W;
		_boneRotationOverrides[i * 4 + 0] = q.X;
		_boneRotationOverrides[i * 4 + 1] = q.Y;
		_boneRotationOverrides[i * 4 + 2] = q.Z;

	}

	//_isRagdolling = isStillMoving;
}

bool Bot::GetClockwiseMovesDown(Vector3 rotationVec, Vector3 direction)
{
	direction = GMath::Normalize(Vector3(direction.X, 0.0f, direction.Z));

	if (abs(rotationVec.X) > abs(rotationVec.Z))
	{
		// closer to X axis
		if (rotationVec.X < 0)
		{
			// on the negative side, so direction should be further DOWN Z axis
			return (direction.Z <= rotationVec.Z);
		}
		else
		{
			// on the positive side, so direction should be further UP Z axis
			return (direction.Z >= rotationVec.Z);
		}
	}
	else
	{
		// closer to Z axis
		if (rotationVec.Z < 0)
		{
			// on the negative side, so direction should be further UP X axis
			return (direction.X >= rotationVec.X);
		}
		else
		{
			// on the positive side, so direction should be further DOWN X axis
			return (direction.X <= rotationVec.X);
		}
	}
}

void Bot::SetPositionXZ(float x, float z)
{
	Ray downRay(Vector3(x, TOP_OF_WORLD, z), Vector3(0.0f, -1.0f, 0.0f), 1e30f);
	CollisionResult gravityCollision = _game->RayWorldIntersection(downRay, this, MapCollision);
	if (gravityCollision.collisionType != NoCollision)
		SetPosition(gravityCollision.collisionPoint);
	else
		SetPosition(Vector3(x, BOTTOM_OF_WORLD, z));

	RefreshBotState();
}

void Bot::Update(float deltaTime)
{
	// do things that the AI has commanded the bot to do
	ExecuteAiFrame(deltaTime);

	// set upper body position
	if (_pitch >= 0.5f)
	{
		_upperBodyInterpolationFactor = _pitch * 2 - 1;
		_upperBodyFrameTo = 2.0f;
		_upperBodyFrameFrom = 0.0f;
	}
	else
	{
		_upperBodyInterpolationFactor = 1.0f - (_pitch * 2.0f);
		_upperBodyFrameTo = 1.0f;
		_upperBodyFrameFrom = 0.0f;
	}

	// set lower body position and handle moving character
	if (_isRunning)
	{
		_wasRunningLastFrame = true;

		_lowerBodyFrameTo += deltaTime * 0.1f;
		if (_lowerBodyFrameTo > 70.0f)
			_lowerBodyFrameTo -= 55.0f;

		_lowerBodyInterpolationFactor = 1.0f;
		_lowerBodyFrameFrom = 0.0f;

		// move the character
		Move(deltaTime / 80.0f * sin(_heading) * -1, deltaTime / 80.0f * cos(_heading) * -1);
	}
	else
	{
		if (_wasRunningLastFrame)
		{
			_lowerBodyFrameFrom = _lowerBodyFrameTo;
			_timeUntilFullStop = STOP_TIME;
		}

		if (_timeUntilFullStop > 0)
		{
			if (_timeUntilFullStop <= deltaTime)
			{
				_lowerBodyFrameFrom = 0.0f;
				_lowerBodyFrameTo = 0.0f;
				_lowerBodyInterpolationFactor = 1.0f;
			}
			else
			{
				_timeUntilFullStop -= deltaTime;
				_lowerBodyFrameTo = 0.0f;
				_lowerBodyInterpolationFactor = 1.0f - ((float)_timeUntilFullStop / (float)STOP_TIME);
			}
		}

		_wasRunningLastFrame = false;

	}

	// update falling
	if (_isFalling)
	{
		Ray downRay(Vector3(_position.X, TOP_OF_WORLD, _position.Z), Vector3(0.0f, -1.0f, 0.0f), 1e30f);
		CollisionResult gravityCollision = _game->RayWorldIntersection(downRay, this, MapCollision);
		float floorHeight = gravityCollision.collisionType != NoCollision ? gravityCollision.collisionPoint.Y + GUY_Y_OFFSET : BOTTOM_OF_WORLD;
		float seconds = deltaTime / 1000.0f;
		_position.Y -= _fallSpeed * seconds;

		if (floorHeight > _position.Y)
		{
			// he hit the ground; falling is finished
			_position.Y = floorHeight;
			_isFalling = false;
			_fallSpeed = 0.0f;
		}
		else
		{
			// still falling
			_fallSpeed += FALL_ACCELERATION * seconds;
		}
	}

	if (_isRagdolling)
		UpdateRagdollBoneRotationOverrides(deltaTime);
	else
		UpdateNormalBoneRotationOverrides();

	RefreshBotState();
}

void Bot::ExecuteAiFrame(float deltaTime)
{
	AiFrame *frame = _aiFrameBuffer.UpdateAndReturnActiveFrame();

	if (!frame->command.doNothing && frame->command.isTravelling)
	{
		Vector3 currentXZ = Vector3(_position.X, 0.0f, _position.Z);

		Vector2 moveTo = frame->command.path.points[frame->command.path.currentSegment];
		Vector3 destinationXZ = Vector3(moveTo.X, 0.0f, moveTo.Y);
		Vector3 diff = GMath::VecDiff(destinationXZ, currentXZ);
		float distance = GMath::VecLength(diff);
		bool isStillTravelling = true;

		// check if at end of path segment
		if (distance < 1.0f)
		{
			if (frame->command.path.currentSegment  >= frame->command.path.segmentCount - 1)
			{
				// reached the end of the last segment so stop travelling
				isStillTravelling = false;
			}
			else
			{
				// there are more segments so reload data but for new segment
				frame->command.path.currentSegment += 1;
				moveTo = frame->command.path.points[frame->command.path.currentSegment];
				destinationXZ = Vector3(moveTo.X, 0.0f, moveTo.Y);
				diff = GMath::VecDiff(destinationXZ, currentXZ);
				distance = GMath::VecLength(diff);
			}
		}

		// if not already at destination then run to it
		if (isStillTravelling)
		{
			_isRunning = true;

			// rotate toward destination
			Vector3 direction = GMath::Normalize(diff);
			float desiredHeading = atan2(direction.Z, direction.X) * -1.0f - PI_OVER_TWO;
			if (desiredHeading < 0.0f)
				desiredHeading += TWO_PI;

			float headingDiff = desiredHeading - _heading;
			if (headingDiff > PI)
				headingDiff = headingDiff - TWO_PI;

			float deltaHeading = deltaTime / 100.0f;

			if (headingDiff < 0.0f)
				deltaHeading = GMath::Max(headingDiff, -deltaHeading);
			else
				deltaHeading = GMath::Min(headingDiff, deltaHeading);

			_heading += deltaHeading;
		}
		else
		{
			frame->isComplete = true;
			_isRunning = false;
		}
	}
}

void Bot::Render()
{
	glPushMatrix();

	// bind the texture used to store bone animation data
	glActiveTexture(GL_TEXTURE0 + BONE_FRAME_MATRICES_TEX_OFFSET);
	glBindTexture(GL_TEXTURE_2D, _boneFrameMatricesTextureId);
	glActiveTexture(GL_TEXTURE0 + BONE_FRAME_QUATERNIONS_TEX_OFFSET);
	glBindTexture(GL_TEXTURE_2D, _boneFrameQuaternionsTextureId);

	glTranslatef(_position.X, _position.Y, _position.Z);
	glRotatef(_heading * _180_OVER_PI, 0.0f, 1.0f, 0.0f);

	glDisable(GL_DEPTH_TEST);

	if (_isSelected)
	{
		glPushMatrix();
		glEnable(GL_BLEND);
		glTranslatef(0.0f, -GUY_Y_OFFSET + 0.05f, 0.0f);
		_markerMesh->Render();
		glDisable(GL_BLEND);
		glPopMatrix();
	}

	glFrontFace(GL_CW);
	glPolygonMode( GL_FRONT, GL_FILL);
	glUseProgram(_shaderProgramId);

	glUniform4fv(_boneOverridesUniLoc, _boneCount, _boneRotationOverrides);

	glUniform1i(_lowerBodyFrameFromUniLoc, (int)_lowerBodyFrameFrom);
	glUniform1i(_lowerBodyFrameToUniLoc, (int)_lowerBodyFrameTo);
	glUniform1f(_lowerBodyInterpolationFactorUniLoc, _lowerBodyInterpolationFactor);

	glUniform1i(_upperBodyFrameFromUniLoc, (int)_upperBodyFrameFrom);
	glUniform1i(_upperBodyFrameToUniLoc, (int)_upperBodyFrameTo);
	glUniform1f(_upperBodyInterpolationFactorUniLoc, _upperBodyInterpolationFactor);

	glBindBuffer(GL_ARRAY_BUFFER, _vertBonePositionsBuffer);
	glEnableVertexAttribArray(_bonePathPositionAttrLoc);
	glVertexAttribPointer(_bonePathPositionAttrLoc, 1, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, _vertBodySectionBuffer);
	glEnableVertexAttribArray(_bodySectionAttrLoc);
	glVertexAttribPointer(_bodySectionAttrLoc, 1, GL_FLOAT, GL_FALSE, 0, NULL);

	Color outlineColor = _team->GetColor();
	glUniform4f(_outlineColorUniLoc, outlineColor.R, outlineColor.G, outlineColor.B, 1.0f);

	glUniform1f(_outlineThicknessUniLoc, GetOutlineThickness());
    _bodyMesh->Render();

	glEnable(GL_DEPTH_TEST);
	glUniform1f(_outlineThicknessUniLoc, 0.0f);
    _bodyMesh->Render();

	glDisableVertexAttribArray(_bonePathPositionAttrLoc);
	glDisableVertexAttribArray(_bodySectionAttrLoc);

	glUseProgram(0);

	glPopMatrix();
}

void Bot::StartRunning()
{
	_isRunning = true;
}

void Bot::StopRunning()
{
	_isRunning = false;
}

void Bot::HandleEvent(Uint8 *keyState, float deltaTime)
{
	if (keyState[SDLK_UP])
		_isRunning = true;
	else
		_isRunning = false;

	if (keyState[SDLK_LEFT])
	{
		_heading += deltaTime / 400.0f;
	}

	if (keyState[SDLK_RIGHT])
	{
		_heading -= deltaTime / 400.0f;
	}

	if (keyState[SDLK_z])
	{
		_pitch += deltaTime / 1000.0f;
		if (_pitch > 1.0f) _pitch = 1.0f;
	}

	if (keyState[SDLK_x])
	{
		_pitch -= deltaTime / 1000.0f;
		if (_pitch < 0.0f) _pitch = 0.0f;
	}

	if (keyState[SDLK_r])
	{
		_isRagdolling = true;
	}

	/*if (keyState[SDLK_c] && !_isShooting)
	{
		Shoot();
		_isShooting = true;
	}
	else if (!keyState[SDLK_c] && _isShooting)
	{
		_isShooting = false;
	}*/
}

LaserInfo Bot::CreateNewLaserShotFromGun()
{

	Vector3 mePos = GetPosition();
	Vector3 rel, start;
	rel = RotateVertWithBone(_boneHandLeft, _gunBarrelVert, (int)_upperBodyFrameFrom, (int)_upperBodyFrameTo, _upperBodyInterpolationFactor);

	//float pitchDegrees = (0.5f - _pitch) * -PI;
	//float pitchRadians = pitchDegrees;

	float pitchRadians = (_pitch - 0.5f) * PI;
	float sinH = sin(-_heading);
	float cosH = cos(-_heading);
	float sinP = sin(-pitchRadians);
	float cosP = cos(-pitchRadians);

	start.X = (cosH * rel.X - sinH * rel.Z) + mePos.X;
	start.Y = rel.Y + mePos.Y;
	start.Z = (sinH * rel.X + cosH * rel.Z) + mePos.Z;

	Vector3 direction, da(0, 0, -1.0f), db;

	db.Y = sinP * da.Z + cosP * da.Y;
	db.Z = cosP * da.Z - sinP * da.Y;

	direction.X = -sinH * db.Z;
	direction.Y = db.Y;
	direction.Z = sinH * db.X + cosH * db.Z;

	LaserInfo laser = LaserInfo(start, direction, _heading, pitchRadians);
	return laser;
	//_game->StartLaserShot(laser);

}

void Bot::Move(float deltaX, float deltaZ)
{
	float xDir = deltaX > 0.0f ? 1.0f : -1.0f;
	float zDir = deltaZ > 0.0f ? 1.0f : -1.0f;
	float absDeltaX = fabs(deltaX);
	float absDeltaZ = fabs(deltaZ);

	Ray moveRayX(_position, Vector3(xDir, 0.0f, 0.0f), absDeltaX + BOT_WALL_HIT_RADIUS);
	Ray moveRayZ(_position, Vector3(0.0f, 0.0f, zDir), absDeltaZ + BOT_WALL_HIT_RADIUS);

	CollisionResult xCollision = _game->RayWorldIntersection(moveRayX, this, MapCollision);
	CollisionResult zCollision = _game->RayWorldIntersection(moveRayZ, this, MapCollision);

	if (xCollision.collisionType != NoCollision)
		deltaX = (fabs(xCollision.collisionPoint.X - _position.X) - BOT_WALL_HIT_RADIUS) * xDir;
	if (zCollision.collisionType != NoCollision)
		deltaZ = (fabs(zCollision.collisionPoint.Z - _position.Z) - BOT_WALL_HIT_RADIUS) * zDir;

	_position.X += deltaX;
	_position.Z += deltaZ;

	Ray downRay(_position, Vector3(0.0f, -1.0f, 0.0f), 1e30f);

	CollisionResult yCollision = _game->RayWorldIntersection(downRay, this, MapCollision);

	float floorHeight = yCollision.collisionType != NoCollision ? yCollision.collisionPoint.Y + GUY_Y_OFFSET : BOTTOM_OF_WORLD;
	if (abs(_position.Y - floorHeight) > FALL_THRESHOLD)
	{
		// start falling
		_isFalling = true;
	}
	else
	{
		// not falling, just clamp to floor
		_position.Y = floorHeight;
	}

	UpdateCell();
}

Vector3 Bot::RotateDirectionWithBoneUsingOverrides(Bone *bone, Vector3 vert)
{
	Bone *currentBone = bone;
	BoneData *boneData;
	Vector3 bonePos;
	Matrix33 m;
	Quaternion q;

	while (currentBone != NULL)
	{
		boneData = currentBone->GetBoneData();
		bonePos = boneData->Head;

		q = Quaternion(
			_boneRotationOverrides[currentBone->GetIndex() * 4 + 3],
			_boneRotationOverrides[currentBone->GetIndex() * 4 + 0],
			_boneRotationOverrides[currentBone->GetIndex() * 4 + 1],
			_boneRotationOverrides[currentBone->GetIndex() * 4 + 2]);
		m = GMath::NormalizedQuaternionToMatrix33(q);

		Vector3 tempVert(vert);

		tempVert.X = (m.v11 * vert.X + m.v21 * vert.Y + m.v31 * vert.Z);
		tempVert.Y = (m.v12 * vert.X + m.v22 * vert.Y + m.v32 * vert.Z);
		tempVert.Z = (m.v13 * vert.X + m.v23 * vert.Y + m.v33 * vert.Z);

		vert = tempVert;

		currentBone = currentBone->GetParent();
	}

	return GMath::Normalize(vert);
}

Vector3 Bot::RotateVertWithBone(Bone *bone, Vector3 vert, int frameFrom, int frameTo, float interpolationFactor)
{
	Bone *currentBone = bone;
	float x, y, z;
	BoneData *boneData;
	Vector3 bonePos;
	Matrix33 m;

	while (currentBone != NULL)
	{
		boneData = currentBone->GetBoneData();
		bonePos = boneData->Head;

		if (interpolationFactor > 0.999f)
		{
			m = boneData->MatrixFrames[frameTo];
		}
		else if (interpolationFactor < 0.001f)
		{
			m = boneData->MatrixFrames[frameFrom];
		}
		else
		{
			Quaternion q0 = boneData->Frames[frameFrom];
			Quaternion q1 = boneData->Frames[frameTo];
			m = GMath::InterpolateQuaternionsToMatrix(q0, q1, interpolationFactor);
		}

		x = vert.X - bonePos.X;
		y = vert.Y - bonePos.Y;
		z = vert.Z - bonePos.Z;

		vert.X = (m.v11 * x + m.v21 * y + m.v31 * z) + bonePos.X;
		vert.Y = (m.v12 * x + m.v22 * y + m.v32 * z) + bonePos.Y;
		vert.Z = (m.v13 * x + m.v23 * y + m.v33 * z) + bonePos.Z;

		currentBone = currentBone->GetParent();
	}

	return vert;
}

float Bot::GetOutlineThickness()
{
	float distance = GMath::GetDistanceBetweenPoints(_game->GetCameraPosition(), _position);
	float multiplier = 0.0015f;
	return GMath::Max(distance * multiplier, 0.03f);
}

CollisionResult Bot::CalculateRayCollision(const Ray &ray)
{
	CollisionResult collision = GMath::RaySphereIntersection(ray, _position, BOT_BOUNDING_RADIUS, BotCollision);
	collision.object = this;
	return collision;
}

void Bot::UpdateCell()
{
	int cellRow = GMath::FloorZeroInt(_position.Z / _cellWidth) + CELL_PADDING;
	int cellCol = GMath::FloorZeroInt(_position.X / _cellWidth) + CELL_PADDING;

	int newCellIndex = (cellRow * _cellsAcross) + cellCol;

	// exit if there is no change
	if (newCellIndex == _cellIndex)
		return;

	// --- remove from old spot ---

	if (_cellIndex >= 0)
	{
		if (_previousBotInCell == NULL)
		{
			if (_nextBotInCell != NULL)
				_nextBotInCell->_previousBotInCell = NULL;
			_cells[_cellIndex] = _nextBotInCell;
		}
		else
		{
			if (_nextBotInCell != NULL)
				_nextBotInCell->_previousBotInCell = _previousBotInCell;
			_previousBotInCell->_nextBotInCell = _nextBotInCell;
		}
	}

	// --- insert to new spot ---

	if (_cells[newCellIndex] == NULL)
	{
		_nextBotInCell = NULL;
	}
	else
	{
		_cells[newCellIndex]->_previousBotInCell = this;
		_nextBotInCell = _cells[newCellIndex];
	}
	_previousBotInCell = NULL;
	_cells[newCellIndex] = this;

	//char msg[100];
	//snprintf(msg, 100, "new cell: %d (x: %f, y: %f, row: %d, col: %d)\n", newCellIndex, _position.X, _position.Z, cellRow, cellCol);
	//Logger::DebugMsg(msg);

	// update cell index
	_cellIndex = newCellIndex;
}

/* static */ void Bot::InitBotCells(float worldWidth, float worldDepth)
{
	float worldSize = GMath::Max(worldWidth, worldDepth);
	float cellWidth = BOT_BOUNDING_RADIUS;
	if (worldSize / cellWidth > 256.0f)
	{
		cellWidth = worldSize / 256.0f;
	}
	_cellWidth = cellWidth;
	_cellsAcross = ((int)(worldSize / cellWidth)) + (CELL_PADDING * 2);
	int cellCount = _cellsAcross * _cellsAcross;
	_cells = new Bot *[cellCount];
	for (int i = 0; i < cellCount; i++)
		_cells[i] = NULL;
}

/* static */ void Bot::DestroyBotCells()
{
	delete _cells;
}

/* static */ CollisionResult Bot::RayBotCollision(const Ray &ray, void *objToIgnore)
{
	int cellRow = GMath::FloorZeroInt(ray.start.Z / _cellWidth) + CELL_PADDING;
	int cellCol = GMath::FloorZeroInt(ray.start.X / _cellWidth) + CELL_PADDING;
	int startCell = (cellRow * _cellsAcross) + cellCol;
	CollisionResult nearestCollision(NoCollision);

	if (cellRow < 3 || cellRow >= (_cellsAcross - 3) || cellCol < 3 || cellRow >= (_cellsAcross - 3)) return nearestCollision;

	// Get nearest collision in the start cell
	nearestCollision = RayBotCollisionWithinCell(ray, startCell, objToIgnore);
	if (nearestCollision.collisionType == NoCollision)
		nearestCollision.distance = 1e30f;
	bool foundCollisionInInnerCell = nearestCollision.collisionType != NoCollision;

	// Loop through ring of adjacent cells and check if any of them have a closer collision
	for (int r = cellRow - 1; r < cellRow + 2; r++)
	{
		for (int c = cellCol - 1; c < cellCol + 2; c++)
		{
			if (!(r == cellRow && c == cellCol)) // skip the one in the middle
			{
				int cell = (r * _cellsAcross) + c;
				CollisionResult collision = RayBotCollisionWithinCell(ray, cell, objToIgnore);
				if (collision.collisionType != NoCollision && collision.distance < nearestCollision.distance)
					nearestCollision = collision;
			}
		}
	}

	// First chance for an early out: if there was a collision in inner cell then return nearest
	if (foundCollisionInInnerCell)
		return nearestCollision;

	// If the ray is really vertical like then it will never make it to any other cells
	if (fabs(ray.direction.Y) > 0.99f)
		return nearestCollision;

	// No definitive result yet, so push onward to next block of 3 adjacent cells
	bool isGoingEast = ray.direction.X < 0;
	bool isGoingNorth = ray.direction.Z > 0;
	Vector3 rayEnd = GMath::AddVectors(ray.start, GMath::VecScalarMult(ray.direction, ray.length));
	while (true)
	{
		foundCollisionInInnerCell = nearestCollision.collisionType != NoCollision;

		int nextRow = isGoingNorth ? cellRow + 1 : cellRow - 1;
		int nextCol = isGoingEast ? cellCol - 1 : cellCol + 1;

		// Find whether next row or next column comes next
		float nextRowStartZ = isGoingNorth ? (nextRow - CELL_PADDING) * _cellWidth : (nextRow - CELL_PADDING + 1) * _cellWidth;
		float nextColStartX = isGoingEast ? (nextCol - CELL_PADDING + 1) * _cellWidth : (nextCol - CELL_PADDING) * _cellWidth;
		float colFactor = fabs(ray.direction.X) > 0.0001f ? (nextColStartX - ray.start.X) / ray.direction.X : 1e30f;
		float rowFactor = fabs(ray.direction.Z) > 0.0001f ? (nextRowStartZ - ray.start.Z) / ray.direction.Z : 1e30f;

		if (colFactor < rowFactor)
		{
			// next column is first
			cellCol = nextCol;
			if (nextCol >= _cellsAcross || nextCol < 0 // does it exists?
				|| cellRow >= _cellsAcross || cellRow < 0
				|| (isGoingEast && nextColStartX < rayEnd.X) // is it within range going east?
				|| (!isGoingEast && nextColStartX > rayEnd.X)) // is it within range going west?
			{
				break;
			}
			else
			{
				if (cellRow < 3 || cellRow >= (_cellsAcross - 3) || cellCol < 3 || cellRow >= (_cellsAcross - 3)) return nearestCollision;
				for (int r = cellRow - 1; r < cellRow + 2; r++)
				{
					int cell = (r * _cellsAcross) + cellCol;
					CollisionResult collision = RayBotCollisionWithinCell(ray, cell, objToIgnore);
					if (collision.collisionType != NoCollision && collision.distance < nearestCollision.distance)
						nearestCollision = collision;
				}
			}
		}
		else
		{
			// next row is first
			cellRow = nextRow;
			if (nextRow >= _cellsAcross || nextRow < 0 // does it exist?
				|| cellCol >= _cellsAcross || cellCol < 0
				|| (isGoingNorth && nextRowStartZ > rayEnd.Z) // is it within range going north?
				|| (!isGoingNorth && nextRowStartZ < rayEnd.Z)) // is it within range going south?
			{
				break;
			}
			else
			{
				if (cellRow < 3 || cellRow >= (_cellsAcross - 3) || cellCol < 3 || cellRow >= (_cellsAcross - 3)) return nearestCollision;
				for (int c = cellCol - 1; c < cellCol + 2; c++)
				{
					int cell = (cellRow * _cellsAcross) + c;
					CollisionResult collision = RayBotCollisionWithinCell(ray, cell, objToIgnore);
					if (collision.collisionType != NoCollision && collision.distance < nearestCollision.distance)
						nearestCollision = collision;
				}
			}
		}

		if (foundCollisionInInnerCell)
			break;
	}

	return nearestCollision;
}

/* static */ CollisionResult Bot::RayBotCollisionWithinCell(const Ray &ray, int cellIndex, void *objToIgnore)
{
	CollisionResult nearestCollision(NoCollision, Vector3(), 1e30f);
	Bot *bot = _cells[cellIndex];
	while (bot != NULL)
	{
		if (bot != objToIgnore)
		{
			CollisionResult collision = bot->CalculateRayCollision(ray);
			if (collision.collisionType != NoCollision && collision.distance < nearestCollision.distance)
				nearestCollision = collision;
		}
		bot = bot->_nextBotInCell;
	}
	return nearestCollision;
}

void Bot::RefreshBotState()
{
	BotState state;
	state.position = this->_position;
	_botStateBuffer.SetActiveFrame(state);
}

AiFrameBuffer::AiFrameBuffer()
{
	_frameA = new AiFrame();
	_frameB = new AiFrame();
	_usingFrameA = true;
	_frameA->isActive = true;
}

AiFrameBuffer::~AiFrameBuffer()
{
	delete _frameA;
	delete _frameB;
}

AiFrame *AiFrameBuffer::UpdateAndReturnActiveFrame()
{
	if (_usingFrameA)
	{
		if (_frameB->isActive)
		{
			_frameA->isActive = false;
			_usingFrameA = false;
			return _frameB;
		}
		else
		{
			return _frameA;
		}
	}
	else
	{
		if (_frameA->isActive)
		{
			_frameB->isActive = false;
			_usingFrameA = true;
			return _frameA;
		}
		else
		{
			return _frameB;
		}
	}
}

AiFrame *AiFrameBuffer::GetActiveFrame()
{
	if (!_frameA->isActive)
		return _frameB;
	else if (!_frameB->isActive)
		return _frameA;
	else
		return NULL;
}

AiFrame *AiFrameBuffer::GetInactiveFrame()
{
	if (!_frameA->isActive)
		return _frameA;
	else if (!_frameB->isActive)
		return _frameB;
	else
		return NULL;
}

BotStateBuffer::BotStateBuffer()
{
	_usingFrameA = true;
}

BotState BotStateBuffer::GetActiveFrame()
{
	if (_usingFrameA)
		return _frameA;
	else
		return _frameB;
}

void BotStateBuffer::SetActiveFrame(BotState state)
{
	if (_usingFrameA)
		_frameB = state;
	else
		_frameA = state;

	_usingFrameA = !_usingFrameA;
}
