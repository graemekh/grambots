#include <math.h>
#include <iostream>
#include <fstream>
#include "TextureManager.h"

TextureManager::TextureManager()
{
	_textureMap = new std::map<std::string, unsigned int>();
}

TextureManager::~TextureManager()
{
	DestroyAllTextures();
	delete _textureMap;
}

GLuint TextureManager::CreateTexture(std::string name, GLubyte pixels[], unsigned int width, unsigned int height, GLint format, GLenum formatType)
{
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, formatType, GL_UNSIGNED_BYTE, pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	_textureMap->insert(std::pair<std::string, unsigned int>(name, textureId));
	return textureId;
}

GLuint TextureManager::CreateFloatTexture2d(std::string name, GLfloat *data, unsigned int minWidth, unsigned int minHeight, GLint format, GLenum formatType, int floatsPerPixel) 
{
	// find nearest width/height powers of two
	unsigned int width = RoundToNextPowerOfTwo(minWidth);
	unsigned int height = RoundToNextPowerOfTwo(minHeight);

	// make a copy of data that uses the new width and height that are powers of two
	// the result should look like this where # = data and * = padding:
	// #####***
	// #####***
	// #####***
	// ********
	GLfloat *paddedData = new GLfloat[width * height * floatsPerPixel];
	int valuesPerUnpaddedRow = minWidth * floatsPerPixel;
	int valuesPerPaddedRow = width * floatsPerPixel;
	int bytesPerUnpaddedRow = sizeof(GLfloat) * valuesPerUnpaddedRow;
	for (unsigned int i = 0; i < minHeight; i++)
	{
		int paddedOffset = i * valuesPerPaddedRow;
		int unpaddedOffset = i * valuesPerUnpaddedRow;
		memcpy(paddedData + paddedOffset, data + unpaddedOffset, bytesPerUnpaddedRow);
	}

	// create the actual texture
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, formatType, GL_FLOAT, paddedData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	_textureMap->insert(std::pair<std::string, unsigned int>(name, textureId));

	// padded data was just a temp value that has already been sent to the video card, so it
	// is time for it to die.
	delete[] paddedData;

	return textureId;
}

void TextureManager::DestroyTexture(std::string name)
{
	GLuint textureId = _textureMap->at(name);
	glDeleteTextures(1, &textureId);
	_textureMap->erase(name);
}

void TextureManager::DestroyAllTextures()
{
	std::map<std::string, unsigned int>::const_iterator iterator;
	for(iterator = _textureMap->begin(); iterator != _textureMap->end(); iterator++)
		glDeleteTextures(1, &iterator->second);
	_textureMap->clear();
}

GLuint TextureManager::CreateTextureFromFile(std::string fileName, std::string name, GLint format, GLenum formatType)
{

	SDL_Surface *bitmap = SDL_LoadBMP(fileName.c_str());
	unsigned int width = (unsigned int)bitmap->w;
	unsigned int height = (unsigned int)bitmap->h;
	GLubyte *pixels = (GLubyte *)bitmap->pixels;

	//if (formatType == GL_RGBA)
	//{
	//	for (int i = 0; i < width * height; i++)
	//	{
	//		int first = i * 4;
	//		int fourth = i * 4 + 3;
	//		GLubyte swap = pixels[first];
	//		pixels[first] = pixels[fourth];
	//		pixels[fourth] = swap;
	//	}
	//}

	GLuint textureId = CreateTexture(name, pixels, width, height, format, formatType);

	SDL_FreeSurface(bitmap);
	return textureId;
}

unsigned int TextureManager::RoundToNextPowerOfTwo(unsigned int value)
{
	// I don't think texture dimension is allowed to be less than 64, so start there
	unsigned int powerOfTwo = 64;

	while(powerOfTwo < value)
	{
		powerOfTwo *= 2;
	}

	return powerOfTwo;
}