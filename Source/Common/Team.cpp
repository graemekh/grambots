#include "Team.h"

Team::Team(Game *game, Color color, unsigned int number, Vector2 spawnPoint, AnimatedMeshData *botMeshData)
{
	_game = game;
	_color = color;
	_number = number;
	_spawnPoint = spawnPoint;

	_bots = new Bot *[MAX_BOTS_PER_TEAM];
	for (unsigned int i = 0; i < MAX_BOTS_PER_TEAM; i++)
	{
		_bots[i] = new Bot(_game, botMeshData, this);
	}
}

Team::~Team()
{
	for (unsigned int i = 0; i < MAX_BOTS_PER_TEAM; i++)
		delete _bots[i];

	delete _bots;
}

Bot *Team::SpawnNewBot()
{
	// Find the next inactive bot
	Bot *availableBot = NULL;
	for (unsigned int i = 0; i < MAX_BOTS_PER_TEAM; i++)
	{
		if (!_bots[i]->GetIsActive())
		{
			availableBot = _bots[i];
			availableBot->SetIsActive(true);
			availableBot->SetPositionXZ(_spawnPoint.X, _spawnPoint.Y);
			break;
		}
	}

	// Note: if the maximum number of bots are already active then this will return null
	return availableBot;
}
