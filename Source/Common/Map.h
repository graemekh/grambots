#pragma once

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>
#include "Game.h"
#include "StaticMesh.h"

#define TILE_WIDTH 6.0f
#define TILE_HEIGHT 3.5f
#define BOTTOM_OF_WORLD -1000.0f
#define MAX_WALKABLE_SLOPE 1.0f
#define MAP_NODE_MAX_CONNECTION_COUNT 8

struct MapNode;

struct MapConnection
{
	MapConnection()
	{
		heightDiff = 0.0f;
		connectedNode = NULL;
	}

	float heightDiff;
	MapNode *connectedNode;
};

struct MapNode
{
	MapConnection connections[MAP_NODE_MAX_CONNECTION_COUNT];
	int index, row, col, connectionCount;
};

enum Direction
{
	NORTH,
	EAST,
	SOUTH,
	WEST
};

enum TileType
{
	TILE_BLANK,
	TILE_FLAT,
	TILE_RAMP_NORTH,
	TILE_RAMP_EAST,
	TILE_RAMP_SOUTH,
	TILE_RAMP_WEST
};

class Tile
{
public:
	Tile();
	Tile(int tileLevel, TileType tileType);
	~Tile();

	void SetType(TileType type) { _type = type; }
	TileType GetType() const { return _type; }

	void SetLevel(int level) { _level = level; }
	int GetBaseLevel() const { return _level; }
	float GetBaseHeight() const { return (_level + 1) * TILE_HEIGHT; }

	int GetLevelTopLeft() const;
	int GetLevelTopRight() const;
	int GetLevelBottomLeft() const;
	int GetLevelBottomRight() const;

	std::vector<Triangle> GetTriangles() const { return _triangles; }
	void SetTriangles(std::vector<Triangle> triangles) { _triangles = triangles; }

	MapNode *GetMapNode() { return &_mapNode; }

private:

	int _level;
	TileType _type;
	bool _isBlockedEast, _isBlockedNorth, _isBlockedWest, _isBlockedSouth;

	std::vector<Triangle> _triangles;
	MapNode _mapNode;

};

class Map
{
public:
	static Map *LoadFromFile(std::string filePath, Game *game);
	~Map();

	inline int GetTilesAcross() { return _tilesAcross; }
	inline int GetTilesDeep() { return _tilesDeep; }
	inline Tile **GetTiles() { return _tiles; }
	void Render();
	float GetTileWidth() { return TILE_WIDTH; }
	Vector2 GetSpawnPoint(int teamNumber);
	int GetNumberOfPlayers();

	inline float GetHeightAtPoint(float x, float z)
	{
		int row = (int)(z / TILE_WIDTH);
		int col = (int)(x / TILE_WIDTH);

		if (x < 0.0f || z < 0.0f || row >= _tilesDeep || col >= _tilesAcross)
		{
			// if point is off the edge then return large negative number so objects will fall far
			return BOTTOM_OF_WORLD;
		}
		else
		{
			// use height of this tile
			Tile tile = _tiles[row][col];
			return GetHeightFromTileAtRelativePoint(tile, x - (col * TILE_WIDTH), z - (row * TILE_WIDTH));
		}
	}

	inline Tile GetTileAtPoint(float x, float z)
	{
		int row = (int)(z / TILE_WIDTH);
		int col = (int)(x / TILE_WIDTH);

		if (x < 0.0f || z < 0.0f || row >= _tilesDeep || col >= _tilesAcross)
			return Tile(-1, TILE_BLANK);
		else
			return _tiles[row][col];
	}

	inline Tile GetTileAtIndex(int index)
	{
		int r = (int)(index / GetTilesAcross());
		int c = index % GetTilesAcross();
		return _tiles[r][c];
	}

	inline Vector2 GetXZPositionAtCenterOfTileAtIndex(int index)
	{
		int r = (int)(index / GetTilesAcross());
		int c = index % GetTilesAcross();
		return Vector2(
			c * TILE_WIDTH + (TILE_WIDTH / 2.0f),
			r * TILE_WIDTH + (TILE_WIDTH / 2.0f));
	}

	inline int GetRowAtZ(float z)
	{
		return (int)(z / TILE_WIDTH);
	}

	inline int GetColAtX(float x)
	{
		return (int)(x / TILE_WIDTH);
	}

	static inline float GetHeightFromTileAtRelativePoint(Tile tile, float relX, float relZ)
	{
		switch (tile.GetType())
		{
		case TILE_FLAT:
			return GetHeightFromLevel(tile.GetBaseLevel());
		case TILE_RAMP_NORTH:
			return GetHeightFromLevel(tile.GetBaseLevel()) + (((TILE_WIDTH - relZ) / TILE_WIDTH) * TILE_HEIGHT);
		case TILE_RAMP_EAST:
			return GetHeightFromLevel(tile.GetBaseLevel()) + ((relX / TILE_WIDTH) * TILE_HEIGHT);
		case TILE_RAMP_SOUTH:
			return GetHeightFromLevel(tile.GetBaseLevel()) + ((relZ / TILE_WIDTH) * TILE_HEIGHT);
		case TILE_RAMP_WEST:
			return GetHeightFromLevel(tile.GetBaseLevel()) + (((TILE_WIDTH - relX) / TILE_WIDTH) * TILE_HEIGHT);
		default:
			return BOTTOM_OF_WORLD;
		}
	}

	bool FindNextTileAlongRay(const Ray &ray, int *currentTileRowIndex, int *currentTileColIndex, Tile *tileOut);

	CollisionResult CalculateRayTileCollision(const Ray &ray, const Tile &tile, int tileRowIndex, int tileColIndex);
	//float CalculateRayTileCollision(const Ray &ray, const Tile &tile, Vector3 &collisionPoint, int tileRowIndex, int tileColIndex);

private:
	Map(int tilesAcross, int tilesDeep, Tile **tiles, Game *game, std::vector<std::pair<char, Vector2> > specialPoints);
	GLuint CreatePanelTexture();
	void InitMesh();
	void InitBlockages();
	bool IsBlockedFromAtoB(Tile a, Tile b, Direction direction);
	static Tile GetTileFromChars(char a, char b);
	static int GetTileLevelFromChar(char c);

	GLuint _shaderProgramId;
	void InitShader();

	Tile **_tiles;
	int _tilesAcross;
	int _tilesDeep;
	Game *_game;
	GLuint _panelTextureId;
	float _rampSlope;
	std::vector<std::pair<char, Vector2> > _specialPoints;

	StaticMesh *_mesh;
	Tile GetTile(int r, int c);

	static inline float GetHeightFromLevel(int level)
	{
		return (level + 1) * TILE_HEIGHT;
	}

};
