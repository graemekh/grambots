#include "SkyBox.h"

SkyBox::SkyBox(GLuint textureId)
{
	_textureId = textureId;

	float width = 5.0f;
	float height = 5.0f;
	float depth = 5.0f;

	float vertices[] = {
        -width/2,  height/2,  depth/2,  // 0  BACK
         width/2,  height/2,  depth/2,  // 1
         width/2, -height/2,  depth/2,  // 2
        -width/2, -height/2,  depth/2,  // 3

        -width/2,  height/2, -depth/2,  // 4  FRONT
         width/2,  height/2, -depth/2,  // 5
         width/2, -height/2, -depth/2,  // 6
        -width/2, -height/2, -depth/2,  // 7

		-width/2,  height/2, -depth/2,  // 8  RIGHT
        -width/2,  height/2,  depth/2,  // 9
        -width/2, -height/2,  depth/2,  // 10
        -width/2, -height/2, -depth/2,  // 11

		 width/2,  height/2, -depth/2,  // 12 LEFT
         width/2,  height/2,  depth/2,  // 13
         width/2, -height/2,  depth/2,  // 14
         width/2, -height/2, -depth/2,   // 15

		-width/2,  height/2,  depth/2,  // 16 TOP
         width/2,  height/2,  depth/2,  // 17
         width/2,  height/2, -depth/2,  // 18
        -width/2,  height/2, -depth/2,  // 19

		-width/2, -height/2,  depth/2,  // 20 BOTTOM
         width/2, -height/2,  depth/2,  // 21
         width/2, -height/2, -depth/2,  // 22
        -width/2, -height/2, -depth/2  // 23
    };

	float uvcoords[] = {
		0, 0,  1, 0,  1, 1,  0, 1,
		0, 0,  1, 0,  1, 1,  0, 1,
		0, 0,  1, 0,  1, 1,  0, 1,
		0, 0,  1, 0,  1, 1,  0, 1,
		0, 0,  1, 0,  1, 1,  0, 1,
		0, 0,  1, 0,  1, 1,  0, 1
	};

	GLushort indices[] = {
        0, 3, 2, // front
        0, 2, 1,
        5, 6, 7, // back
        5, 7, 4,
        8, 11, 10, // right
        8, 10, 9, 
        12, 14, 15, // left
        12, 13, 14, 
        16, 19, 18, // top
        16, 18, 17,
        20, 23, 22, // bottom
        20, 22, 21
    };

	_mesh = new StaticMesh(vertices, 24, indices, 12, 3, GL_TRIANGLES, NULL, uvcoords, _textureId, NULL);
}

SkyBox::~SkyBox()
{
	delete _mesh;
}

void SkyBox::Render()
{
	glDisable(GL_DEPTH_TEST);
	_mesh->Render();
	glEnable(GL_DEPTH_TEST);
}