#pragma once

#include "CommonStructs.h"
#include "Renderable.h"
#include "StaticMesh.h"

#define LASER_BEAM_LENGTH 15.0f

class Laser
{
public:
    Laser(GLuint shaderProgramId);
    ~Laser();
    void Render();

private:
    StaticMesh *_mesh;
	GLuint _shaderProgramId;
};

struct LaserInfo
{
	LaserInfo(Vector3 startPosition, Vector3 laserDirection, float headingAngle, float pitchAngle)
	{
		position = startPosition;
		originalPosition = startPosition;
		heading = headingAngle;
		pitch = pitchAngle;
		direction = laserDirection;
		scale = 1.0f;
		isExploding = false;
	}

	Vector3 position;
	float heading;
	float pitch;
	Vector3 direction;
	float scale;
	bool isExploding;
	Vector3 originalPosition;
};

