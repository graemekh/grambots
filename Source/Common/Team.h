#pragma once

#include "CommonStructs.h"
#include "AnimatedMeshData.h"
#include "Bot.h"

class Bot;

class Team
{
public:
	Team(Game *game, Color color, unsigned int number, Vector2 spawnPoint, AnimatedMeshData *botMeshData);
	~Team();
	Color GetColor() { return _color; }
	void SetColor(Color color) { _color = color; }
	unsigned int GetNumber() { return _number; }
	Bot **GetBots() { return _bots; };
	Bot *SpawnNewBot();

private:
	Color _color;
	unsigned int _number;
	Bot **_bots;
	Vector2 _spawnPoint;
	Game *_game;
};
