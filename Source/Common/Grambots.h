#include "Game.h"
#include "Map.h"
#include "Laser.h"
#include "Font.h"
#include "TextBlock.h"
#include "Bot.h"
#include "Team.h"
#include "AiWorker.h"
#include "SkyBox.h"

#define MIN_DELTA 0
#define MAX_DELTA 42
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define MAX_LASER_DISTANCE 1000
#define LASER_SPEED 0.01f
#define FIELD_OF_VIEW 60.0f

class Grambots : public Game
{
public:
	Grambots();
	~Grambots();

	void StartLaserShot(LaserInfo laserInfo);
	void LoadMap(std::string mapName);

	Font *GetSmallFont() { return _smallFont; }
	void InitFonts();

	CollisionResult RayWorldIntersection(const Ray &ray, void *objectToIgnore = NULL, int collisionTypeMask = 0);
	void Init();
	void RenderScene();

    void Begin();
    void End();
    void Run();
	void Pause();
	void Resume();

private:
	SkyBox *_skyBox;
	float _time;
	AiWorker *_humanPlayerAiWorker;

	void UpdateScene(float deltaTime);
	void UpdateProjectiles(float deltaTime);
	void ShootLaser(Bot *bot);
	Color GetTeamColor(int teamNumber);

	Map *_map;
	std::vector<AiWorker *> _aiWorkers;
	std::vector<Team *> _teams;
	std::vector<LaserInfo> _lasers;

	Font *_smallFont;
	TextBlock *_fpsText;
	Laser *_standardLaser;

	void MousePick(Vector2 screenPos);
	Ray CastRayFromScreenPosition(Vector2 screenPos);

	TextBlock **_aiFpsTextBlocks;

	void OrthoMode();
	void PerspectiveMode();

	float _prevMillis;
    int _framesSinceFpsUpdate;
    float _millisAtFpsUpdate;

	bool _isPaused;

	StaticMesh *_indicatorMesh;
	void InitIndicatorMesh();
	Vector3 _indicatorPosition;
	float _indicatorStartTime;

	inline CollisionResult NearestCollision(CollisionResult a, CollisionResult b)
	{
		if (a.collisionType == NoCollision)
			return b;
		else if (b.collisionType == NoCollision)
			return a;
		else if (a.distance < b.distance)
			return a;
		else
			return b;
	}
};
