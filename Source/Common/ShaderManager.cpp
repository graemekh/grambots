#include "ShaderManager.h"
#include <fstream>

ShaderManager::ShaderManager()
{
}

ShaderManager::~ShaderManager()
{
	// kill all the 
	std::map<std::string, GLuint>::const_iterator iterator;
	for(iterator = _shaderMap.begin(); iterator != _shaderMap.end(); iterator++)
		glDeleteShader(iterator->second);
	_shaderMap.clear();
}

GLuint ShaderManager::LoadShadersFromCode(std::string name, std::string vertexShaderCode, std::string pixelShaderCode)
{
	// create glsl source strings
	const char *vertexShaderSource = vertexShaderCode.c_str();
	const char *pixelShaderSource = pixelShaderCode.c_str();

	// init program and shaders
	GLuint shaderProgramId = glCreateProgram();
	GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
	GLuint pixelShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	// load shader source code
	glShaderSource(vertexShaderId, 1, &vertexShaderSource, NULL);
	glShaderSource(pixelShaderId, 1, &pixelShaderSource, NULL);

	// compile shaders
	glCompileShader(vertexShaderId);
	glCompileShader(pixelShaderId);

	// check for compile errors (this is just here so I can check for
	// compile errors/warnings when stepping though the code
	GLchar *vertexLog = new GLchar[5000];
	GLchar *pixelLog = new GLchar[5000];
	glGetShaderInfoLog(vertexShaderId, 5000, NULL, vertexLog);
	glGetShaderInfoLog(pixelShaderId, 5000, NULL, pixelLog);
    std::string vertLogAsStr(vertexLog);
    std::string pixelLogAsStr(pixelLog);
	delete[] vertexLog;
	delete[] pixelLog;

	// connect the two shaders to their program
	glAttachShader(shaderProgramId, vertexShaderId);
	glAttachShader(shaderProgramId, pixelShaderId);
	glLinkProgram(shaderProgramId);

	// check for errors (it doesn't do anything with the error, just gives something to look at when debugging)
	std::string error((const char *)gluErrorString(glGetError()));

	// keep track of this shader program based on its name
	_shaderMap.insert(std::pair<std::string, GLuint>(name, shaderProgramId));

	// return the id of the newly created shader program that links this vertex/pixel shader pair
	return shaderProgramId;
}

GLuint ShaderManager::LoadShaders(std::string name, std::string vertexShaderPath, std::string pixelShaderPath)
{
	// create glsl source strings
	std::string vertexShaderSourceString = ReadAllText(vertexShaderPath);
	std::string pixelShaderSourceString = ReadAllText(pixelShaderPath);

	return LoadShadersFromCode(name, vertexShaderSourceString, pixelShaderSourceString);
}

GLuint ShaderManager::LoadShaders(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, std::vector<std::pair<std::string, std::string> > textReplacements)
{
	// create glsl source strings
	std::string vertexShaderSourceString = ReadAllText(vertexShaderPath);
	std::string pixelShaderSourceString = ReadAllText(pixelShaderPath);

	// perform text replacements
	for (unsigned int i = 0; i < textReplacements.size(); i ++)
	{
		const char *oldText = textReplacements[i].first.c_str();
		const char *newText = textReplacements[i].second.c_str();
		vertexShaderSourceString = replaceAll(vertexShaderSourceString, oldText, newText);
		pixelShaderSourceString = replaceAll(pixelShaderSourceString, oldText, newText);
	}

	return LoadShadersFromCode(name, vertexShaderSourceString, pixelShaderSourceString);
}

void ShaderManager::DeleteShaders(std::string name)
{
	glDeleteShader(_shaderMap.at(name));
	_shaderMap.erase(name);
}

GLuint ShaderManager::GetShaderProgramId(std::string name)
{
	return _shaderMap.at(name);
}

std::string ShaderManager::ReadAllText(std::string path)
{
	std::string line, allText;
	std::ifstream inStream(path.c_str());

	while (std::getline(inStream, line)) 
	{
	   allText += line + "\n";
	}

	return allText;
}

std::string ShaderManager::replaceAll(std::string result, const std::string& replaceWhat, const std::string& replaceWithWhat)
{
	while(true)
	{
		const int pos = result.find(replaceWhat);
		if (pos==-1) break;
		result.replace(pos,replaceWhat.size(),replaceWithWhat);
	}
	return result;
}
