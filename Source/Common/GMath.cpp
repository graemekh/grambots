#include "GMath.h"
#include <math.h>


float GMath::RayTriangleIntersection(
	const Vector3 &rayOrig, 
	const Vector3 &rayDelta,
	const Vector3 &p0,
	const Vector3 &p1,
	const Vector3 &p2,
	float minT)
{
	const float kNoIntersection = 1e30f;

	Vector3 e1 = VecDiff(p1, p0);
	Vector3 e2 = VecDiff(p2, p1);

	Vector3 n = CrossProduct(e1, e2);

	float dot = DotProduct(n, rayDelta);

	if (!(dot < 0.0f))
		return kNoIntersection;

	float d = DotProduct(n, p0);

	float t = d - DotProduct(n, rayOrig);

	if (!(t <= 0.0f))
		return kNoIntersection;

	if (!(t >= dot * minT))
		return kNoIntersection;

	t /= dot;

	Vector3 p = AddVectors(rayOrig, VecScalarMult(rayDelta, t));

	float u0, u1, u2;
	float v0, v1, v2;
	if (fabs(n.X) > fabs(n.Y))
	{
		if (fabs(n.X) > fabs(n.Z))
		{
			u0 = p.Y - p0.Y;
			u1 = p1.Y - p0.Y;
			u2 = p2.Y - p0.Y;

			v0 = p.Z - p0.Z;
			v1 = p1.Z - p0.Z;
			v2 = p2.Z - p0.Z;
		}
		else
		{
			u0 = p.X - p0.X;
			u1 = p1.X - p0.X;
			u2 = p2.X - p0.X;

			v0 = p.Y - p0.Y;
			v1 = p1.Y - p0.Y;
			v2 = p2.Y - p0.Y;
		}
	}
	else
	{
		if (fabs(n.Y) > fabs(n.Z))
		{
			u0 = p.X - p0.X;
			u1 = p1.X - p0.X;
			u2 = p2.X - p0.X;

			v0 = p.Z - p0.Z;
			v1 = p1.Z - p0.Z;
			v2 = p2.Z - p0.Z;
		}
		else
		{
			u0 = p.X - p0.X;
			u1 = p1.X - p0.X;
			u2 = p2.X - p0.X;

			v0 = p.Y - p0.Y;
			v1 = p1.Y - p0.Y;
			v2 = p2.Y - p0.Y;
		}
	}

	float temp = u1 * v2 - v1 * u2;
	if (!(temp != 0.0f))
		return kNoIntersection;

	temp = 1.0f / temp;

	float alpha = (u0 * v2 - v0 * u2) * temp;
	if (!(alpha >= 0.0f))
		return kNoIntersection;

	float beta = (u1 * v0 - v1 * u0) * temp;
	if (!(beta >= 0.0f))
		return kNoIntersection;

	float gamma = 1.0f - alpha - beta;
	if (!(gamma >= 0.0f))
		return kNoIntersection;

	return t;
}

CollisionResult GMath::RayCylinderIntersection(const Ray &ray, const Vector3 &cylinderOrigin, const Vector3 &cylinderDirection, float cylinderRadius, CollisionType type)
{
	throw "not implemented";
}

CollisionResult GMath::RaySphereIntersection(const Ray &ray, const Vector3 &center, float radius, CollisionType type)
{
	const CollisionResult noCollision(NoCollision);

	Vector3 e = VecDiff(center, ray.start);
	float a = DotProduct(e, ray.direction);
	float e2 = DotProduct(e, e);

	float r2 = radius * radius;

	// if ray is inside sphere then treat as no collision
	if (e2 < r2)
		return noCollision;

	float radicand = r2 - e2 + a*a;

	// negative radicands are impossible and indicate no solution
	if (!(radicand >= 0.0f))
		return noCollision;

	float squareRoot = sqrt(radicand);

	float d1 = a - squareRoot;
	float d2 = a + squareRoot;

	// if EITHER interection point is behind ray start then there are no collisions
	if (d1 < 0.0f || d2 < 0.0f)
		return noCollision;

	float distance = GMath::Min(d1, d2);

	// if distance is longer than ray then there is no collision
	if (distance > ray.length)
		return noCollision;

	Vector3 collisionPoint(
		ray.start.X + ray.direction.X * distance,
		ray.start.Y + ray.direction.Y * distance,
		ray.start.Z + ray.direction.Z * distance);

	return CollisionResult(type, collisionPoint, distance);
}