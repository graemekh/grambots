#include "StaticMesh.h"

StaticMesh::StaticMesh(float vertexData[], int vertexCount, GLushort indexData[], int polygonCount, int vertsPerPoly, GLenum polygonType, float colorData[], float uvcoords[], unsigned int textureId, float normalData[])
{
	_hasNormalData = normalData != NULL;
	_hasColorData = colorData != NULL;
	_hasTextureData = uvcoords != NULL;
	_textureId = textureId;
	_vertexBufferId = NULL;
	_indexBufferId = NULL;
	_colorBufferId = NULL;
	_uvBufferId = NULL;
    _polygonType = polygonType;
	_indexData = indexData;
    PolygonCount = polygonCount;
    VertsPerPoly = vertsPerPoly;
	VertexCount = vertexCount;

    // create vertex buffer
    glGenBuffers(1, &_vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, VertexCount * 3 * sizeof(float), vertexData, GL_STATIC_DRAW);

	if (_hasNormalData)
	{
		// create normal buffer
		glGenBuffers(1, &_normalBufferId);
		glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
		glBufferData(GL_ARRAY_BUFFER, VertexCount * 3 * sizeof(float), normalData, GL_STATIC_DRAW);
	}

    if (_hasColorData)
	{
		// create color buffer
		glGenBuffers(1, &_colorBufferId);
		glBindBuffer(GL_ARRAY_BUFFER, _colorBufferId);
		glBufferData(GL_ARRAY_BUFFER, VertexCount * 3 * sizeof(float), colorData, GL_STATIC_DRAW);
	}
	
	if (_hasTextureData)
	{
		// create uv coord buffer
		glGenBuffers(1, &_uvBufferId);
		glBindBuffer(GL_ARRAY_BUFFER, _uvBufferId);
		glBufferData(GL_ARRAY_BUFFER, VertexCount * 2 * sizeof(float), uvcoords, GL_STATIC_DRAW);
	}

    // create index buffer
    glGenBuffers(1, &_indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertsPerPoly * polygonCount * sizeof(GLushort), _indexData, GL_STATIC_DRAW);
}

StaticMesh *StaticMesh::CreateTexturedTriangleMesh(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float colorData[], float uvcoords[], unsigned int textureId, float normalData[])
{
	return new StaticMesh(vertexData, vertexCount, indexData, triangleCount, 3, GL_TRIANGLES, colorData, uvcoords, textureId, normalData);
}

StaticMesh *StaticMesh::CreateTriangleMesh(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float colorData[])
{
    return new StaticMesh(vertexData, vertexCount, indexData, triangleCount, 3, GL_TRIANGLES, colorData);
}

StaticMesh *StaticMesh::CreateQuadMesh(float vertexData[], int vertexCount, GLushort indexData[], int quadCount, float colorData[])
{
    return new StaticMesh(vertexData, vertexCount, indexData, quadCount, 4, GL_QUADS, colorData);
}

StaticMesh::~StaticMesh()
{
    glDeleteBuffers(1, &_vertexBufferId);
    glDeleteBuffers(1, &_indexBufferId);
    if (_hasColorData) glDeleteBuffers(1, &_colorBufferId);
	if (_hasTextureData) glDeleteBuffers(1, &_uvBufferId);
}

void StaticMesh::Render()
{	
    // setup vertex buffer
    glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glVertexPointer(3, GL_FLOAT, 0, (char *)NULL);

	if (_hasNormalData)
	{
		// setup normal buffer
		glEnableClientState(GL_NORMAL_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
		glNormalPointer(GL_FLOAT, 0, (char *)NULL);
	}

	if (_hasColorData)
	{
		// setup color buffer
		glEnableClientState(GL_COLOR_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, _colorBufferId);
		glColorPointer(3, GL_FLOAT, 0, (char *)NULL);
	}
	
	if (_hasTextureData)
	{
		// setup uv coord buffer
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, _uvBufferId);
		glTexCoordPointer(2, GL_FLOAT, 0, (char *)NULL);

		// bind the actual texture
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, _textureId);
	}

    // setup index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);

    // render mesh
	glDrawElements(_polygonType, PolygonCount * VertsPerPoly, GL_UNSIGNED_SHORT, NULL);
    //glDrawRangeElements(_polygonType, 0, PolygonCount * VertsPerPoly, PolygonCount * VertsPerPoly, GL_UNSIGNED_SHORT, NULL);

	// disable VBO features that were enabled at top of function
    glDisableClientState(GL_VERTEX_ARRAY);

	if (_hasNormalData)
		glDisableClientState(GL_NORMAL_ARRAY);
	if (_hasColorData)
		glDisableClientState(GL_COLOR_ARRAY);

	if (_hasTextureData)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisable(GL_TEXTURE_2D);
	}
}
