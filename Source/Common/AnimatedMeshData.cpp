#include "AnimatedMeshData.h"
#include "GMath.h"

AnimatedMeshData::AnimatedMeshData(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float normals[], BoneData **bones, BoneData *rootBone, int boneCount, int frameCount, std::map<std::string, int> specialVerts)
{
	this->VertexData = vertexData;
	this->VertexCount = vertexCount;
	this->IndexData = indexData;
	this->TriangleCount = triangleCount;
	this->Bones = bones;
	this->RootBone = rootBone;
	this->BoneCount = boneCount;
	this->FrameCount = frameCount;
	this->SpecialVertices = specialVerts;
	this->NormalData = normals;
}

AnimatedMeshData::~AnimatedMeshData()
{
	delete[] VertexData;
	delete[] IndexData;
	delete[] NormalData;
	delete RootBone; // this will trigger a cleanup of all child bones
	delete[] Bones;
}

AnimatedMeshData *AnimatedMeshData::LoadFromFile(std::string fileName)
{
	float *vertexData;
	float *normalData;
	GLushort *indexData;
	BoneData *rootBone;
	BoneData **bones;
	int vertexCount, triangleCount, boneCount, frameCount;
	std::map<std::string, int> specialVerts;

	std::string line;
    const char *fileNameAsCStr = fileName.c_str();
	std::ifstream inStream(fileNameAsCStr);
	std::map<std::string, BoneData *> boneMap;

	int vertIndex = 0, triangleIndex = 0;

	while (std::getline(inStream, line))
	{
		if (line.find("vertexCount=") == 0)
		{
			vertexCount = GetIntFromHeaderLine(line);
			vertexData = new float[vertexCount * 3];
			normalData = new float[vertexCount * 3];
		}
		else if (line.find("triangleCount") == 0)
		{
			triangleCount = GetIntFromHeaderLine(line);
			indexData = new GLushort[triangleCount * 3];
		}
		else if (line.find("boneCount") == 0)
		{
			boneCount = GetIntFromHeaderLine(line);
			bones = new BoneData *[boneCount];
		}
		else if (line.find("frameCount") == 0)
		{
			frameCount = GetIntFromHeaderLine(line);
		}
		else if (line.find("s") == 0)
		{
			specialVerts.insert(GetSpecialVertPairFromLine(line));
		}
		else if (line[0] == 'v')
		{
			Vector3 vertex, normal;
			GetVertexDataFromLine(line, vertex, normal);
			vertexData[vertIndex * 3 + 0] = vertex.X;
			vertexData[vertIndex * 3 + 1] = vertex.Y;
			vertexData[vertIndex * 3 + 2] = vertex.Z;
			normalData[vertIndex * 3 + 0] = normal.X;
			normalData[vertIndex * 3 + 1] = normal.Y;
			normalData[vertIndex * 3 + 2] = normal.Z;
			vertIndex++;
		}
		else if (line[0] == 't')
		{
			Face triangle = GetTriangleFromLine(line);
			indexData[triangleIndex * 3 + 0] = triangle.v1;
			indexData[triangleIndex * 3 + 1] = triangle.v2;
			indexData[triangleIndex * 3 + 2] = triangle.v3;
			triangleIndex++;
		}
		else if (line[0] == 'b')
		{
			int boneIndex[1];
			*boneIndex = 0;
			rootBone = LoadBonesFromLine(line, "", &inStream, &boneMap, frameCount, boneIndex, bones, std::vector<BoneData *>());
		}
		else if (line[0] == 'a')
		{
			std::string name;
			int frame;
			Quaternion rotation;

			std::vector<std::string> tokens = SplitString(line, '|');
			name = tokens[1];
			std::istringstream(tokens[2]) >> frame;
			std::istringstream(tokens[3]) >> rotation.W;
			std::istringstream(tokens[4]) >> rotation.X;
			std::istringstream(tokens[5]) >> rotation.Y;
			std::istringstream(tokens[6]) >> rotation.Z;

			BoneData *bone = boneMap[name];
			bone->Frames[frame] = rotation;
			float q[] = {rotation.W, rotation.X, rotation.Y, rotation.Z};
			float m[9];
			GMath::NormalizedQuaternionToMatrix33(q, m);
			Matrix33 matrix(m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8]);
			bone->MatrixFrames[frame] = matrix;
		}
	}

	inStream.close();

	return new AnimatedMeshData(vertexData, vertexCount, indexData, triangleCount, normalData, bones, rootBone, boneCount, frameCount, specialVerts);
}

BoneData *AnimatedMeshData::LoadBonesFromLine(std::string line, std::string indent, std::ifstream *inStream, std::map<std::string, BoneData *> *boneMap, int frameCount, int *boneIndex, BoneData **bonesArray, std::vector<BoneData *> bonePath)
{
	std::vector<std::string> tokens = SplitString(line, '|');
	std::vector<std::string> vertexTokens = SplitString(tokens[8], ',');
	BoneData *boneData = new BoneData();
	boneData->Index = *boneIndex;
	
	bonePath.push_back(boneData);

	// init bone path
	boneData->PathSize = bonePath.size();
	boneData->Path = new BoneData *[boneData->PathSize];
	for (int i = 0; i < boneData->PathSize; i ++)
		boneData->Path[i] = bonePath[i];

	Vector3 head, tail;
	std::istringstream(tokens[2]) >> head.X;
	std::istringstream(tokens[3]) >> head.Y;
	std::istringstream(tokens[4]) >> head.Z;
	std::istringstream(tokens[5]) >> tail.X;
	std::istringstream(tokens[6]) >> tail.Y;
	std::istringstream(tokens[7]) >> tail.Z;

	boneData->FrameCount = frameCount;
	boneData->Frames = new Quaternion[frameCount];
	boneData->MatrixFrames = new Matrix33[frameCount];
	boneData->Head = head;
	boneData->Tail = tail;
	boneData->Direction = GMath::Normalize(GMath::VecDiff(tail, head));
	boneData->Name = tokens[1];

	boneMap->insert(std::pair<std::string, BoneData *>(boneData->Name, boneData));
	bonesArray[*boneIndex] = boneData;

	std::vector<int> tempVerts;
	std::vector<int> tempDirectVerts;

	for (unsigned int i = 0; i < vertexTokens.size(); i++)
	{
		int vert;
		std::istringstream(vertexTokens[i]) >> vert;
		tempVerts.push_back(vert);
		tempDirectVerts.push_back(vert);
	}

	// do children
	std::vector<BoneData *> children;
	std::string childIndent(indent.c_str());
	childIndent.append("-");
	while(true)
	{
		std::streampos oldPosition = inStream->tellg();
		std::string nextLine;
		std::getline(*inStream, nextLine);

		if (nextLine.find(childIndent) == 0)
		{
			(*boneIndex)++;
			BoneData *childBone = LoadBonesFromLine(nextLine, childIndent, inStream, boneMap, frameCount, boneIndex, bonesArray, bonePath);
			for (int i = 0; i < childBone->VertexCount; i++)
				tempVerts.push_back(childBone->Vertices[i]);
			children.push_back(childBone);
		}
		else
		{
			inStream->seekg(oldPosition);
			break;
		}
	}

	boneData->ChildCount = children.size();
	boneData->Children = new BoneData *[boneData->ChildCount];
	for (int i = 0; i < boneData->ChildCount; i++)
		boneData->Children[i] = children[i];

	boneData->VertexCount = tempVerts.size();
	boneData->Vertices = new int[boneData->VertexCount];
	for (int i = 0; i < boneData->VertexCount; i++)
		boneData->Vertices[i] = tempVerts[i];

	boneData->DirectVertexCount = tempDirectVerts.size();
	boneData->DirectVertices = new int[boneData->DirectVertexCount];
	for (int i = 0; i < boneData->DirectVertexCount; i++)
		boneData->DirectVertices[i] = tempDirectVerts[i];

	return boneData;
}

int AnimatedMeshData::GetIntFromHeaderLine(std::string line)
{
	int first = line.find('=') + 1;
	int last = line.find_last_of("0123456789");
	std::string intString = line.substr(first, last - first + 1);
	int value;
	std::istringstream(intString) >> value;
	return value;
}

std::pair<std::string, int> AnimatedMeshData::GetSpecialVertPairFromLine(std::string line)
{
	std::string key;
	int value;

	std::vector<std::string> tokens = SplitString(line, '|');
	key = tokens[1];
	std::istringstream(tokens[2]) >> value;

	return std::pair<std::string, int>(key, value);
}

void AnimatedMeshData::GetVertexDataFromLine(std::string line, Vector3 &vertex, Vector3 &normal)
{
	std::vector<std::string> tokens = SplitString(line, '|');
	std::istringstream(tokens[1]) >> vertex.X;
	std::istringstream(tokens[2]) >> vertex.Y;
	std::istringstream(tokens[3]) >> vertex.Z;
	std::istringstream(tokens[4]) >> normal.X;
	std::istringstream(tokens[5]) >> normal.Y;
	std::istringstream(tokens[6]) >> normal.Z;
}

Face AnimatedMeshData::GetTriangleFromLine(std::string line)
{
	std::vector<std::string> tokens = SplitString(line, '|');
	Face tri;
	std::istringstream(tokens[1]) >> tri.v1;
	std::istringstream(tokens[2]) >> tri.v2;
	std::istringstream(tokens[3]) >> tri.v3;
	return tri;
}

std::vector<std::string> &AnimatedMeshData::SplitString(const std::string &s, char delim, std::vector<std::string> &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> AnimatedMeshData::SplitString(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    return SplitString(s, delim, elems);
}