#include "Map.h"
#include "GMath.h"
#include "Assets.h"

#define ADJACENT_NORMAL_MODIFIER 5.0f

Map::Map(int tilesAcross, int tilesDeep, Tile **tiles, Game *game, std::vector<std::pair<char, Vector2> > specialPoints)
{
	_game = game;
	_specialPoints = specialPoints;
	_tilesAcross = tilesAcross;
	_tilesDeep = tilesDeep;
	_tiles = tiles;
	_mesh = NULL;
	_panelTextureId = CreatePanelTexture(); //_textureManager->GetTexture("satin");
	_rampSlope = TILE_HEIGHT / TILE_WIDTH;

	InitMesh();
	InitBlockages();
	InitShader();
}

Map::~Map()
{
	//for (int i = 0; i < _tilesDeep; i++)
	//{
	//	delete _tiles[i];
	//}
	delete[] _tiles;
	delete _mesh;
}

void Map::InitShader()
{
	_shaderProgramId = _game->GetShaderManager()->LoadShaders("Map",
		Assets::GetAssetPath("Shaders/MapVertex.glsl"),
		Assets::GetAssetPath("Shaders/MapPixel.glsl"));
}

Tile Map::GetTile(int r, int c)
{
	if (r < 0 || r >= _tilesDeep || c < 0 || c >= _tilesAcross)
		return Tile(-1, TILE_BLANK);
	else
		return _tiles[r][c];
}

void Map::InitMesh()
{
	std::vector<Vector3> vertices;
	std::vector<GLushort> indices;
	std::vector<float> uvcoords;
	std::vector<Vector3> normals;
	std::vector<Color> colors;
	Color floorColor(0.5f, 1.0f, 0.5f);
	Color darkFloorColor(0.25f, 0.5f, 0.25f);
	Color wallColor(1.0f, 0.5f, 0.0f);

	for (int tileRowIndex = 0; tileRowIndex < _tilesDeep; tileRowIndex++)
	{
		for (int tileColIndex = 0; tileColIndex < _tilesAcross; tileColIndex++)
		{
			Tile tile = GetTile(tileRowIndex, tileColIndex);
			Tile tileN = GetTile(tileRowIndex + 1, tileColIndex);
			Tile tileNE = GetTile(tileRowIndex + 1, tileColIndex - 1);
			Tile tileE = GetTile(tileRowIndex, tileColIndex - 1);
			Tile tileSE = GetTile(tileRowIndex - 1, tileColIndex - 1);
			Tile tileS = GetTile(tileRowIndex - 1, tileColIndex);
			Tile tileSW = GetTile(tileRowIndex - 1, tileColIndex + 1);
			Tile tileW = GetTile(tileRowIndex, tileColIndex + 1);
			Tile tileNW = GetTile(tileRowIndex + 1, tileColIndex + 1);

			std::vector<Triangle> triangles;

			if (tile.GetType() != TILE_BLANK && tile.GetBaseLevel() >= 0)
			{
				// create top square
				float xstart = tileColIndex * TILE_WIDTH;
				float xend = xstart + TILE_WIDTH;
				float zstart = tileRowIndex * TILE_WIDTH;
				float zend = zstart + TILE_WIDTH;

				Color colorNE = floorColor, colorNW = floorColor, colorSE = floorColor, colorSW = floorColor;

				GLushort nextVertIndex = (GLushort)vertices.size();

				triangles.push_back(Triangle(
					Vector3(xstart, GetHeightFromLevel(tile.GetLevelTopLeft()), zstart),
					Vector3(xend, GetHeightFromLevel(tile.GetLevelTopRight()), zstart),
					Vector3(xend, GetHeightFromLevel(tile.GetLevelBottomRight()), zend)));
				triangles.push_back(Triangle(
					Vector3(xstart, GetHeightFromLevel(tile.GetLevelTopLeft()), zstart),
					Vector3(xend, GetHeightFromLevel(tile.GetLevelBottomRight()), zend),
					Vector3(xstart, GetHeightFromLevel(tile.GetLevelBottomLeft()), zend)));
				vertices.push_back(Vector3(xstart, GetHeightFromLevel(tile.GetLevelTopLeft()), zstart));
				vertices.push_back(Vector3(xend, GetHeightFromLevel(tile.GetLevelTopRight()), zstart));
				vertices.push_back(Vector3(xend, GetHeightFromLevel(tile.GetLevelBottomRight()), zend));
				vertices.push_back(Vector3(xstart, GetHeightFromLevel(tile.GetLevelBottomLeft()), zend));

				uvcoords.push_back(0.0f); uvcoords.push_back(1.0f);
				uvcoords.push_back(1.0f); uvcoords.push_back(1.0f);
				uvcoords.push_back(1.0f); uvcoords.push_back(0.0f);
				uvcoords.push_back(0.0f); uvcoords.push_back(0.0f);

				indices.push_back(nextVertIndex);
				indices.push_back(nextVertIndex + 1);
				indices.push_back(nextVertIndex + 2);

				indices.push_back(nextVertIndex);
				indices.push_back(nextVertIndex + 2);
				indices.push_back(nextVertIndex + 3);

				Vector3 normalNW(0.0f, 1.0f, 0.0f);
				Vector3 normalNE(0.0f, 1.0f, 0.0f);
				Vector3 normalSE(0.0f, 1.0f, 0.0f);
				Vector3 normalSW(0.0f, 1.0f, 0.0f);

				int eastDiff = tileE.GetBaseLevel() - tile.GetBaseLevel();
				int southDiff = tileS.GetBaseLevel() - tile.GetBaseLevel();
				int westDiff = tileW.GetBaseLevel() - tile.GetBaseLevel();
				int northDiff = tileN.GetBaseLevel() - tile.GetBaseLevel();
				int northEastDiff = tileNE.GetBaseLevel() - tile.GetBaseLevel();
				int southEastDiff = tileSE.GetBaseLevel() - tile.GetBaseLevel();
				int northWestDiff = tileNW.GetBaseLevel() - tile.GetBaseLevel();
				int southWestDiff = tileSW.GetBaseLevel() - tile.GetBaseLevel();

				if (westDiff > 0)
				{
					colorNW = darkFloorColor;
					colorSW = darkFloorColor;
				}
				if (eastDiff > 0)
				{
					colorSE = darkFloorColor;
					colorNE = darkFloorColor;
				}
				if (southDiff > 0)
				{
					colorSE = darkFloorColor;
					colorSW = darkFloorColor;
				}
				if (northDiff > 0)
				{
					colorNW = darkFloorColor;
					colorNE = darkFloorColor;
				}

				if (northEastDiff > 0)
				{
					colorNE = darkFloorColor;
				}
				if (northWestDiff > 0)
				{
					colorNW = darkFloorColor;
				}
				if (southEastDiff > 0)
				{
					colorSE = darkFloorColor;
				}
				if (southWestDiff > 0)
				{
					colorSW = darkFloorColor;
				}

				normals.push_back(GMath::Normalize(normalSE));
				normals.push_back(GMath::Normalize(normalSW));
				normals.push_back(GMath::Normalize(normalNW));
				normals.push_back(GMath::Normalize(normalNE));

				colors.push_back(colorSE); colors.push_back(colorSW); colors.push_back(colorNW); colors.push_back(colorNE);

				// -- create side panels BELOW this tile cap if there are any --

				for (int levelTop = tile.GetBaseLevel(); levelTop > tileN.GetBaseLevel(); levelTop--)
				{
					nextVertIndex = (GLushort)vertices.size();
					Triangle t1 = Triangle(
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zend),
						Vector3(xstart, GetHeightFromLevel(levelTop), zend),
						Vector3(xend, GetHeightFromLevel(levelTop), zend));
					Triangle t2 = Triangle(
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zend),
						Vector3(xend, GetHeightFromLevel(levelTop), zend),
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zend));
					triangles.push_back(t1);
					triangles.push_back(t2);

					vertices.push_back(t1.v2); vertices.push_back(t1.v1); vertices.push_back(t1.v3); vertices.push_back(t2.v3);

					uvcoords.push_back(0.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(0.0f); uvcoords.push_back(0.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(0.0f);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex);
					indices.push_back(nextVertIndex + 2);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex + 2);
					indices.push_back(nextVertIndex + 3);

					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, 1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, 1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, 1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, 1.0f)));

					colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor);
				}

				for (int levelTop = tile.GetBaseLevel(); levelTop > tileS.GetBaseLevel(); levelTop--)
				{
					nextVertIndex = (GLushort)vertices.size();
					Triangle t1 = Triangle(
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zstart),
						Vector3(xend, GetHeightFromLevel(levelTop), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop), zstart));
					Triangle t2 = Triangle(
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zstart));
					triangles.push_back(t1);
					triangles.push_back(t2);

					vertices.push_back(t1.v2); vertices.push_back(t1.v1); vertices.push_back(t1.v3); vertices.push_back(t2.v3);

					uvcoords.push_back(0.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(0.0f); uvcoords.push_back(0.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(0.0f);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex);
					indices.push_back(nextVertIndex + 2);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex + 2);
					indices.push_back(nextVertIndex + 3);

					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, -1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, -1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, -1.0f)));
					normals.push_back(GMath::Normalize(Vector3(0.0f, 0.0f, -1.0f)));

					colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor);
				}

				for (int levelTop = tile.GetBaseLevel(); levelTop > tileE.GetBaseLevel(); levelTop--)
				{
					nextVertIndex = (GLushort)vertices.size();
					Triangle t1 = Triangle(
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop), zend));
					Triangle t2 = Triangle(
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zstart),
						Vector3(xstart, GetHeightFromLevel(levelTop), zend),
						Vector3(xstart, GetHeightFromLevel(levelTop - 1), zend));
					triangles.push_back(t1);
					triangles.push_back(t2);

					vertices.push_back(t1.v2); vertices.push_back(t1.v1); vertices.push_back(t1.v3); vertices.push_back(t2.v3);

					uvcoords.push_back(0.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(0.0f); uvcoords.push_back(0.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(0.0f);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex);
					indices.push_back(nextVertIndex + 2);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex + 2);
					indices.push_back(nextVertIndex + 3);

					normals.push_back(GMath::Normalize(Vector3(-1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(-1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(-1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(-1.0f, 0.0f, 0.0f)));

					colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor);
				}

				for (int levelTop = tile.GetBaseLevel(); levelTop > tileW.GetBaseLevel(); levelTop--)
				{
					nextVertIndex = (GLushort)vertices.size();
					Triangle t1 = Triangle(
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zend),
						Vector3(xend, GetHeightFromLevel(levelTop), zend),
						Vector3(xend, GetHeightFromLevel(levelTop), zstart));
					Triangle t2 = Triangle(
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zend),
						Vector3(xend, GetHeightFromLevel(levelTop), zstart),
						Vector3(xend, GetHeightFromLevel(levelTop - 1), zstart));
					triangles.push_back(t1);
					triangles.push_back(t2);

					vertices.push_back(t1.v2); vertices.push_back(t1.v1); vertices.push_back(t1.v3); vertices.push_back(t2.v3);

					uvcoords.push_back(0.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(1.0f);
					uvcoords.push_back(0.0f); uvcoords.push_back(0.0f);
					uvcoords.push_back(1.0f); uvcoords.push_back(0.0f);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex);
					indices.push_back(nextVertIndex + 2);

					indices.push_back(nextVertIndex + 1);
					indices.push_back(nextVertIndex + 2);
					indices.push_back(nextVertIndex + 3);

					normals.push_back(GMath::Normalize(Vector3(1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(1.0f, 0.0f, 0.0f)));
					normals.push_back(GMath::Normalize(Vector3(1.0f, 0.0f, 0.0f)));

					colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor); colors.push_back(wallColor);
				}

			}

			tile.SetTriangles(triangles);
			_tiles[tileRowIndex][tileColIndex] = tile;

		}
	}

	int vertCount = vertices.size();
	int indexCount = indices.size();
	int uvCount = uvcoords.size();
	int triangleCount = indexCount / 3;

	float *vertexArray = new float[vertCount * 3];
	GLushort *indexArray = new GLushort[indexCount];
	float *uvArray = new float[uvCount];

	for (int i = 0; i < vertCount; i++)
	{
		Vector3 vert = vertices[i];
		vertexArray[i * 3 + 0] = vert.X;
		vertexArray[i * 3 + 1] = vert.Y;
		vertexArray[i * 3 + 2] = vert.Z;
	}

	for (int i = 0; i < indexCount; i++)
	{
		indexArray[i] = indices[i];
	}

	for (int i = 0; i < uvCount; i++)
	{
		uvArray[i] = uvcoords[i];
	}

	float *normalArray = new float[vertCount * 3];
	for (int i = 0; i < vertCount; i++)
	{
		normalArray[i * 3 + 0] = normals[i].X;
		normalArray[i * 3 + 1] = normals[i].Y;
		normalArray[i * 3 + 2] = normals[i].Z;
	}

	float *colorArray = new float[vertCount * 4];
	for (int i = 0; i < vertCount; i++)
	{
		colorArray[i * 3 + 0] = colors[i].R;
		colorArray[i * 3 + 1] = colors[i].G;
		colorArray[i * 3 + 2] = colors[i].B;
		colorArray[i * 3 + 3] = 1.0f;
	}

	_mesh = new StaticMesh(vertexArray, vertCount, indexArray, triangleCount, 3, GL_TRIANGLES, colorArray, uvArray, _panelTextureId, normalArray);

	// this data was all copied to video ram so it is not necessary to keep it around
	delete[] vertexArray;
	delete[] indexArray;
	delete[] uvArray;
	delete[] normalArray;
	delete[] colorArray;
}

void Map::InitBlockages()
{
	for (int r = 0; r < _tilesDeep; r++)
	{
		for (int c = 0; c < _tilesAcross; c++)
		{

			Tile *thisTile = &_tiles[r][c];
			MapNode *node = thisTile->GetMapNode();

			node->index = r * _tilesDeep + c;
			node->row = r;
			node->col = c;

			int connectionCount = 0;
			Tile *northTile = NULL, *eastTile = NULL, *southTile = NULL, *westTile = NULL;

			if (r > 0)
			{
				northTile = &_tiles[r - 1][c];
				if (northTile->GetBaseLevel() >= 0)
				{
					node->connections[connectionCount].connectedNode = northTile->GetMapNode();
					node->connections[connectionCount].heightDiff = northTile->GetBaseHeight() - thisTile->GetBaseHeight();
					connectionCount += 1;
				}
			}

			if (r < _tilesDeep - 1)
			{
				southTile = &_tiles[r + 1][c];
				if (southTile->GetBaseLevel() >= 0)
				{
					node->connections[connectionCount].connectedNode = southTile->GetMapNode();
					node->connections[connectionCount].heightDiff = southTile->GetBaseHeight() - thisTile->GetBaseHeight();
					connectionCount += 1;
				}
			}

			if (c < _tilesAcross - 1)
			{
				eastTile = &_tiles[r][c + 1];
				if (eastTile->GetBaseLevel() >= 0)
				{
					node->connections[connectionCount].connectedNode = eastTile->GetMapNode();
					node->connections[connectionCount].heightDiff = eastTile->GetBaseHeight() - thisTile->GetBaseHeight();
					connectionCount += 1;
				}
			}

			if (c > 0)
			{
				westTile = &_tiles[r][c - 1];
				if (westTile->GetBaseLevel() >= 0)
				{
					node->connections[connectionCount].connectedNode = westTile->GetMapNode();
					node->connections[connectionCount].heightDiff = westTile->GetBaseHeight() - thisTile->GetBaseHeight();
					connectionCount += 1;
				}
			}

			if (northTile != NULL && eastTile != NULL)
			{
				Tile *northEastTile = &_tiles[r - 1][c + 1];
				if (northTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& eastTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& northEastTile->GetBaseHeight() == thisTile->GetBaseHeight())
				{
					node->connections[connectionCount].connectedNode = northEastTile->GetMapNode();
					node->connections[connectionCount].heightDiff = 0.0f;
					connectionCount += 1;
				}
			}

			if (eastTile != NULL && southTile != NULL)
			{
				Tile *southEastTile = &_tiles[r + 1][c + 1];
				if (southTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& eastTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& southEastTile->GetBaseHeight() == thisTile->GetBaseHeight())
				{
					node->connections[connectionCount].connectedNode = southEastTile->GetMapNode();
					node->connections[connectionCount].heightDiff = 0.0f;
					connectionCount += 1;
				}
			}

			if (southTile != NULL && westTile != NULL)
			{
				Tile *southWestTile = &_tiles[r + 1][c - 1];
				if (southTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& westTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& southWestTile->GetBaseHeight() == thisTile->GetBaseHeight())
				{
					node->connections[connectionCount].connectedNode = southWestTile->GetMapNode();
					node->connections[connectionCount].heightDiff = 0.0f;
					connectionCount += 1;
				}
			}

			if (northTile != NULL && westTile != NULL)
			{
				Tile *northWestTile = &_tiles[r - 1][c - 1];
				if (northTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& westTile->GetBaseHeight() == thisTile->GetBaseHeight()
						&& northWestTile->GetBaseHeight() == thisTile->GetBaseHeight())
				{
					node->connections[connectionCount].connectedNode = northWestTile->GetMapNode();
					node->connections[connectionCount].heightDiff = 0.0f;
					connectionCount += 1;
				}
			}

			node->connectionCount = connectionCount;
		}
	}
}

bool Map::IsBlockedFromAtoB(Tile a, Tile b, Direction direction)
{
	switch (direction)
	{
	case NORTH:
		return GMath::Max(a.GetLevelTopLeft(), a.GetLevelTopRight()) < GMath::Max(b.GetLevelBottomLeft(), b.GetLevelBottomRight());
	case SOUTH:
		return GMath::Max(a.GetLevelBottomLeft(), a.GetLevelBottomRight()) < GMath::Max(b.GetLevelTopLeft(), b.GetLevelTopRight());
	case EAST:
		return GMath::Max(a.GetLevelTopRight(), a.GetLevelBottomRight()) < GMath::Max(b.GetLevelTopLeft(), b.GetLevelBottomLeft());
	case WEST:
		return GMath::Max(a.GetLevelTopLeft(), a.GetLevelBottomLeft()) < GMath::Max(b.GetLevelTopRight(), b.GetLevelBottomRight());
	default:
		return false;
	}
}

void Map::Render()
{
	glUseProgram(_shaderProgramId);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);
	_mesh->Render();
	glUseProgram(0);
}

int Map::GetNumberOfPlayers()
{
	int players = 0;
	for (unsigned int i = 0; i < _specialPoints.size(); i++)
	{
		switch (_specialPoints[i].first)
		{
		case 'A': players = GMath::Max(players, 1); break;
		case 'B': players = GMath::Max(players, 2); break;
		case 'C': players = GMath::Max(players, 3); break;
		case 'D': players = GMath::Max(players, 4); break;
		case 'E': players = GMath::Max(players, 5); break;
		case 'F': players = GMath::Max(players, 6); break;
		case 'G': players = GMath::Max(players, 7); break;
		case 'H': players = GMath::Max(players, 8); break;
		}
	}

	return players;
}

Vector2 Map::GetSpawnPoint(int teamNumber)
{
	char teamChar;
	switch (teamNumber)
	{
	case 1: teamChar = 'A'; break;
	case 2: teamChar = 'B'; break;
	case 3: teamChar = 'C'; break;
	case 4: teamChar = 'D'; break;
	case 5: teamChar = 'E'; break;
	case 6: teamChar = 'F'; break;
	case 7: teamChar = 'G'; break;
	case 8: teamChar = 'H'; break;
	default: teamChar = 'A';
	}

	for (unsigned int i = 0; i < _specialPoints.size(); i++)
	{
		std::pair<char, Vector2> point = _specialPoints[i];
		if (point.first == teamChar)
			return point.second;
	}

	// uh oh! spawn point not found, so just return some random value
	return Vector2(0.0f, 0.0f);
}

Map *Map::LoadFromFile(std::string filePath, Game *game)
{
	int tilesAcross = 0;
	int tilesDeep = 0;
	std::string line;
	const char *fileNameAsCStr = filePath.c_str();
	std::ifstream inStream(fileNameAsCStr);
	std::vector<Tile *> tileRows;
	std::vector<std::pair<char, Vector2> > specialPoints;

	// read tiles from file
	while (std::getline(inStream, line))
	{
		// make sure it's not just an empty line
		if (line.size() == 0 || line[0] == ' ' || line[0] == '\t')
			continue;

		// init map width if it has not already been initialized
		if (tilesAcross == 0)
			tilesAcross = line.size() / 2;

		// process line of tiles
		Tile *row = new Tile[tilesAcross];
		tileRows.push_back(row);
		for (int i = 0; i < tilesAcross; i++)
		{
			char a = line[i * 2];
			char b = line[i * 2 + 1];
			row[i] = GetTileFromChars(a, b);

			if (b != '-')
			{
				Vector2 point;
				point.X = i * TILE_WIDTH + (TILE_WIDTH / 2.0f);
				point.Y = (tileRows.size() - 1) * TILE_WIDTH + (TILE_WIDTH / 2.0f);
				specialPoints.push_back(std::pair<char, Vector2>(b, point));
			}
		}
	}
	inStream.close();

	// create tile array from vector
	tilesDeep = tileRows.size();
	Tile **tiles = new Tile *[tilesDeep];
	for (int i = 0; i < tilesDeep; i++)
		tiles[i] = tileRows[i];

	return new Map(tilesAcross, tilesDeep, tiles, game, specialPoints);
}

Tile Map::GetTileFromChars(char a, char b)
{
	return Tile(GetTileLevelFromChar(a), TILE_FLAT);
}

int Map::GetTileLevelFromChar(char c)
{
	if (c == '_')
		return -1;
	else
		return (int)c - 48;
}

GLuint Map::CreatePanelTexture()
{
	unsigned int width = 64;
	unsigned int height = 64;
	GLubyte blockValues[] = {
		100, 90, 95, 85,
		90, 85, 100, 95,
		100, 95, 85, 90,
		95, 100, 90, 85};

		GLubyte *pixels = new GLubyte[width * height * 3];
		for (unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++)
			{
				int xBlock = x / 16;
				int yBlock = y / 16;
				GLubyte blockValue = blockValues[yBlock * 4 + xBlock];
				pixels[(y * width + x) * 3 + 0] = blockValue;
				pixels[(y * width + x) * 3 + 1] = blockValue;
				pixels[(y * width + x) * 3 + 2] = blockValue;
			}
		}

		// add texture to texture manager
		GLuint textureId = _game->GetTextureManager()->CreateTexture("panel", pixels, width, height, GL_RGB, GL_RGB);

		delete pixels;

		return textureId;
}

Tile::Tile()
{
	SetLevel(0);
	SetType(TILE_FLAT);
}

Tile::Tile(int tileLevel, TileType tileType)
{
	SetType(tileType);
	SetLevel(tileLevel);
}

Tile::~Tile()
{
}

int Tile::GetLevelTopLeft() const
{
	switch(_type)
	{
	case TILE_RAMP_NORTH:
		return _level + 1;
	case TILE_RAMP_EAST:
		return _level;
	case TILE_RAMP_SOUTH:
		return _level;
	case TILE_RAMP_WEST:
		return _level + 1;
	case TILE_BLANK:
		return _level;
	case TILE_FLAT:
		return _level;
	default:
		return _level;
	}
}

int Tile::GetLevelTopRight() const
{
	switch(_type)
	{
	case TILE_RAMP_NORTH:
		return _level + 1;
	case TILE_RAMP_EAST:
		return _level + 1;
	case TILE_RAMP_SOUTH:
		return _level;
	case TILE_RAMP_WEST:
		return _level;
	case TILE_BLANK:
		return _level;
	case TILE_FLAT:
		return _level;
	default:
		return _level;
	}
}

int Tile::GetLevelBottomLeft() const
{
	switch(_type)
	{
	case TILE_RAMP_NORTH:
		return _level;
	case TILE_RAMP_EAST:
		return _level;
	case TILE_RAMP_SOUTH:
		return _level + 1;
	case TILE_RAMP_WEST:
		return _level + 1;
	case TILE_BLANK:
		return _level;
	case TILE_FLAT:
		return _level;
	default:
		return _level;
	}
}

int Tile::GetLevelBottomRight() const
{
	switch(_type)
	{
	case TILE_RAMP_NORTH:
		return _level;
	case TILE_RAMP_EAST:
		return _level + 1;
	case TILE_RAMP_SOUTH:
		return _level + 1;
	case TILE_RAMP_WEST:
		return _level;
	case TILE_BLANK:
		return _level;
	case TILE_FLAT:
		return _level;
	default:
		return _level;
	}
}

bool Map::FindNextTileAlongRay(const Ray &ray, int *currentTileRowIndex, int *currentTileColIndex, Tile *tileOut)
{
	int lastTileRowIndex = *currentTileRowIndex;
	int lastTileColIndex = *currentTileColIndex;
	bool isGoingEast = ray.direction.X > 0;
	bool isGoingNorth = ray.direction.Z > 0;

	int nextTileRowIndex = isGoingNorth ? lastTileRowIndex + 1 : lastTileRowIndex - 1;
	int nextTileColIndex = isGoingEast ? lastTileColIndex + 1 : lastTileColIndex - 1;

	// find out whether it is going to get to next row of tiles or next column of tiles first
	float nextRowStartZ = isGoingNorth ? nextTileRowIndex * TILE_WIDTH : (nextTileRowIndex + 1) * TILE_WIDTH;
	float nextColStartX = isGoingEast ? nextTileColIndex * TILE_WIDTH : (nextTileColIndex + 1) * TILE_WIDTH;
	float colFactor = fabs(ray.direction.X) > 0.0001f ? (nextColStartX - ray.start.X) / ray.direction.X : 1e30f;
	float rowFactor = fabs(ray.direction.Z) > 0.0001f ? (nextRowStartZ - ray.start.Z) / ray.direction.Z : 1e30f;

	Vector3 rayEnd = GMath::AddVectors(ray.start, GMath::VecScalarMult(ray.direction, ray.length));

	if (colFactor < rowFactor)
	{
		// next column is first so return that tile if it exists and is within range of ray
		*currentTileColIndex = nextTileColIndex;
		if (nextTileColIndex >= _tilesAcross || nextTileColIndex < 0 // does it exists?
			|| lastTileRowIndex >= _tilesDeep || lastTileRowIndex < 0
			|| (isGoingEast && nextColStartX > rayEnd.X) // is it within range going east?
			|| (!isGoingEast && nextColStartX < rayEnd.X)) // is it within range going west?
		{
			return false;
		}
		else
		{
			*tileOut = _tiles[lastTileRowIndex][nextTileColIndex];
			return true;
		}
	}
	else
	{
		// next row is first so return that tile if it exists and is within range of ray
		*currentTileRowIndex = nextTileRowIndex;
		if (nextTileRowIndex >= _tilesDeep || nextTileRowIndex < 0 // does it exist?
			|| lastTileColIndex >= _tilesAcross || lastTileColIndex < 0
			|| (isGoingNorth && nextRowStartZ > rayEnd.Z) // is it within range going north?
			|| (!isGoingNorth && nextRowStartZ < rayEnd.Z)) // is it within range going south?
		{
			return false;
		}
		else
		{
			*tileOut = _tiles[nextTileRowIndex][lastTileColIndex];
			return true;
		}
	}
}

CollisionResult Map::CalculateRayTileCollision(const Ray &ray, const Tile &tile, int tileRowIndex, int tileColIndex)
{
	float collisionDistance = 1e30f;
	std::vector<Triangle> triangles = tile.GetTriangles();
	int triangleCount = triangles.size();

	for (int i = 0; i < triangleCount; i++) {
		Triangle t = triangles[i];
		collisionDistance = GMath::Min(collisionDistance, GMath::RayTriangleIntersection(ray.start, ray.direction, t.v1, t.v3, t.v2, collisionDistance));
	}

	if (collisionDistance <= ray.length)
	{
		Vector3 collisionPoint;
		collisionPoint.X = ray.start.X + ray.direction.X * collisionDistance;
		collisionPoint.Y = ray.start.Y + ray.direction.Y * collisionDistance;
		collisionPoint.Z = ray.start.Z + ray.direction.Z * collisionDistance;
		return CollisionResult(MapCollision, collisionPoint, collisionDistance);
	}
	else
	{
		return CollisionResult(NoCollision);
	}
}

