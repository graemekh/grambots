#pragma once

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>
#include "BoneData.h"
#include "MyOpenGL.h"
#include "CommonStructs.h"

class AnimatedMeshData
{
public:
	AnimatedMeshData(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float normals[], BoneData **bones, BoneData *rootBone, int boneCount, int frameCount, std::map<std::string, int> specialVerts);
	~AnimatedMeshData();
	static AnimatedMeshData *LoadFromFile(std::string fileName);

	float *VertexData;
	float *NormalData;
	int VertexCount;
	GLushort *IndexData;
	int TriangleCount;
	BoneData *RootBone;
	BoneData **Bones;
	int BoneCount;
	int FrameCount;
	std::map<std::string, int> SpecialVertices;

private:
	static std::pair<std::string, int> GetSpecialVertPairFromLine(std::string line);
	static int GetIntFromHeaderLine(std::string line);
	static void GetVertexDataFromLine(std::string line, Vector3 &vertex, Vector3 &normal);
	static Face GetTriangleFromLine(std::string line);
	static BoneData *LoadBonesFromLine(std::string line, std::string indent, std::ifstream *inStream, std::map<std::string, BoneData *> *boneMap, int frameCount, int *boneIndex, BoneData **bonesArray, std::vector<BoneData *> bonePath);

	static std::vector<std::string> &SplitString(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> SplitString(const std::string &s, char delim);
};