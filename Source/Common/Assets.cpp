#include "Assets.h"

#ifdef OS_MAC
#include "CoreFoundation/CoreFoundation.h"
#endif

std::string Assets::GetAssetPath(std::string relativePath)
{
    return JoinPaths(Assets::GetAssetBasePath(), relativePath);
}

std::string Assets::JoinPaths(std::string part1, std::string part2)
{
    // if one of the strings is empty then there is nothing to combine so just return the other
    if (part1.size() == 0)
        return part2;
    else if (part2.size() == 0)
        return part1;
    
    // make sure the first string ends with a slash
    char lastCharInPart1 = part1[part1.size() - 1];
    if (lastCharInPart1 != '/' && lastCharInPart1 != '\\')
        part1 += '/';
    
    // make sure the second string DOES NOT start with a slash
    char firstCharInPart2 = part2[0];
    if (firstCharInPart2 == '/' || firstCharInPart2 == '\\')
        part2 = part2.substr(1, part2.size() - 1);
    
    std::string fullPath = part1 + part2;
    
    return fullPath;
}

#ifdef OS_WINDOWS
std::string Assets::GetAssetBasePath()
{
	return std::string("C:\\Dev\\Grambots\\Assets\\");
}
#endif

#ifdef OS_LINUX
std::string Assets::GetAssetBasePath()
{
	return std::string("/home/graeme/dev/grambots/Assets");
}
#endif

#ifdef OS_MAC
std::string Assets::GetAssetBasePath()
{
	CFBundleRef mainBundle = CFBundleGetMainBundle();
	CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
	char path[PATH_MAX];
	if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, PATH_MAX))
	{
		// error!
	}
	CFRelease(resourcesURL);

	return JoinPaths(std::string(path), std::string("Assets"));
}
#endif
