#include "Bone.h"

Bone::Bone(BoneData *data, Bone *parent)
{
	_boneData = data;
	_childCount = data->ChildCount;
	_children = new Bone *[_childCount];
	_parent = parent;
	_torque = Vector3();

	for (int i = 0; i < _childCount; i++)
		_children[i] = new Bone(data->Children[i], this);
}

Bone::~Bone()
{
	for (int i = 0; i < _childCount; i++)
		delete _children[i];
	delete _children;
}