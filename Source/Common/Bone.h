#pragma once

#include "CommonStructs.h"
#include "BoneData.h"

class Bone
{
public:
	Bone(BoneData *boneData, Bone *parent);
	~Bone();

	Bone *GetParent() { return _parent; }

	BoneData *GetBoneData() { return _boneData; }
	int GetChildCount() { return _childCount; }
	Bone **GetChildren() { return _children; }

	Vector3 GetTorque() { return _torque; }
	int GetIndex() { return _index; }

	void SetTorque(Vector3 newTorque) { _torque = newTorque; }
	void SetIndex(int newIndex) { _index = newIndex; }

private:
	Vector3 _torque;
	int _index;

	Bone *_parent;
	BoneData *_boneData;
	int _childCount;
	Bone **_children;
};