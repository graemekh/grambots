#include "Logger.h"
#include <iostream>

#ifdef OS_WINDOWS
	#include <Windows.h>
#endif

void Logger::DebugMsg(char *msg)
{
#ifdef OS_WINDOWS
    OutputDebugString(msg);
#else
	std::cout << msg << std::endl;
#endif
}

void Logger::DebugMsg(std::string msg)
{
#ifdef OS_WINDOWS
    OutputDebugString(msg.c_str());
#else
	std::cout << msg << std::endl;
#endif
}

void Logger::DebugMsg(std::wstring msg)
{
#ifdef OS_WINDOWS
    OutputDebugStringW(msg.c_str());
#else
	std::cout << "TODO: implement DebugMsg(std::wstring) for non windows :)" << std::endl;
#endif
}