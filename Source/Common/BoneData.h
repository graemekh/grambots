#pragma once

#include "CommonStructs.h"
#include <string>

class BoneData
{
public:

	BoneData();
	~BoneData();

	BoneData **Path;
	int PathSize;
	std::string Name;
	Vector3 Head;
	Vector3 Tail;
	Vector3 Direction;
	Quaternion *Frames;
	Matrix33 *MatrixFrames;
	int ChildCount;
	BoneData **Children;
	int *Vertices;
	int VertexCount;
	int FrameCount;
	int Index;
	int DirectVertexCount;
	int *DirectVertices;
};