#include "StaticMeshData.h"
#include "GMath.h"

StaticMeshData::StaticMeshData(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount)
{
	this->VertexData = vertexData;
	this->VertexCount = vertexCount;
	this->IndexData = indexData;
	this->TriangleCount = triangleCount;
}

StaticMeshData::~StaticMeshData()
{
	delete[] VertexData;
	delete[] IndexData;
}

StaticMeshData *StaticMeshData::LoadFromFile(std::string fileName)
{
	float *vertexData;
	GLushort *indexData;
	int vertexCount, triangleCount;

	std::string line;
    const char *fileNameAsCStr = fileName.c_str();
	std::ifstream inStream(fileNameAsCStr);

	int vertIndex = 0, triangleIndex = 0;

	while (std::getline(inStream, line))
	{
		if (line.find("vertexCount=") == 0)
		{
			vertexCount = GetIntFromHeaderLine(line);
			vertexData = new float[vertexCount * 3];
		}
		else if (line.find("triangleCount") == 0)
		{
			triangleCount = GetIntFromHeaderLine(line);
			indexData = new GLushort[triangleCount * 3];
		}
		else if (line[0] == 'v')
		{
			Vector3 vertexPos = GetVertexFromLine(line);
			vertexData[vertIndex * 3 + 0] = vertexPos.X;
			vertexData[vertIndex * 3 + 1] = vertexPos.Y;
			vertexData[vertIndex * 3 + 2] = vertexPos.Z;
			vertIndex++;
		}
		else if (line[0] == 't')
		{
			Face triangle = GetTriangleFromLine(line);
			indexData[triangleIndex * 3 + 0] = triangle.v1;
			indexData[triangleIndex * 3 + 1] = triangle.v2;
			indexData[triangleIndex * 3 + 2] = triangle.v3;
			triangleIndex++;
		}
	}

	return new StaticMeshData(vertexData, vertexCount, indexData, triangleCount);
}

int StaticMeshData::GetIntFromHeaderLine(std::string line)
{
	int first = line.find('=') + 1;
	int last = line.find_last_of("0123456789");
	std::string intString = line.substr(first, last - first + 1);
	int value;
	std::istringstream(intString) >> value;
	return value;
}

Vector3 StaticMeshData::GetVertexFromLine(std::string line)
{
	std::vector<std::string> tokens = SplitString(line, '|');
	Vector3 vec;
	std::istringstream(tokens[1]) >> vec.X;
	std::istringstream(tokens[2]) >> vec.Y;
	std::istringstream(tokens[3]) >> vec.Z;
	return vec;
}

Face StaticMeshData::GetTriangleFromLine(std::string line)
{
	std::vector<std::string> tokens = SplitString(line, '|');
	Face tri;
	std::istringstream(tokens[1]) >> tri.v1;
	std::istringstream(tokens[2]) >> tri.v2;
	std::istringstream(tokens[3]) >> tri.v3;
	return tri;
}

std::vector<std::string> &StaticMeshData::SplitString(const std::string &s, char delim, std::vector<std::string> &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> StaticMeshData::SplitString(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    return SplitString(s, delim, elems);
}