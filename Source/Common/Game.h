#pragma once

#include "GStdio.h"
#include "GMath.h"
#include "Logger.h"
#include "Assets.h"
#include "TextureManager.h"
#include "ShaderManager.h"
#include "MyOpenGL.h"
#include "CommonStructs.h"
#include <string>
#include <vector>
#include <map>
#include <memory>

#define MAX_BOTS_PER_TEAM 500

class Game
{
public:
    Game();
    virtual ~Game();

	TextureManager *GetTextureManager() { return _textureManager; }
	ShaderManager *GetShaderManager() { return _shaderManager; }
	Vector3 GetCameraPosition() { return _cameraPosition; }
	
	static std::string ErrorCheck();

	virtual CollisionResult RayWorldIntersection(const Ray &ray, void *objectToIgnore = NULL, int collisionTypeMask = 0) = 0;

protected:

	float _cameraPitch;
	float _cameraHeading;
	float _cameraBank;
	Vector3 _cameraPosition;
	float _cameraRotationMatrix[16];
	Vector2 _cameraHeadingVector;

	void RotateCameraWithMouse(int deltaX, int deltaY);
	void UpdateCameraRotation();
	void UpdateProjectiles(float deltaTime);
	void MoveCameraLeft(float deltaTime);
	void MoveCameraRight(float deltaTime);
	void MoveCameraForward(float deltaTime);
	void MoveCameraBackward(float deltaTime);
	void MoveCameraUp(float deltaTime);
	void MoveCameraDown(float deltaTime);
	void RotateCameraCcwAroundY(float deltaTime);
	void RotateCameraCwAroundY(float deltaTime);
private:


	TextureManager *_textureManager;
	ShaderManager *_shaderManager;
};

