#include "Grambots.h"

#ifdef OS_WINDOWS
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
int main(int argc, char *args[])
#endif
{
	Grambots game;
	game.Begin();
	game.Run();
	game.End();

    return 0;
}