#pragma once

#include "StaticMesh.h"

class SkyBox
{
public:
	SkyBox(GLuint textureId);
	~SkyBox();
	void Render();

private:
	StaticMesh *_mesh;
	GLuint _textureId;
};
