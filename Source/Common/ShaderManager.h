#pragma once

#include "MyOpenGL.h"
#include <map>
#include <string>
#include <vector>

class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

	GLuint LoadShadersFromCode(std::string name, std::string vertexShaderCode, std::string pixelShaderCode);
	GLuint LoadShaders(std::string name, std::string vertexShaderPath, std::string pixelShaderPath);
	GLuint LoadShaders(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, std::vector<std::pair<std::string, std::string> > textReplacements);
	void DeleteShaders(std::string name);
	GLuint GetShaderProgramId(std::string name);
private:
	std::map<std::string, GLuint> _shaderMap;
	std::string ReadAllText(std::string path);
	std::string replaceAll(std::string result, const std::string& replaceWhat, const std::string& replaceWithWhat);
};