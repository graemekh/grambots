#pragma once

#include "MyOpenGL.h"
#include <map>
#include <string>
#include <vector>
#include "StaticMesh.h"

struct CharInfo
{
	CharInfo()
	{
		Row = 0;
		Col = 0;
	}

	CharInfo(int row, int col)
	{
		Row = row;
		Col = col;
	}

	int Row, Col;
};

class Font
{
public:
	Font(std::vector<CharInfo> characters, GLuint textureId, int textureWidth, int textureHeight, int charWidth, int charHeight);
	int GetCharWidth() { return _charWidth; }
	int GetCharHeight() { return _charHeight; }
	int GetTextureWidth() { return _textureWidth; }
	int GetTextureHeight() { return _textureHeight; }
	GLint GetTextureId() { return _textureId; }
	std::vector<CharInfo> GetCharInfos() { return _characters; }

private:
	std::vector<CharInfo> _characters;
	GLuint _textureId;
	int _textureWidth;
	int _textureHeight;
	int _charWidth;
	int _charHeight;
};