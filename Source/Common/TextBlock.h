#pragma once

#include <string>
#include "Font.h"
#include "CommonStructs.h"

class TextBlock
{
public:
	TextBlock(int size, Font *font, std::string text);
	void SetText(std::string text);
	void Render();
	Vector2 GetPosition() { return _position; }
	void SetPosition(Vector2 position) { _position = position; }

private:
	void InitMesh(std::string text);
	std::vector<float> GetUvCoords(std::string text);

	Font *_font;
	int _size;
	Vector2 _position;

	GLuint _vertexBufferId;
    GLuint _indexBufferId;
	GLuint _uvBufferId;
};