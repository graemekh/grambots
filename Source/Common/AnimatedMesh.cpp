#pragma inline_recursion(on)

#include "AnimatedMesh.h"
#include "GMath.h"

AnimatedMesh::AnimatedMesh(AnimatedMeshData *data)
{
	Data = data;
	RootBone = new Bone(data->RootBone, NULL);
	_originalVertexData = data->VertexData;
    _polygonCount = data->TriangleCount;
	_vertexCount = data->VertexCount;
	_vertexData = new float[data->VertexCount * 3];

    // create vertex buffer
    glGenBuffers(1, &_vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, _vertexCount * 3 * sizeof(float), _vertexData, GL_STATIC_DRAW);

    // create index buffer
    glGenBuffers(1, &_indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * _polygonCount * sizeof(GLushort), data->IndexData, GL_STATIC_DRAW);

	// craete normal buffer
	glGenBuffers(1, &_normalBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
	glBufferData(GL_ARRAY_BUFFER, _vertexCount * 3 * sizeof(float), data->NormalData, GL_STATIC_DRAW);
}

AnimatedMesh::~AnimatedMesh()
{
    glDeleteBuffers(1, &_vertexBufferId);
    glDeleteBuffers(1, &_indexBufferId);
	delete[] _vertexData;
	delete RootBone;
}

//void AnimatedMesh::UpdateMeshToBoneAtFrame(Bone *bone)
//{
//	int childCount = bone->GetChildCount();
//	Bone **children = bone->GetChildren();
//	for (int childIndex = 0; childIndex < childCount; childIndex++)
//		UpdateMeshToBoneAtFrame(children[childIndex]);
//
//	BoneData *boneData = bone->GetBoneData();
//	static float x, y, z;
//	static int xIndex, yIndex, zIndex;
//	int *vertices = boneData->Vertices;
//	Vector3 bonePos = boneData->Position;
//	Matrix33 m;
//	int boneVertCount = boneData->VertexCount;
//
//	if (bone->GetIsInterpolating())
//	{
//		Quaternion q0 = boneData->Frames[bone->GetFrame()];
//		Quaternion q1 = boneData->Frames[bone->GetFrameInterpolatingTo()];
//		m = GMath::InterpolateQuaternionsToMatrix(q0, q1, bone->GetInterpolationFactor());
//	}
//	else
//	{
//		m = boneData->MatrixFrames[bone->GetFrame()];
//	}
//
//	bone->SetCurrentRotationMatrix(m);
//	
//	for (int i = 0; i < boneVertCount; i++)
//	{
//		xIndex = vertices[i] * 3;
//		yIndex = xIndex + 1;
//		zIndex = yIndex + 1;
//
//		float x1 = _vertexData[xIndex];
//		float y1 = _vertexData[yIndex];
//		float z1 = _vertexData[zIndex];
//
//		x = _vertexData[xIndex] - bonePos.X;
//		y = _vertexData[yIndex] - bonePos.Y;
//		z = _vertexData[zIndex] - bonePos.Z;
//		
//		float ix = m.v11 * x + m.v21 * y + m.v31 * z;
//		float iy = m.v12 * x + m.v22 * y + m.v32 * z;
//		float iz = m.v13 * x + m.v23 * y + m.v33 * z;
//
//		_vertexData[xIndex] = ix + bonePos.X;
//		_vertexData[yIndex] = iy + bonePos.Y;
//		_vertexData[zIndex] = iz + bonePos.Z;
//	}
//}

//void AnimatedMesh::UpdateMesh()
//{
//	int vertDataSize = _vertexCount * 3;
//	for(int i = 0; i < vertDataSize; i++)
//		_vertexData[i] = _originalVertexData[i];	
//	UpdateMeshToBoneAtFrame(RootBone);
//	glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
//	glBufferSubData(GL_ARRAY_BUFFER, 0, _vertexCount * 3 * sizeof(float), _vertexData);
//}

void AnimatedMesh::Render()
{
    glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

    // setup vertex buffer
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glVertexPointer(3, GL_FLOAT, 0, (char *)NULL);

	// setup normal buffer
	glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
	glNormalPointer(GL_FLOAT, 0, (char *)NULL);

    // setup index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);

    // render mesh
    glDrawRangeElements(GL_TRIANGLES, 0, _polygonCount * 3, _polygonCount * 3, GL_UNSIGNED_SHORT, (char *)NULL);

    glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}
