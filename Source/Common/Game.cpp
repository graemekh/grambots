#include "Game.h"

Game::Game()
{
	_textureManager = new TextureManager();
	_shaderManager = new ShaderManager();

    // initialize camera position/rotation
	_cameraPosition = Vector3(35, 10, -10);
	_cameraHeading = PI;
	_cameraPitch = 0;
	_cameraBank = 0;
	_cameraHeadingVector.X = 0;
	_cameraHeadingVector.Y = -1;

	_cameraRotationMatrix[0]  = 1; _cameraRotationMatrix[1]  = 0; _cameraRotationMatrix[2]  = 0; _cameraRotationMatrix[3]  = 0;
	_cameraRotationMatrix[4]  = 0; _cameraRotationMatrix[5]  = 1; _cameraRotationMatrix[6]  = 0; _cameraRotationMatrix[7]  = 0;
	_cameraRotationMatrix[8]  = 0; _cameraRotationMatrix[9]  = 0; _cameraRotationMatrix[10] = 1; _cameraRotationMatrix[11] = 0;
	_cameraRotationMatrix[12] = 0; _cameraRotationMatrix[13] = 0; _cameraRotationMatrix[14] = 0; _cameraRotationMatrix[15] = 1;

	UpdateCameraRotation();


}

Game::~Game()
{
	delete _textureManager;
	delete _shaderManager;
}

void Game::RotateCameraCcwAroundY(float deltaTime)
{
	_cameraHeading += deltaTime * 0.002f;
}

void Game::RotateCameraCwAroundY(float deltaTime)
{
	_cameraHeading -= deltaTime * 0.002f;
}

void Game::RotateCameraWithMouse(int deltaX, int deltaY)
{
	_cameraHeading -= deltaX * 0.004f;
	_cameraPitch -= deltaY * 0.004f;
}

void Game::MoveCameraLeft(float deltaTime)
{
	_cameraPosition.Z -= deltaTime / 40.0f * _cameraHeadingVector.X * -1;
	_cameraPosition.X -= deltaTime / 40.0f * _cameraHeadingVector.Y;
}

void Game::MoveCameraRight(float deltaTime)
{
	_cameraPosition.Z += deltaTime / 40.0f * _cameraHeadingVector.X * -1;
	_cameraPosition.X += deltaTime / 40.0f * _cameraHeadingVector.Y;
}

void Game::MoveCameraBackward(float deltaTime)
{
	_cameraPosition.Z += deltaTime / 40.0f * _cameraHeadingVector.Y;
	_cameraPosition.X += deltaTime / 40.0f * _cameraHeadingVector.X;
}

void Game::MoveCameraForward(float deltaTime)
{
	_cameraPosition.Z -= deltaTime / 40.0f * _cameraHeadingVector.Y;
	_cameraPosition.X -= deltaTime / 40.0f * _cameraHeadingVector.X;
}

void Game::MoveCameraUp(float deltaTime)
{
	_cameraPosition.Y += deltaTime / 40.0f;
}

void Game::MoveCameraDown(float deltaTime)
{
	_cameraPosition.Y -= deltaTime / 40.0f;
}

void Game::UpdateCameraRotation()
{
	// update rotation matrix
	float sinH = sin(_cameraHeading);
	float sinP = sin(_cameraPitch);
	float sinB = sin(_cameraBank);
	float cosH = cos(_cameraHeading);
	float cosP = cos(_cameraPitch);
	float cosB = cos(_cameraBank);

	_cameraRotationMatrix[0]  = cosH*cosB + sinH*sinP*sinB;
	_cameraRotationMatrix[1]  = -cosH*sinB + sinH*sinP*cosB; 
	_cameraRotationMatrix[2]  = sinH*cosP;

	_cameraRotationMatrix[4]  = sinB*cosP;
	_cameraRotationMatrix[5]  = cosB*cosP;
	_cameraRotationMatrix[6]  = -sinP;

	_cameraRotationMatrix[8]  = -sinH*cosB + cosH*sinP*sinB;
	_cameraRotationMatrix[9]  = sinB*sinH + cosH*sinP*cosB;
	_cameraRotationMatrix[10] = cosH*cosP;
	
	// update heading vector
	_cameraHeadingVector.X = sinH;
	_cameraHeadingVector.Y = cosH;
}

std::string Game::ErrorCheck()
{
	GLenum errCode = glGetError();

	if (errCode != GL_NO_ERROR)
		return std::string((const char *)gluErrorString(errCode));
	else
		return std::string(":-)");
}
