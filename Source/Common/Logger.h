#pragma once

#include <string>

class Logger
{
public:
	static void DebugMsg(char *msg);
	static void DebugMsg(std::string msg);
	static void DebugMsg(std::wstring msg);
};