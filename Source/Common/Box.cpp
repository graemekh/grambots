//#include "MyOpenGL.h"
//#include "Box.h"
//#include "CommonStructs.h"
//#include "GMath.h"
//
//Box::Box(float width, float height, float depth, Vector3 position, TextureManager *textureManager, ShaderManager *shaderManager)
//{
//    _width = width;
//    _height = height;
//    _depth = depth;
//	_shaderId = shaderManager->GetShaderProgramId("block");
//
//	_transform[0] = 1.0f; _transform[1] = 0; _transform[2] = 0; _transform[3] = 0; 
//	_transform[4] = 0; _transform[5] = 1.0f; _transform[6] = 0; _transform[7] = 0; 
//	_transform[8] = 0; _transform[9] = 0; _transform[10] = 1.0f; _transform[11] = 0; 
//	_transform[12] = 0; _transform[13] = 0; _transform[14] = 0; _transform[15] = 1.0f;
//
//    float vertices[] = {
//        -width/2,  height/2,  depth/2,  // 0
//         width/2,  height/2,  depth/2,  // 1
//         width/2, -height/2,  depth/2,  // 2
//        -width/2, -height/2,  depth/2,  // 3
//        -width/2,  height/2, -depth/2,  // 4
//         width/2,  height/2, -depth/2,  // 5
//         width/2, -height/2, -depth/2,  // 6
//        -width/2, -height/2, -depth/2   // 7
//    };
//
//	float uvcoords[] = {
//		0, 0,
//		1, 0,
//		1, 1,
//		0, 1,
//		//
//		0, 0,
//		1, 0,
//		1, 1,
//		0, 1
//	};
//
//	// sqrt(3) is the length of vector {1,1,1} so 1/sqrt(3) is normalized axis component
//	float n = 1 / sqrt(3.0f);
//
//	float normals[] = {
//		-n,  n,  n,
//		 n,  n,  n,
//		 n, -n,  n,
//		-n, -n,  n,
//		-n,  n, -n,
//		 n,  n, -n,
//		 n, -n, -n,
//		-n, -n, -n
//	};
//
//    //GLushort indices[] = {
//    //    0, 3, 2, 1, // front
//    //    5, 6, 7, 4, // back
//    //    1, 2, 6, 5, // right
//    //    4, 7, 3, 0, // left
//    //    4, 0, 1, 5, // top
//    //    3, 7, 6, 2  // bottom
//    //};
//	
//    GLushort indices[] = {
//        0, 3, 2, // front
//        0, 2, 1,
//        5, 6, 7, // back
//        5, 7, 4,
//        1, 6, 5, // right
//        1, 2, 6, 
//        4, 7, 3, // left
//        4, 3, 0, 
//        4, 0, 1, // top
//        4, 1, 5,
//        3, 7, 2, // bottom
//        7, 6, 2
//    };
//
//    _mesh = StaticMesh::CreateTexturedTriangleMesh(vertices, 8, indices, 12, NULL, uvcoords, textureManager->GetTexture("satin"), normals);
//}
//
//Box::~Box()
//{
//    delete _mesh;
//}
//
//void Box::OnRender()
//{
//	glUseProgram(_shaderId);
//	glPushMatrix();
//	glMultMatrixf(_transform);
//
//	glTranslatef(30, 30, 30);
//
//	glFrontFace(GL_CCW);
//	//glColor3f( 1.0f, 0.5f, 0.5f );
//
//    _mesh->Render();
//
//	//glColor3f( 0.0f, 0.0f, 0.0f );
//	//glLineWidth(2);
//	//glPolygonMode(GL_FRONT, GL_LINE);
//    //glFrontFace(GL_CCW);
//
//	//_mesh->Render();
//
//	glPopMatrix();
//	glUseProgram(NULL);
//}