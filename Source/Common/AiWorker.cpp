#include "Game.h"
#include "AiWorker.h"
#include <assert.h>

int AiThreadStarter(void *context)
{
	AiWorker *worker = (AiWorker *)context;
	worker->Run();
	return EXIT_SUCCESS;
}

AiWorker::AiWorker(Team *team, Map *map)
{
	_team = team;
	_isRunning = false;
	_map = map;
	_framesSinceLastFpsUpdate = 0;
	_timeOfLastFpsUpdate = SDL_GetTicks();
	_timeOfLastExecutedFrame = _timeOfLastFpsUpdate;
	_fps = 0;
	_inputCommandBuffer = new AsyncMessageBuffer<AiCommand>(AI_COMMAND_BUFFER_CAPACITY);

	// init A* memory pool
	_mapNodeCount = _map->GetTilesAcross() * _map->GetTilesDeep();
	_astar = new AStarNode[_mapNodeCount];
	ZeroOutAStarMemoryPool();
}

AiWorker::~AiWorker() 
{
	delete _inputCommandBuffer;
	delete _astar;
}

void AiWorker::Start()
{
	_isRunning = true;
	_thread = SDL_CreateThread(AiThreadStarter, (void *)this);
}

void AiWorker::Run()
{
	while (_isRunning)
	{
		int now = SDL_GetTicks();

		// share cpu resources by yielding control to another thread if this one has already had enough time recently
		if ((now - _timeOfLastExecutedFrame) < MIN_MILLIS_BETWEEN_AI_FRAMES)
		{
			SDL_Delay(0);
			continue;
		}

		// update ai fps info
		if ((now - _timeOfLastFpsUpdate) >= 1000)
		{
			_fps = _framesSinceLastFpsUpdate;
			_timeOfLastFpsUpdate = now;
			_framesSinceLastFpsUpdate = 1;
		}
		else
		{
			_framesSinceLastFpsUpdate++;
		}

		_timeOfLastExecutedFrame = now;

		while (true)
		{
			AiCommand cmd;
			bool hasAiCommand = _inputCommandBuffer->Read(&cmd);
			if (hasAiCommand)
			{
				AiFrame *availableFrame = cmd.bot->GetAiFrameBuffer()->GetInactiveFrame();
				ProcessAiCommand(cmd, availableFrame);
			}
			else
			{
				break;
			}
		}

		Bot **bots = _team->GetBots();
		for (unsigned int i = 0; i < MAX_BOTS_PER_TEAM; i++)
		{
			Bot *bot = bots[i];

			AiFrame *availableFrame = bot->GetAiFrameBuffer()->GetInactiveFrame();
			AiFrame *activeFrame = bot->GetAiFrameBuffer()->GetActiveFrame();
			
			// If there is no frame ready for the AI thread to modify it, then do nothing for now
			if (availableFrame == NULL)
				continue;

			DecideWhatToDo(bot, availableFrame, activeFrame);
		}
	}
}

void AiWorker::Stop() 
{
	_isRunning = false;
}

void AiWorker::WaitUntilFinished()
{
	int status;
	SDL_WaitThread(_thread, &status);
}

void AiWorker::DecideWhatToDo(Bot *bot, AiFrame *availableFrame, AiFrame *activeFrame)
{
	if (NeedToMakeNewDecision(availableFrame, activeFrame))
	{
		float x = GMath::GetRandomNumber(52.0f, 360.0f);
		float z = GMath::GetRandomNumber(52.0f, 360.0f);

		availableFrame->command.doNothing = false;
		
		BotState botState = bot->GetStateBuffer().GetActiveFrame();

		Vector2 currentPosition(botState.position.X, botState.position.Z);
		Vector2 destination(x, z);

		availableFrame->command.finalDestination = destination;

		FindPathWithAStar(
			GetNodeIndexAtPosition(currentPosition), 
			GetNodeIndexAtPosition(destination),
			&availableFrame->command.path);

		availableFrame->command.isTravelling = availableFrame->command.path.segmentCount > 0;
		if (!availableFrame->command.isTravelling)
			availableFrame->command.doNothing = true;

		ZeroOutAStarMemoryPool();

		availableFrame->isComplete = false;
		availableFrame->isActive = true;
	}
}

void AiWorker::ProcessAiCommand(AiCommand command, AiFrame *availableFrame)
{
	availableFrame->command.doNothing = false;
		
	BotState botState = command.bot->GetStateBuffer().GetActiveFrame();

	Vector2 currentPosition(botState.position.X, botState.position.Z);
	Vector2 destination(command.destination);

	availableFrame->command.finalDestination = destination;

	FindPathWithAStar(
		GetNodeIndexAtPosition(currentPosition), 
		GetNodeIndexAtPosition(destination),
		&availableFrame->command.path);

	availableFrame->command.isTravelling = availableFrame->command.path.segmentCount > 0;
	if (!availableFrame->command.isTravelling)
		availableFrame->command.doNothing = true;

	ZeroOutAStarMemoryPool();

	availableFrame->isComplete = false;
	availableFrame->isActive = true;
}

bool AiWorker::NeedToMakeNewDecision(AiFrame *availableFrame, AiFrame *activeFrame)
{
	return 
		activeFrame->isComplete || 
		activeFrame->command.doNothing ||
		(activeFrame->command.path.currentSegment >= (activeFrame->command.path.segmentCount - 2) && !activeFrame->command.path.reachesFinalDestination);
}

bool AiWorker::FindPathWithAStar(int startNodeIndex, int goalNodeIndex, Path *result)
{
	_astar[startNodeIndex].isOpen = true;
	_astar[startNodeIndex].hScore = AStarHeuristicCostEstimate(startNodeIndex, goalNodeIndex);
	_astar[startNodeIndex].fScore = _astar[startNodeIndex].fScore;

	AStarNode *bestOpenNode = &_astar[startNodeIndex];
	bestOpenNode->segmentIndex = -1;

	while (bestOpenNode != NULL)
	{
		AStarNode *currentAStarNode = bestOpenNode;//_map->GetTileAtIndex(bestOpenNode->index).GetMapNode();
		MapNode *currentMapNode = _map->GetTileAtIndex(currentAStarNode->index).GetMapNode();

		if (currentAStarNode->index == goalNodeIndex)
		{
			*result = ConstructPath(currentAStarNode->index);
			return true;
		}

		bestOpenNode->isOpen = false;
		bestOpenNode->isClosed = true;
		bestOpenNode = bestOpenNode->nextOpenNode;

		// loop through neighbors
		for (int neighborIndex = 0; neighborIndex < currentMapNode->connectionCount; neighborIndex++)
		{
			MapConnection connection = currentMapNode->connections[neighborIndex];

			// skip if height difference blocks this path
			if (connection.heightDiff > 0.001f)
				continue;

			MapNode *neighborMapNode = connection.connectedNode;
			AStarNode *neighborAStarNode = &_astar[neighborMapNode->index];

			// skip if node already processed
			if (neighborAStarNode->isClosed)
				continue;

			bool tentativeIsBetter = false;
			float tentativeGScore = neighborAStarNode->gScore + AStarGetDistanceBetweenNodes(currentMapNode, neighborMapNode);

			if (!neighborAStarNode->isOpen)
			{
				neighborAStarNode->isOpen = true;
				neighborAStarNode->hScore = AStarHeuristicCostEstimate(neighborMapNode->index, goalNodeIndex);
				tentativeIsBetter = true;
			}
			else if (tentativeGScore < neighborAStarNode->gScore)
			{
				tentativeIsBetter = true;
			}

			if (tentativeIsBetter)
			{
				neighborAStarNode->cameFrom = currentAStarNode;
				neighborAStarNode->segmentIndex = currentAStarNode->segmentIndex + 1;
				neighborAStarNode->gScore = tentativeGScore;
				neighborAStarNode->fScore = neighborAStarNode->gScore + neighborAStarNode->hScore;

				AStarNode *previousNode = NULL;
				AStarNode *nextNode = bestOpenNode;
				while (true)
				{
					if (nextNode != NULL && neighborAStarNode->fScore < nextNode->fScore)
					{
						neighborAStarNode->nextOpenNode = nextNode;
						if (previousNode != NULL)
							previousNode->nextOpenNode = neighborAStarNode;
						else
							bestOpenNode = neighborAStarNode;
						break;
					}

					if (nextNode == NULL)
					{
						if (previousNode != NULL)
							previousNode->nextOpenNode = neighborAStarNode;
						else
							bestOpenNode = neighborAStarNode;
						break;
					}

					previousNode = nextNode;
					nextNode = nextNode->nextOpenNode;
				}
			}
		}
	}

	result->currentSegment = 0;
	result->segmentCount = 0;
	return false;
}

Path AiWorker::ConstructPath(int nodeIndex)
{
	Path path;
	path.currentSegment = 0;
	path.segmentCount = 0;

	AStarNode *node = &_astar[nodeIndex];
	path.reachesFinalDestination = node->segmentIndex >= MAX_PATH_SEGMENTS;

	while (node != NULL)
	{
		if (node->segmentIndex < MAX_PATH_SEGMENTS && node->segmentIndex >= 0)
		{
			path.points[node->segmentIndex] = _map->GetXZPositionAtCenterOfTileAtIndex(node->index);
			path.segmentCount += 1;
		}
		node = node->cameFrom;
	}

	return path;
}

void AiWorker::ZeroOutAStarMemoryPool()
{
	memset(_astar, 0x0, sizeof(AStarNode) * _mapNodeCount);
	for (int i = 0; i < _mapNodeCount; i++)
	{
		_astar[i].index = i;
	}
}

int AiWorker::GetNodeIndexAtPosition(Vector2 pos)
{
	int row = GMath::Min(_map->GetRowAtZ(pos.Y), _map->GetTilesDeep() - 1);
	int col = GMath::Min(_map->GetColAtX(pos.X), _map->GetTilesAcross() - 1);

	return row * _map->GetTilesAcross() + col;
}

float AiWorker::AStarHeuristicCostEstimate(int startNodeIndex, int goalNodeIndex)
{
	MapNode *startNode = _map->GetTileAtIndex(startNodeIndex).GetMapNode();
	MapNode *goalNode = _map->GetTileAtIndex(goalNodeIndex).GetMapNode();
	return AStarGetDistanceBetweenNodes(startNode, goalNode);
}

float AiWorker::AStarGetDistanceBetweenNodes(MapNode *nodeA, MapNode *nodeB)
{
	return (float)(abs(nodeA->row - nodeB->row) + abs(nodeA->col - nodeB->col));
}
