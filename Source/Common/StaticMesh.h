#pragma once

#include "MyOpenGL.h"

class StaticMesh
{
public:
	static StaticMesh *CreateTexturedTriangleMesh(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float colorData[], float uvcoords[], unsigned int textureId, float normalData[]);
    static StaticMesh *CreateTriangleMesh(float vertexData[], int vertexCount, GLushort indexData[], int triangleCount, float colorData[]=NULL);
    static StaticMesh *CreateQuadMesh(float vertexData[], int vertexCount, GLushort indexData[], int quadCount, float colorData[]=NULL);
    virtual ~StaticMesh();
    void Render();

    int PolygonCount;
    int VertsPerPoly;
	int VertexCount;

	float *GetVertexData() { return _vertexData; }

    StaticMesh(float vertexData[], int vertexCount, GLushort indexData[], int polygonCount, int vertsPerPoly, GLenum polygonType, float colorData[]=NULL, float uvcoords[]=NULL, unsigned int textureId=NULL, float normalData[]=NULL);

private:
	bool _hasColorData;
	bool _hasTextureData;
	bool _hasNormalData;
	float *_vertexData;
	GLushort *_indexData;
    GLuint _vertexBufferId;
    GLuint _indexBufferId;
    GLuint _colorBufferId;
	GLuint _normalBufferId;
	GLuint _uvBufferId;
    GLenum _polygonType;
	unsigned int _textureId;
};

