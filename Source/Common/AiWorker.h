#pragma once

#include "Team.h"
#include "Bot.h"
#include "Map.h"
#include "AsyncMessageBuffer.h"

#define MIN_MILLIS_BETWEEN_AI_FRAMES 200
#define AI_COMMAND_BUFFER_CAPACITY 1000

struct AiCommand
{
	AiCommand()
	{
		bot = NULL;
		target = NULL;
	}

	Bot *bot;
	Vector2 destination;
	Bot *target;
};

struct AStarNode
{
	bool isClosed;
	bool isOpen;
	AStarNode *cameFrom;
	float gScore;
	float hScore;
	float fScore;
	AStarNode *nextOpenNode;
	int index;
	int segmentIndex;
};

class AiWorker
{
public:
	AiWorker(Team *team, Map *map);
	~AiWorker();
	void Run();
	void Start();
	void Stop();
	void WaitUntilFinished();
	int GetFps() { return _fps; }
	AsyncMessageBuffer<AiCommand> *GetInputCommandBuffer() { return _inputCommandBuffer; }

private:
	void ProcessAiCommand(AiCommand command, AiFrame *availableFrame);
	void DecideWhatToDo(Bot *bot, AiFrame *availableFrame, AiFrame *activeFrame);
	bool NeedToMakeNewDecision(AiFrame *availableFrame, AiFrame *activeFrame);
	bool FindPathWithAStar(int startNodeIndex, int goalNodeIndex, Path *result);
	Path ConstructPath(int nodeIndex);
	float AStarHeuristicCostEstimate(int startNodeIndex, int goalNodeIndex);
	float AStarGetDistanceBetweenNodes(MapNode *nodeA, MapNode *nodeB);
	
	AsyncMessageBuffer<AiCommand> *_inputCommandBuffer;
	Team *_team;
	SDL_Thread *_thread;
	volatile bool _isRunning;
	Map *_map;

	int _mapNodeCount;
	AStarNode *_astar;
	void ZeroOutAStarMemoryPool();
	int GetNodeIndexAtPosition(Vector2 pos);

	int _timeOfLastFpsUpdate;
	int _timeOfLastExecutedFrame;
	int _framesSinceLastFpsUpdate;
	volatile int _fps;
};

int AiThreadStarter(void *context);
