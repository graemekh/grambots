#include "BoneData.h"

BoneData::BoneData()
{
}

BoneData::~BoneData()
{
	delete Frames;
	delete MatrixFrames;
	for (int i = 0; i < ChildCount; i++)
	{
		delete Children[i];
	}
	delete Children;
	delete Path;
	delete Vertices;
	delete DirectVertices;
}