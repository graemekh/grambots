#include "TextBlock.h"

TextBlock::TextBlock(int size, Font *font, std::string text)
{
	_size = size;
	_font = font;
	InitMesh(text);
}

std::vector<float> TextBlock::GetUvCoords(std::string text)
{
	int charCount = text.size();
	std::vector<float> uvCoords(_size * 4 * 2);
	float quadWidth = (float)_font->GetCharWidth();
	float quadHeight = (float)_font->GetCharHeight();
	float u1, u2, v1, v2, startPixelX, startPixelY;
	CharInfo charInfo;

	for (int i = 0; i < _size; i++)
	{
		if (i >= charCount)
			charInfo = _font->GetCharInfos()[' '];
		else
			charInfo = _font->GetCharInfos()[text[i]];
		
		startPixelX = (float)charInfo.Col * quadWidth;
		startPixelY = (float)charInfo.Row * quadHeight;
		u1 = startPixelX / _font->GetTextureWidth();
		u2 = (startPixelX + quadWidth) / _font->GetTextureWidth();
		v1 = startPixelY / _font->GetTextureHeight();
		v2 = (startPixelY + quadHeight) / _font->GetTextureHeight();

		
		uvCoords[i * 8 + 0] = u1;
		uvCoords[i * 8 + 1] = v2;
		
		uvCoords[i * 8 + 2] = u2;
		uvCoords[i * 8 + 3] = v2;

		uvCoords[i * 8 + 4] = u1;
		uvCoords[i * 8 + 5] = v1;

		uvCoords[i * 8 + 6] = u2;
		uvCoords[i * 8 + 7] = v1;
	}

	return uvCoords;
}

void TextBlock::SetText(std::string text)
{
	// at this point it is assumed that the uv buffer has already been populated by
	// InitMesh() so we are replacing that data with glBufferSubData

	std::vector<float> uvCoords = GetUvCoords(text);

	glBindBuffer(GL_ARRAY_BUFFER, _uvBufferId);
	glBufferSubData(GL_ARRAY_BUFFER, 0, uvCoords.size() * sizeof(float), &uvCoords[0]);
}

void TextBlock::InitMesh(std::string text)
{
	std::vector<float> verts(_size * 4 * 3);
	std::vector<GLushort> indices(_size * 3 * 2);
	float quadWidth = (float)_font->GetCharWidth();
	float quadHeight = (float)_font->GetCharHeight();

	for (int i = 0; i < _size; i++)
	{
		verts[i * 12 + 0] = i * quadWidth;
		verts[i * 12 + 1] = 0.0f;
		verts[i * 12 + 2] = 0.0f;
		
		verts[i * 12 + 3] = (i + 1) * quadWidth;
		verts[i * 12 + 4] = 0.0f;
		verts[i * 12 + 5] = 0.0f;
		
		verts[i * 12 + 6] = i * quadWidth;
		verts[i * 12 + 7] = quadHeight;
		verts[i * 12 + 8] = 0.0f;

		verts[i * 12 + 9] = (i + 1) * quadWidth;
		verts[i * 12 + 10] = quadHeight;
		verts[i * 12 + 11] = 0.0f;

		indices[i * 6 + 0] = i * 4 + 0;
		indices[i * 6 + 1] = i * 4 + 2;
		indices[i * 6 + 2] = i * 4 + 1;

		indices[i * 6 + 3] = i * 4 + 1;
		indices[i * 6 + 4] = i * 4 + 2;
		indices[i * 6 + 5] = i * 4 + 3;
	}

	std::vector<float> uvCoords = GetUvCoords(text);

	// create vertex buffer
    glGenBuffers(1, &_vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(float), &verts[0], GL_STATIC_DRAW);

	// create uv coord buffer (make it dynamic so the text can change)
	glGenBuffers(1, &_uvBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, _uvBufferId);
	glBufferData(GL_ARRAY_BUFFER, uvCoords.size() * sizeof(float), &uvCoords[0], GL_DYNAMIC_DRAW);

	// create index buffer
    glGenBuffers(1, &_indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

	// Set the text to init texture uvcoords
	SetText(text);
}
void TextBlock::Render()
{
	// setup vertex buffer
    glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glVertexPointer(3, GL_FLOAT, 0, (char *)NULL);

	// setup uv coord buffer
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, _uvBufferId);
	glTexCoordPointer(2, GL_FLOAT, 0, (char *)NULL);

	// bind the actual texture
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _font->GetTextureId());

	// setup index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferId);

	glPushMatrix();
	glTranslatef(_position.X, _position.Y, 0.0f);

	// render mesh
	glDrawElements(GL_TRIANGLES, _size * 8, GL_UNSIGNED_SHORT, NULL);

	glPopMatrix();

	// disable VBO features that were enabled at top of function
    glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisable(GL_TEXTURE_2D);
}