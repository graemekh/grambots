#include "Grambots.h"

Grambots::Grambots() : Game()
{
	_prevMillis = 0;
    _framesSinceFpsUpdate = 0;
    _millisAtFpsUpdate = 0;
	_isPaused = false;
	_time = 0.0f;

	_cameraPitch = -PI_OVER_TWO / 2.0f;
	_cameraPosition = Vector3(200.0f, 175.0f, -25.0f);

	UpdateCameraRotation();
}

Grambots::~Grambots()
{
	unsigned int teamCount = _teams.size();
	for (unsigned int i = 0; i < teamCount; i++)
		delete _teams[i];

	unsigned int aiWorkerCount = _aiWorkers.size();
	for (unsigned int i = 0; i < aiWorkerCount; i ++)
	{
		delete _aiWorkers[i];
		delete _aiFpsTextBlocks[i];
	}

	delete[] _aiFpsTextBlocks;

	delete _skyBox;
	delete _map;

	delete _smallFont;
	delete _fpsText;
	delete _standardLaser;

	Bot::DestroySharedResources();
}

void Grambots::Init()
{
	Bot::InitBotCells(800.0f, 800.0f);

	// init laser
	GLuint laserShader = GetShaderManager()->LoadShaders("Laser",
		Assets::GetAssetPath("Shaders/LaserVertex.glsl"),
		Assets::GetAssetPath("Shaders/LaserPixel.glsl"));
	_standardLaser = new Laser(laserShader);

	InitFonts();
	_fpsText = new TextBlock(15, _smallFont, "  FPS");

	GetTextureManager()->CreateTextureFromFile(Assets::GetAssetPath("Textures/marker.gtx"), "marker", GL_RGBA, GL_RGBA);
	GLuint skyTextureId = GetTextureManager()->CreateTextureFromFile(Assets::GetAssetPath("Textures/BlueStars.gtx"), "sky", GL_RGB, GL_RGB);
	GetTextureManager()->CreateTextureFromFile(Assets::GetAssetPath("Textures/weird.gtx"), "satin", GL_RGB, GL_RGB);
	AnimatedMeshData *botMeshData = AnimatedMeshData::LoadFromFile(Assets::GetAssetPath("Models/guy.g3d"));
	LoadMap("Maps/test.gmap");

	_skyBox = new SkyBox(skyTextureId);

	InitIndicatorMesh();

	int teamCount = _map->GetNumberOfPlayers();
	for (int i = 1; i <= teamCount; i++)
	{
		Team *team = new Team(this, GetTeamColor(i), i, _map->GetSpawnPoint(i), botMeshData);
		AiWorker *aiWorker = new AiWorker(team, _map);
		_teams.push_back(team);
		_aiWorkers.push_back(aiWorker);

		for (int j = 0; j < 10; j++)
			team->SpawnNewBot();

		if (i == 1)
			_humanPlayerAiWorker = aiWorker;

		aiWorker->Start();
	}

	_aiFpsTextBlocks = new TextBlock *[_aiWorkers.size()];
	for (unsigned int i = 0; i < _aiWorkers.size(); i++)
	{
		_aiFpsTextBlocks[i] = new TextBlock(10, _smallFont, "AIFPS");
		_aiFpsTextBlocks[i]->SetPosition(Vector2(0.0f, (i + 1) * 15.0f));
	}
}

Color Grambots::GetTeamColor(int teamNumber)
{
	switch (teamNumber)
	{
	case 1: return Color(0.0f, 1.0f, 0.0f);
	case 2: return Color(1.0f, 0.0f, 0.0f);
	case 3: return Color(0.0f, 0.0f, 1.0f);
	case 4: return Color(1.0f, 1.0f, 0.0f);
	case 5: return Color(0.0f, 1.0f, 1.0f);
	case 6: return Color(1.0f, 0.0f, 1.0f);
	case 7: return Color(1.0f, 1.0f, 1.0f);
	case 8: return Color(0.5f, 0.5f, 1.0f);
	default: return Color(0.0f, 0.0f, 0.0f);
	}
}

void Grambots::InitIndicatorMesh()
{
	float radius = 2.5f;
	float verts[] = {
		-radius, 0.0f,  radius,
		 radius, 0.0f,  radius,
		 radius, 0.0f, -radius,
		-radius, 0.0f, -radius
	};

	GLushort indices[] = { 0, 2, 1, 0, 3, 2 };

	float uvCoords[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	GLuint textureId = GetTextureManager()->GetTexture("marker");

	_indicatorMesh = new StaticMesh(verts, 4, indices, 2, 3, GL_TRIANGLES, NULL, uvCoords, textureId, NULL);
}

void Grambots::LoadMap(std::string mapName)
{
	_map = Map::LoadFromFile(Assets::GetAssetPath(mapName), this);
}

CollisionResult Grambots::RayWorldIntersection(const Ray &ray, void *objectToIgnore, int collisionTypeMask)
{
	bool checkBotCollision = collisionTypeMask == 0 || (collisionTypeMask & BotCollision);
	bool checkMapCollision = collisionTypeMask == 0 || (collisionTypeMask & MapCollision);
	CollisionResult nearest(NoCollision);

	if (checkMapCollision)
	{
		Tile currentTile = _map->GetTileAtPoint(ray.start.X, ray.start.Z);
		int currentTileRowIndex = (int)(ray.start.Z / TILE_WIDTH);
		int currentTileColIndex = (int)(ray.start.X / TILE_WIDTH);

		while (true)
		{

			CollisionResult tileCollision = _map->CalculateRayTileCollision(ray, currentTile, currentTileRowIndex, currentTileColIndex);
			if (tileCollision.collisionType != NoCollision)
			{
				nearest = NearestCollision(nearest, tileCollision);
				break;
			}
			else if (!_map->FindNextTileAlongRay(ray, &currentTileRowIndex, &currentTileColIndex, &currentTile))
			{
				break;
			}

		}
	}

	if (checkBotCollision)
	{
		// if the ray already collided with map, then shorten to the point of that collision to avoid pointless tests
		Ray effectiveRay(ray.start, ray.direction, nearest.collisionType != NoCollision ? nearest.distance : ray.length);

		CollisionResult botCollision = Bot::RayBotCollision(effectiveRay, objectToIgnore);
		nearest = NearestCollision(nearest, botCollision);
	}

	return nearest;
}

void Grambots::StartLaserShot(LaserInfo laserInfo)
{
	_lasers.push_back(laserInfo);
}

void Grambots::RenderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    PerspectiveMode();

	glPushMatrix();
	glMultMatrixf(_cameraRotationMatrix);
	_skyBox->Render();
	glTranslatef(_cameraPosition.X * -1, _cameraPosition.Y * -1, _cameraPosition.Z * -1);

	// render the map
	_map->Render();

	// render bots
	unsigned int teamCount = (unsigned int)_teams.size();
    for (unsigned int i = 0; i < teamCount; i++)
    {
    	Bot **bots = _teams[i]->GetBots();
    	for (unsigned int j = 0; j < MAX_BOTS_PER_TEAM; j++)
    	{
    		Bot *bot = bots[j];
    		if (bot->GetIsActive())
    			bot->Render();
    	}
    }

	// render projectiles
	unsigned int laserCount = _lasers.size();
	for (unsigned int i = 0; i < laserCount; i++)
	{
		LaserInfo laserInfo = _lasers[i];
		glPushMatrix();
		glTranslatef(laserInfo.position.X, laserInfo.position.Y, laserInfo.position.Z);
		glRotatef(laserInfo.heading * _180_OVER_PI, 0.0f, 1.0f, 0.0f);
		glRotatef(laserInfo.pitch * _180_OVER_PI, 1.0f, 0.0f, 0.0f);

		// this is to shorten the laser beam if necessary (like if you shoot someone at point blank)
		if (laserInfo.scale != 1.0f) glScalef(1.0f, 1.0f, laserInfo.scale);

		_standardLaser->Render();
		glPopMatrix();
		_lasers[i] = laserInfo;
	}

	// render 3d UI stuff like cusor click indicator
	float timeSinceIndicatorStart = _time - _indicatorStartTime;
	if (timeSinceIndicatorStart < 300.0f)
	{
		float scale = timeSinceIndicatorStart / 300.0f;

		glPushMatrix();
		glEnable(GL_BLEND);
		glTranslatef(_indicatorPosition.X, _indicatorPosition.Y, _indicatorPosition.Z);
		glScalef(scale, scale, scale);
		_indicatorMesh->Render();
		glDisable(GL_BLEND);
		glPopMatrix();
	}

	glPopMatrix();




	OrthoMode();

	glEnable(GL_BLEND);
	glFrontFace(GL_CW);
	_fpsText->Render();
	for (unsigned int i = 0; i < _aiWorkers.size(); i++)
		_aiFpsTextBlocks[i]->Render();
	glDisable(GL_BLEND);

    SDL_GL_SwapBuffers();
}

void Grambots::InitFonts()
{
	GLuint textureId = GetTextureManager()->CreateTextureFromFile(Assets::GetAssetPath("Textures/GraemeFaceBold.gtx"), "smallFont", GL_RGBA, GL_RGBA);
	std::vector<CharInfo> characters(256);
	characters['A'] = CharInfo(0,0);
	characters['B'] = CharInfo(0,1);
	characters['C'] = CharInfo(0,2);
	characters['D'] = CharInfo(0,3);
	characters['E'] = CharInfo(0,4);
	characters['F'] = CharInfo(0,5);
	characters['G'] = CharInfo(0,6);
	characters['H'] = CharInfo(0,7);
	characters['I'] = CharInfo(0,8);
	characters['J'] = CharInfo(0,9);
	characters['K'] = CharInfo(0,10);
	characters['L'] = CharInfo(0,11);
	characters['M'] = CharInfo(0,12);
	characters['N'] = CharInfo(1,0);
	characters['O'] = CharInfo(1,1);
	characters['P'] = CharInfo(1,2);
	characters['Q'] = CharInfo(1,3);
	characters['R'] = CharInfo(1,4);
	characters['S'] = CharInfo(1,5);
	characters['T'] = CharInfo(1,6);
	characters['U'] = CharInfo(1,7);
	characters['V'] = CharInfo(1,8);
	characters['W'] = CharInfo(1,9);
	characters['X'] = CharInfo(1,10);
	characters['Y'] = CharInfo(1,11);
	characters['Z'] = CharInfo(1,12);
	characters['a'] = CharInfo(2,0);
	characters['b'] = CharInfo(2,1);
	characters['c'] = CharInfo(2,2);
	characters['d'] = CharInfo(2,3);
	characters['e'] = CharInfo(2,4);
	characters['f'] = CharInfo(2,5);
	characters['g'] = CharInfo(2,6);
	characters['h'] = CharInfo(2,7);
	characters['i'] = CharInfo(2,8);
	characters['j'] = CharInfo(2,9);
	characters['k'] = CharInfo(2,10);
	characters['l'] = CharInfo(2,11);
	characters['m'] = CharInfo(2,12);
	characters['n'] = CharInfo(3,0);
	characters['o'] = CharInfo(3,1);
	characters['p'] = CharInfo(3,2);
	characters['q'] = CharInfo(3,3);
	characters['r'] = CharInfo(3,4);
	characters['s'] = CharInfo(3,5);
	characters['t'] = CharInfo(3,6);
	characters['u'] = CharInfo(3,7);
	characters['v'] = CharInfo(3,8);
	characters['w'] = CharInfo(3,9);
	characters['x'] = CharInfo(3,10);
	characters['y'] = CharInfo(3,11);
	characters['z'] = CharInfo(3,12);
	characters['0'] = CharInfo(4,0);
	characters['1'] = CharInfo(4,1);
	characters['2'] = CharInfo(4,2);
	characters['3'] = CharInfo(4,3);
	characters['4'] = CharInfo(4,4);
	characters['5'] = CharInfo(4,5);
	characters['6'] = CharInfo(4,6);
	characters['7'] = CharInfo(4,7);
	characters['8'] = CharInfo(4,8);
	characters['9'] = CharInfo(4,9);
	characters['`'] = CharInfo(5,0);
	characters['~'] = CharInfo(5,1);
	characters['!'] = CharInfo(5,2);
	characters['@'] = CharInfo(5,3);
	characters['#'] = CharInfo(5,4);
	characters['$'] = CharInfo(5,5);
	characters['%'] = CharInfo(5,6);
	characters['^'] = CharInfo(5,7);
	characters['&'] = CharInfo(5,8);
	characters['*'] = CharInfo(5,9);
	characters['('] = CharInfo(5,10);
	characters[')'] = CharInfo(5,11);
	characters['-'] = CharInfo(5,12);
	characters['_'] = CharInfo(5,13);
	characters['+'] = CharInfo(6,0);
	characters['='] = CharInfo(6,1);
	characters['{'] = CharInfo(6,2);
	characters['}'] = CharInfo(6,3);
	characters['['] = CharInfo(6,4);
	characters[']'] = CharInfo(6,5);
	characters[':'] = CharInfo(6,6);
	characters[';'] = CharInfo(6,7);
	characters[','] = CharInfo(6,8);
	characters['.'] = CharInfo(6,9);
	characters['"'] = CharInfo(6,10);
	characters['\''] = CharInfo(6,11);
	characters['<'] = CharInfo(6,12);
	characters['>'] = CharInfo(6,13);
	characters['?'] = CharInfo(7,0);
	characters['/'] = CharInfo(7,1);
	characters['\\'] = CharInfo(7,2);
	characters['|'] = CharInfo(7,3);
	characters[' '] = CharInfo(8,0);
	characters['\0'] = CharInfo(8,0);
	characters['\n'] = CharInfo(8,0);
	_smallFont = new Font(characters, textureId, 128, 128, 9, 11);
}

void Grambots::UpdateScene(float deltaTime)
{
	unsigned int teamCount = _teams.size();
	for (unsigned int i = 0; i < teamCount; i++)
	{
		Bot **bots = _teams[i]->GetBots();
		for (unsigned int j = 0; j < MAX_BOTS_PER_TEAM; j++)
		{
			Bot *bot = bots[j];
			if (bot->GetIsActive())
				bot->Update(deltaTime);
		}
	}

	UpdateProjectiles(deltaTime);
}

void Grambots::UpdateProjectiles(float deltaTime)
{
	float laserMoveFactor = deltaTime * LASER_SPEED;

	for (unsigned int i = 0; i < _lasers.size(); i++)
	{
		LaserInfo laser = _lasers[i];

		if (laser.position.X > MAX_LASER_DISTANCE || laser.position.Y > MAX_LASER_DISTANCE || laser.position.Z > MAX_LASER_DISTANCE)
		{
			_lasers.erase(_lasers.begin()+i);
			i--;
		}
		else
		{
			if (laser.isExploding)
			{
				_lasers.erase(_lasers.begin()+i);
				i--;
			}
			else
			{
				Vector3 startPosition = laser.position;
				laser.position.X += laser.direction.X * laserMoveFactor;
				laser.position.Y += laser.direction.Y * laserMoveFactor;
				laser.position.Z += laser.direction.Z * laserMoveFactor;
				float rayLength = GMath::GetDistanceBetweenPoints(startPosition, laser.position);
				Ray thisFrameLaserRay(startPosition, laser.direction, rayLength);
				CollisionResult collision = RayWorldIntersection(thisFrameLaserRay);
				if (collision.collisionType != NoCollision)
				{
					laser.isExploding = true;
					laser.position = collision.collisionPoint;
					if (collision.object != NULL) {
						Bot *botThatGotShot = (Bot *)collision.object;
						botThatGotShot->Die();
					}
				}

				laser.scale = GMath::Min(1.0f, GMath::GetDistanceBetweenPoints(laser.originalPosition, laser.position) / LASER_BEAM_LENGTH);

				_lasers[i] = laser;
			}
		}
	}
}

void Grambots::Pause()
{
	_isPaused = true;
}

void Grambots::Resume()
{
	_isPaused = false;
}

void Grambots::Begin()
{
    // init opengl
    SDL_Init(SDL_INIT_VIDEO);
    const SDL_VideoInfo *info = SDL_GetVideoInfo();
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	// antialiasing!
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

    SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, info->vfmt->BitsPerPixel, SDL_HWSURFACE | SDL_OPENGL); // | SDL_FULLSCREEN

#ifdef OS_WINDOWS
    glewInit();
#endif

    glShadeModel(GL_SMOOTH);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glEnable(GL_CULL_FACE);

	glShadeModel (GL_SMOOTH);

	// alpha blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(FIELD_OF_VIEW, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1, 1000.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SDL_GL_SwapBuffers();
    SDL_ShowCursor(1);

	Init();
}

void Grambots::End()
{
	// tell all the ai workers to stop (this is asynchronous and could take a few millis)
	unsigned int aiWorkerCount = _aiWorkers.size();
	for (unsigned int i = 0; i < aiWorkerCount; i++)
		_aiWorkers[i]->Stop();

	// wait until each ai worker is actually stopped as the above was asynchronous
	for (unsigned int i = 0; i < aiWorkerCount; i++)
		_aiWorkers[i]->WaitUntilFinished();

    // stop opengl
    SDL_Quit();
}

void Grambots::Run()
{

    _prevMillis = (float)SDL_GetTicks();

    float deltaTime;
    SDL_Event sdlEvent;

	bool leftMouseButtonDownLastTime = false;
	//int middleX = SCREEN_WIDTH / 2, middleY = SCREEN_HEIGHT / 2;
	//SDL_WarpMouse(middleX, middleY);

	bool running = true;
    while (running)
    {
		SDL_PollEvent(&sdlEvent);
        switch (sdlEvent.type)
        {
        case SDL_QUIT:
            return;
		//case SDL_ACTIVEEVENT:
		//	if (sdlEvent.active.state & SDL_APPINPUTFOCUS)
		//	{
		//		if (sdlEvent.active.gain)
		//			Resume();
		//		else
		//			Pause();
		//	}
		//	break;
        }

		if (_isPaused)
			continue;

        _time = (float)SDL_GetTicks();

        deltaTime = _time - _prevMillis;
        if (deltaTime > MAX_DELTA)
            deltaTime = MAX_DELTA;
        else if (deltaTime < MIN_DELTA)
            continue;
        _prevMillis = _time;

        if (_time - _millisAtFpsUpdate >= 1000)
        {
            int fps = _framesSinceFpsUpdate;
            char fpsStr[15];
            snprintf(fpsStr, 15, "  FPS %d\n", fps);
            //std::string msg(fpsStr);
            //Logger::DebugMsg(msg);
			_fpsText->SetText(std::string(fpsStr));
            _framesSinceFpsUpdate = 0;
            _millisAtFpsUpdate = _time;

			for (unsigned int i = 0; i < _aiWorkers.size(); i++)
			{
				char aiFpsStr[15];
				snprintf(aiFpsStr, 15, "AIFPS %d\n", _aiWorkers[i]->GetFps());
				_aiFpsTextBlocks[i]->SetText(std::string(aiFpsStr));
			}
        }
        else
        {
            _framesSinceFpsUpdate++;
        }



		int numKeys;
		Uint8 *keyState = SDL_GetKeyState(&numKeys);


		if (keyState[SDLK_w])
			MoveCameraForward(deltaTime);
		if (keyState[SDLK_a])
			MoveCameraLeft(deltaTime);
		if (keyState[SDLK_s])
			MoveCameraBackward(deltaTime);
		if (keyState[SDLK_d])
			MoveCameraRight(deltaTime);
		if (keyState[SDLK_q])
			RotateCameraCcwAroundY(deltaTime);
		if (keyState[SDLK_e])
			RotateCameraCwAroundY(deltaTime);
		if (keyState[SDLK_LSHIFT])
			MoveCameraDown(deltaTime);
		if (keyState[SDLK_SPACE])
			MoveCameraUp(deltaTime);
		if (keyState[SDLK_ESCAPE])
			running = false;

		static bool isShooting = false;
		if (keyState[SDLK_c])
		{
			if (!isShooting)
			{
				ShootLaser(_teams[0]->GetBots()[0]);
				isShooting = true;
			}
		}
		else
		{
			isShooting = false;
		}

		//static int x = 0, y = 0;
		//SDL_GetMouseState(&x, &y);
		//x -= middleX;
		//y -= middleY;
		//if (x != 0 || y != 0)
		//{
		//	RotateCameraWithMouse(x, y);
		//	UpdateCameraRotation();
		//}
		//SDL_WarpMouse(middleX, middleY);

		//if (_selectedRenderable != NULL)
		//	_selectedRenderable->HandleEvent(keyState, deltaTime);

		if (sdlEvent.type == SDL_MOUSEBUTTONDOWN && sdlEvent.button.button == SDL_BUTTON_LEFT)
		{
			if (!leftMouseButtonDownLastTime)
			{
				int x = 0, y = 0;
				SDL_GetMouseState(&x, &y);
				MousePick(Vector2((float)x, (float)y));
				leftMouseButtonDownLastTime = true;
			}
		}
		else
		{
			leftMouseButtonDownLastTime = false;
		}


		UpdateCameraRotation();

//		unsigned int botCount = _bots.size();
//		for (unsigned int i = 0; i < botCount; i++)
//			if (_bots[i]->GetIsSelected())
//				_bots[i]->HandleEvent(keyState, deltaTime);

		UpdateScene(deltaTime);

        RenderScene();
    }
}

void Grambots::OrthoMode()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0.0f, SCREEN_WIDTH, 0.0f, SCREEN_HEIGHT, -1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}

void Grambots::PerspectiveMode()
{
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
}

void Grambots::ShootLaser(Bot *bot)
{
	StartLaserShot(bot->CreateNewLaserShotFromGun());
}

Ray Grambots::CastRayFromScreenPosition(Vector2 screenPos)
{
	Ray result;

	float halfScreenWidth = SCREEN_WIDTH / 2.0f;
	float halfScreenHeight = SCREEN_HEIGHT / 2.0f;
	//float aspectRatio = SCREEN_WIDTH / SCREEN_HEIGHT;

	//float x = (screenPos.X / halfScreenWidth - 1.0f) / aspectRatio;
	//float y = 1.0f - screenPos.Y / halfScreenHeight;

	float x = screenPos.X - halfScreenWidth;
	float y = halfScreenHeight - screenPos.Y;
	float cameraDistanceToScreen = halfScreenHeight * tan(FIELD_OF_VIEW * PI_OVER_180);

	Vector3 vec = GMath::Normalize(Vector3(x, y, -cameraDistanceToScreen));

	Matrix33 m(
		_cameraRotationMatrix[0], _cameraRotationMatrix[4], _cameraRotationMatrix[8],
		_cameraRotationMatrix[1], _cameraRotationMatrix[5], _cameraRotationMatrix[9],
		_cameraRotationMatrix[2], _cameraRotationMatrix[6], _cameraRotationMatrix[10]);

	result.direction = GMath::MatVecMult(m, vec);
	result.start = _cameraPosition;
	result.length = 1e20f;

	return result;
}

void Grambots::MousePick(Vector2 screenPos)
{
	Ray ray = CastRayFromScreenPosition(screenPos);
	CollisionResult collision = RayWorldIntersection(ray);

	if (collision.object != NULL && collision.collisionType == BotCollision)
	{
		unsigned int teamCount = _teams.size();
		for (unsigned int i = 0; i < teamCount; i++)
		{
			Bot **bots = _teams[i]->GetBots();
			for (unsigned int j = 0; j < MAX_BOTS_PER_TEAM; j++)
				bots[j]->SetIsSelected(false);
		}

		Bot *bot = (Bot *)collision.object;
		bot->SetIsSelected(!bot->GetIsSelected());
	}
	else if (collision.collisionType == MapCollision)
	{
		_indicatorPosition = collision.collisionPoint;
		_indicatorPosition.Y += 1.0f;
		_indicatorStartTime = _time;
		Vector2 destination(collision.collisionPoint.X, collision.collisionPoint.Z);

		unsigned int teamCount = _teams.size();
		for (unsigned int i = 0; i < teamCount; i++)
		{
			Bot **bots = _teams[i]->GetBots();
			for (unsigned int j = 0; j < MAX_BOTS_PER_TEAM; j++)
			{
				Bot *bot = bots[j];
				if (bot->GetIsSelected())
				{
					AiCommand cmd;
					cmd.bot = bot;
					cmd.destination = destination;
					_humanPlayerAiWorker->GetInputCommandBuffer()->Write(cmd);
				}
			}
		}
	}
}
