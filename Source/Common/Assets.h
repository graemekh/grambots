#pragma once

#include <string>

class Assets
{
public:
    static std::string GetAssetPath(std::string relativePath);
    
private:
    static std::string JoinPaths(std::string part1, std::string part2);
    static std::string GetAssetBasePath();
};
