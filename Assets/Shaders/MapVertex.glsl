#version 120

varying vec2 uvCoord;
varying vec3 normal;
varying vec3 lightDir;
varying float lightDistance;
varying vec3 sunDir;
varying vec4 color;

void main()
{
	sunDir = vec3(0.3333333, 0.66666667, 0.66666667); 
	vec3 lightPos = vec3(0.0, 0.0, 0.0);
	color = gl_Color;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    uvCoord = vec2(gl_MultiTexCoord0);
	normal = gl_NormalMatrix * gl_Normal;
	vec3 vertToLightVec = lightPos - vec3(gl_ModelViewMatrix * gl_Vertex);
	lightDir = normalize(vertToLightVec);
	lightDistance = length(vertToLightVec);
}