#version 120
#extension GL_EXT_gpu_shader4 : require

uniform int animationMode; // 0 = normal; 1 = use overrides
uniform vec4 boneOverrides[__BONE_COUNT__];
uniform vec3 bonePositions[__BONE_COUNT__];
uniform sampler2D boneFrameMatrices;
uniform sampler2D boneFrameQuaternions;
uniform int bonePaths[__BONE_PATHS_LENGTH__];

uniform int upperBodyFrameFrom;
uniform int upperBodyFrameTo;
uniform float upperBodyInterpolationFactor;

uniform int lowerBodyFrameFrom;
uniform int lowerBodyFrameTo;
uniform float lowerBodyInterpolationFactor;

uniform vec4 outlineColor;
uniform float outlineThickness;

attribute float bonePathPosition;
attribute float bodySection;

varying vec3 normal;
varying vec4 overrideColor;

vec4 Slerp(vec4 q0, vec4 q1, float t)
{
	float cosOmega = q0.x*q1.x + q0.y*q1.y + q0.z*q1.z + q0.w*q1.w;
	if (cosOmega < 0.0)
	{
		q1.x = -q1.x;
		q1.y = -q1.y;
		q1.z = -q1.z;
		q1.w = -q1.w;
		cosOmega = -cosOmega;
	}
	float k0, k1;
	if (cosOmega > 0.9999f)
	{
		k0 = 1.0f - t;
		k1 = t;
	}
	else
	{
		float sinOmega = sqrt(1.0 - cosOmega*cosOmega);
		float omega = atan(sinOmega, cosOmega);
		float oneOverSinOmega = 1.0 / sinOmega;
		k0 = sin((1.0 - t) * omega) * oneOverSinOmega;
		k1 = sin(t * omega) * oneOverSinOmega;
	}
	return vec4(
		q0.x*k0 + q1.x*k1,
		q0.y*k0 + q1.y*k1,
		q0.z*k0 + q1.z*k1,
		q0.w*k0 + q1.w*k1);
}

mat3 NormalizedQuaternionToMatrix33(vec4 q)
{
	return mat3(
		1.0f - 2.0f*q.y*q.y - 2.0f*q.z*q.z,
		2.0f*q.x*q.y + 2.0f*q.z*q.w, 
		2.0f*q.x*q.z - 2.0f*q.y*q.w,
		
		2.0f*q.x*q.y - 2.0f*q.z*q.w,
		1.0f - 2.0f*q.x*q.x - 2.0f*q.z*q.z,
		2.0f*q.y*q.z + 2.0f*q.x*q.w,
		
		2.0f*q.x*q.z + 2.0f*q.y*q.w, 
		2.0f*q.y*q.z - 2.0f*q.x*q.w,
		1.0f - 2.0f*q.x*q.x - 2.0f*q.y*q.y);
}

void main()
{
	bool isOutline = outlineThickness > 0.001;
	overrideColor = vec4(-1, -1, -1, -1);
	int bonePathPositionInt = int(bonePathPosition);
	int bodySectionInt = int(bodySection);
    int boneCount = __BONE_COUNT__;
    
	vec3 vertPosition = vec3(gl_Vertex);
	normal = gl_Normal;

    int frameFrom;
    int frameTo;
    float interpolationFactor;
    
	// BEFORE rotating vert with bone(s) extrude along normal if necessary
	if (isOutline)
	{
		overrideColor = outlineColor;
		vertPosition = vertPosition + (normal * outlineThickness);
	}

	// apply animation transformation
	for (int position = bonePathPositionInt; position < __BONE_PATHS_LENGTH__; position++)
	{
		int boneIndex = bonePaths[position];
		vec3 bonePos = bonePositions[boneIndex];
		mat3 boneMatrix;
        
        if (animationMode == 0)
		{
			if (bodySectionInt == 0)
			{
				frameFrom = upperBodyFrameFrom;
				frameTo = upperBodyFrameTo;
				interpolationFactor = upperBodyInterpolationFactor;
			}
			else
			{
				frameFrom = lowerBodyFrameFrom;
				frameTo = lowerBodyFrameTo;
				interpolationFactor = lowerBodyInterpolationFactor;            
			}

			if (interpolationFactor < 0.999)
			{
				// there is interpolation to do, so create matrix from SLERPed quaternions    
				vec4 q0 = texelFetch2D(boneFrameQuaternions, ivec2(frameFrom, boneIndex), 0);
				vec4 q1 = texelFetch2D(boneFrameQuaternions, ivec2(frameTo, boneIndex), 0);
				vec4 interpolatedQuaternion = Slerp(q0, q1, interpolationFactor);
				boneMatrix = NormalizedQuaternionToMatrix33(interpolatedQuaternion);
			}
			else
			{
				// there is no interpolation to do, so directly use rotation stored for this frame
				boneMatrix = mat3(
					vec3(texelFetch2D(boneFrameMatrices, ivec2(frameTo * 3 + 0, boneIndex), 0)), 
					vec3(texelFetch2D(boneFrameMatrices, ivec2(frameTo * 3 + 1, boneIndex), 0)), 
					vec3(texelFetch2D(boneFrameMatrices, ivec2(frameTo * 3 + 2, boneIndex), 0)));
			}
		}
		else
		{
			// use precalculated rotation from CPU
			vec4 quaternion = boneOverrides[boneIndex];
			boneMatrix = NormalizedQuaternionToMatrix33(quaternion);
		}
        
		// transform vertex/normal to frame
		vertPosition = (boneMatrix * (vertPosition - bonePos)) + bonePos;
		
		// normal is only needed if not drawing outline
		if (!isOutline) normal = normalize((boneMatrix * (normal - bonePos)) + bonePos);
		
		// check for termination character
		if (boneIndex == 0)
			break;
	}

	gl_Position = gl_ModelViewProjectionMatrix * vec4(vertPosition, 1.0);
	
	// normal is only needed if not drawing outline
	if (!isOutline) normal = gl_NormalMatrix * normal;
}