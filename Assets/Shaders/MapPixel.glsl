#version 120

uniform sampler2D mapTexture;

varying vec2 uvCoord;
varying vec3 normal;
varying vec3 lightDir;
varying float lightDistance;
varying vec3 sunDir;
varying vec4 color;

void main()
{
	//float diffuseTerm = 1.0 / (lightDistance / 10);
	//float diffuseTerm = clamp(dot(normal, lightDir), 0.0, 1.0) * 0.5 + 0.5;

	float sunTerm = clamp(dot(normal, sunDir), 0.0, 1.0) / 2 + 0.5;

    gl_FragColor = texture2D(mapTexture, uvCoord);//vec4(1.0, 1.0, 1.0, 1.0);
	gl_FragColor.r *= color.r;
	gl_FragColor.g *= color.g;
	gl_FragColor.b *= color.b;
	gl_FragColor *= sunTerm * 2.0;
	//gl_FragColor += diffuseTerm;
	
	//gl_FragColor = gl_FragColor * sunTerm + diffuseTerm;
	
	//gl_FragColor = vec4(diffuseTerm, diffuseTerm, diffuseTerm, 1.0);
}