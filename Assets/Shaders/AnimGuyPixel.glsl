#version 120

varying vec3 normal;
varying vec4 overrideColor;

void main()
{
	if (!(overrideColor.x < -0.0001))
	{
		gl_FragColor = overrideColor;
	}
	else
	{
		vec3 lightVec = normalize(vec3(1.0, 1.0, 1.0));
		float lightDotProd = dot(lightVec, normal);
		float magnitude = 0.0; //(lightDotProd + 1.0) / 7.0;

		gl_FragColor = vec4(magnitude, magnitude, magnitude, 1.0);
	}
}
