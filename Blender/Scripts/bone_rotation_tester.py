import bpy, time
from math import *
from mathutils import *

def get_pose_bone_matrix(pose_bone):
    local_matrix = pose_bone.matrix_channel.to_3x3()
    if pose_bone.parent is None:
        return local_matrix
    else:
        return pose_bone.parent.matrix_channel.to_3x3().inverted() * local_matrix

def translate_point(bone_name, frame, scene, armature, bone_limit=None):
    
    # get bone names in order
    current_bone = armature.pose.bones[bone_name]
    bone_names = [current_bone.name]
    while current_bone.parent is not None:
        current_bone = current_bone.parent
        bone_names.insert(0, current_bone.name)
    bone_names.reverse()

    # find initial tail position
    start = armature.data.bones[bone_name].tail_local.copy()
    point = start.copy()
    
    # go to desired frame and transform for each bone
    scene.frame_set(frame)
    bone_count = 0
    for bname in bone_names:
        print(bname)
        bone_count += 1
        bone = armature.data.bones[bname]
        pbone = armature.pose.bones[bname]
        pbm = get_pose_bone_matrix(pbone) # pbm = pose bone matrix
        point = pbm * (point - bone.head_local) + bone.head_local
        if bone_limit is not None and bone_count >= bone_limit:
            break
    
    # move the cursor to end location as visual check
    scene.cursor_location = point.copy()
    
    # print some debug info
    print('result: ' + str(point))
    print('actual: ' + str(armature.pose.bones[bone_names[-1]].tail))
    
scene = bpy.data.scenes[0]
armature = scene.objects['Bones']
translate_point('hand.r', 42, scene, armature, bone_limit=None)