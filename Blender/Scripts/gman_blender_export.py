import bpy
from mathutils import *
from math import *

def get_code_for_mesh(mesh):
    ''' Creates a bunch of C arrays to be used for vertex, index, and normal buffers '''

    code = '# vertices (v|position.x|position.y|position.z|normal.x|normal.y|normal.z)\n'

    # vertices (NOTE: z and y are flipped)
    for vert in mesh.data.vertices:
        co = mesh.matrix_world * vert.co
        n = mesh.matrix_world * vert.normal
        code += 'v|%f|%f|%f|%f|%f|%f\n' % (co.x, co.z, co.y, n.x, n.z, n.y) 

    code += '# triangles\n'
    
    # indices
    for face in mesh.data.faces:
        
        if len(face.vertices) != 3:
            raise Exception('This face does not have 3 vertices.  Only triangles are supported.')
        
        code += 't|%d|%d|%d\n' % (face.vertices[0], face.vertices[1], face.vertices[2])

    return code

def get_bone_code(armature, bone, vertex_groups, indent=''):
    bone_code = ''
    
    if bone.parent is None:
        parent_name = 'NULL'
    else:
        parent_name = bone.parent.name
        
    head = armature.matrix_world * bone.head_local
    bone_code += '%sb|%s|%f|%f|%f|' % \
        (indent, bone.name, head.x, head.z, head.y)
    
    sep = ''
    for vert_index in vertex_groups[bone.name]:
        bone_code += '%s%d' % (sep, vert_index)
        sep = ','
    
    bone_code += '\n'
    
    for child in bone.children:
        bone_code += get_bone_code(armature, child, vertex_groups, indent + '-')
        
    return bone_code
    
    
def get_code_for_armature(armature, vertex_groups):
    armature_code = '# bones (name|X|Y|Z|vertex indices...)\n'
    
    for bone in armature.data.bones:
        if bone.parent is None:
            armature_code += get_bone_code(armature, bone, vertex_groups)
            
    return armature_code

def get_vertex_groups(mesh):
    vert_group_names = {}
    for grp in mesh.vertex_groups:
        vert_group_names[grp.index] = grp.name

    vert_groups = {}
    for vert in mesh.data.vertices:
        for grp in vert.groups:
            grp_name = vert_group_names[grp.group]
            if not grp_name in vert_groups.keys():
                vert_groups[grp_name] = []
            vert_groups[grp_name].append(vert.index)
            
    return vert_groups

def get_pose_bone_matrix(pose_bone):
    local_matrix = pose_bone.matrix_channel.to_3x3()
    if pose_bone.parent is None:
        return local_matrix
    else:
        return pose_bone.parent.matrix_channel.to_3x3().inverted() * local_matrix

def get_animation_code(armature, frame_count, scene):
    code = '# bone animation data (bone|frame|quat W|quat X|quat Y|quat Z)\n'
    for frame in range(frame_count):
        scene.frame_set(frame)
        for posed_bone in armature.pose.bones:
            q = get_pose_bone_matrix(posed_bone).to_quaternion()
            code += 'a|%s|%d|%f|%f|%f|%f\n' % \
                (posed_bone.name, frame, q.w*-1, q.x, q.z, q.y)
    return code
     
def get_header_text(mesh, armature, frame_count):
    text = '# header info\n'
    text += 'vertexCount=%d\n' % len(mesh.data.vertices)
    text += 'triangleCount=%d\n' % len(mesh.data.faces)
    if armature is not None:
        text += 'boneCount=%d\n' % len(armature.data.bones)
        text += 'frameCount=%d\n' % frame_count
    return text

def get_special_vert_group_text(vertex_groups):
    special_keys = [key for key in vertex_groups.keys() if key.startswith('special.')]
    
    if len(special_keys) == 0:
        return ""
    else:
        text = "# Special vertices\n"
        for key in special_keys:
            text += "s|%s|%d\n" % (key.split('.')[1], vertex_groups[key][0])
        return text

def save(path):
    scene = bpy.context.scene
    meshes = [m for m in bpy.context.selected_objects if m.type == 'MESH']
    armatures = [m for m in bpy.context.selected_objects if m.type == 'ARMATURE']

    if len(meshes) != 1:
        raise Exception("Must be exactly one mesh!")
        
    if len(armatures) > 1:
        raise Exception("No more than one armature supported!")

    mesh = meshes[0]
    armature = armatures[0] if len(armatures) > 0 else None
    frame_count = scene.frame_end + 1
    
    vertex_groups = get_vertex_groups(mesh)
    
    header_output = get_header_text(mesh, armature, frame_count)
    mesh_output = get_code_for_mesh(mesh)
    special = get_special_vert_group_text(vertex_groups)
    result = header_output + special + mesh_output
    
    if armature is not None:
        bone_output = get_code_for_armature(armature, vertex_groups)
        anim_output = get_animation_code(armature, frame_count, scene)
        result += bone_output + anim_output

    out_file = open(path, 'w')
    out_file.write(result)
    out_file.close()    

save(r'C:\Dev\SdlPhysX\Assets\Models\guy.g3d')
